﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class goal_toggle : MonoBehaviour {

	public Text my_name;
	public Toggle my_toggle;
		public Image image_on;
		public Image image_off;

	public Sprite toggle_on;
	public Sprite toggle_off;

	public Sprite star_on;
	public Sprite star_off;

	public void Setup(string name, bool done, bool use_star)
	{
		if (use_star)
			{
			image_on.sprite = star_on;
			image_off.sprite = star_off;
			}
		else
			{
			image_on.sprite = toggle_on;
			image_off.sprite = toggle_off;
			}

		my_name.text = name;
		Done(done);
	}

	public void Done(bool done)
	{
		my_toggle.isOn = done;
	}

}
