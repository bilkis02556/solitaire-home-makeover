﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class deck : MonoBehaviour
{
    public GameManager my_gameManager;
    public enum main_rule
    {
        chain
    }

    [Header("Rule")]
    public main_rule main_rule_selected = main_rule.chain;
    public enum StateFinalScore { win, lose_time_up, lose_time_up_card,lose_do_not_card_in_deck }
    private StateFinalScore stateFinal;

    [Header("Card")]
    public bool show_unused_card_when_player_take_a_new_card_from_the_deck;//if true, when player ask a new card and miss to use the current, the miss move shake
    public int give_an_useful_card_not_less_than_n_new_card_from_the_deck;//because deck card are random, in order to avoid to have a long sequence of useless cards by accident, you can set here the max lenght of the useless sequence. After that the deck will give an useful for sure. If you don't want give this help to the player, just put here a number greater than total card
    int unused_card_count;//the remaining deck card when the player win. It is used to give the bonus score "score_for_each_deck_card_spared"
    public bool first_card_is_always_useful;
    public bool never_give_same_card_rank_twice_in_a_row;
    public bool play_until_no_more_cards_then_check_win_condition;//instead to win when reach the win condition, continue the game until no move avaibles, then calculate the player outcome 

    public int number_of_countdown_card;
    public card[] countdown_card;

    [Header("Custom Deck")]
    public bool is_custom_card_on_deck;
    public card_class[] deck_custom_card;
    public DeckCustom deckCustom;   

    [Header("Left Final Card On Deck")]
    public Image end_card_image;
    public SpriteRenderer deck_sprite_except_final_card;
    public Transform deck_final_card;
    public CardEndGame endGameCard;
    public SpriteRenderer spriteRenderThisCard;
    public Animator endGameCardAnim;
    [SerializeField]
    public bool canOpenEndCard = true;
    public bool isOpenEndCard;

    [Header("Add Card In Deck")]
    public int numberOfAddCard;
    [Header("Coin")]
    public Text coin_current_text;

    [Header("Board")]
    public Board this_board;
    //public Transform board;//the child of this transform are the card on board
                           //how know is the player had collect all cards:
    public int current_total_card_on_board;
    int total_card_on_board;
    //padlock
    public int current_total_padlock_hp_on_board;
    int total_padlock_hp_on_board;
    //royal padlock
    int current_royal_padlock_hp_on_board;
    int total_royal_padlock_hp_on_board;

    //these variables aim to avoid to have too many cards with the same rank
    public int max_number_of_card_with_the_same_rank_on_board;
    public int[] card_on_board_by_rank;

    card[] cards_on_board;
    private bool win_condition_satisfied_in_this_board;//this is need to erase the progress when replay same board in multy-board scenario

    public int collect_all_cards_in_n_boards_target;

    private int collect_all_cards_in_n_boards_current_count;
    public int collect_all_gold_cards_in_n_boards_target;
    private int collect_all_gold_cards_in_n_boards_current_count;
    private bool all_gold_cards_collected_in_this_board;
    public enum win_condition_must_be_reach
    {
        independently_in_each_board
    }
    public win_condition_must_be_reach win_condition_must_be_reach_selected = win_condition_must_be_reach.independently_in_each_board;
    //win condition rules
    public enum win_condition
    {
        collect_all_cards,
        reach_target_score,
        reach_this_combo_multiplier,
        reach_this_combo_length,
        reach_this_combo_sum,
        reach_this_star_score_sum,
        collect_all_cards_n_in_times,
        collect_card_at_time_on_card_page
    }
    [Header("Win Condition")]
    public win_condition win_condition_selected;

    public bool is_gameover_time_on_card;

    public float time_collect_all_card;
    private float time_collect_all_card_start;
    public float reach_this_combo_multiplier_target;
    public int reach_this_combo_length_target;
    public int reach_this_combo_sum_target;
    public int collect_n_gold_cards_target;
    public int target_star_score_sum;

    private bool win_conditions_satisfied;
    public int number_of_win_conditions;
    public enum win_conditions_indispensable
    {
        all,
        just_one
    }
    public win_conditions_indispensable win_conditions_indispensable_selected = win_conditions_indispensable.all;

    public int win_target_score;
    //public Slider target_score_slider;//the GUI slider that show the player progress
    public Text target_score_text;
    public GameObject time;
    public GameObject score;
    public Text time_text;
    public bool isGameOver;

    //random rules
    public enum distribute_card_metod
    {
        none,
        random_roll_for_each_card,//there will be a random roll for each card, and if the roll is under the target number, the card will have the trait
        as_percentage_on_total_cards//for sure a percentage of all the card will have the trai
    }
    public distribute_card_metod distribute_card_metod_for_turn_up_a_covered_card;
    public int chance_of_turn_up_a_covered_card_at_start;//usually card covered by other card are face down, but you can roll for each a chance from 0 to 100 to turn up a covered card
    public int percentage_of_turn_up_a_covered_card_at_start;
    
    [Header("Wild Card")]
    public distribute_card_metod distribute_card_metod_for_wild_card_on_board;
    public int chance_of_wild_card_on_board;
    public int percentage_of_wild_card_on_board;

    public distribute_card_metod distribute_card_metod_for_wild_card_on_deck;
    public int chance_of_wild_card_on_deck;
    public int percentage_of_wild_card_on_deck;

    [Header("Normal Card")]
    public distribute_card_metod distribute_card_metod_for_joker_on_board;
    public int chance_of_joker_on_board;
    public int percentage_of_joker_card_on_board;

    public distribute_card_metod distribute_card_metod_for_joker_on_deck;
    public int chance_of_joker_on_deck;
    public int percentage_of_joker_card_on_deck;
    
    [Header("Bonus")]
    public Transform bonus_container;
    public GameObject bonus_button_obj;
    public bonus_button[] bonus_button_script;
    public enum bonus_type
    {
        none = -1,
        destroy_one = 0, //destroy one specific card
        destroy_some_card = 1, //destroy some card casually
        bonus_key = 2, //remove key-padlock from a card
        shuffle = 3, //Shuffle the card on board
        add_card_in_deck = 4 //Add card in deck
    }
    [Header("Canvas Bonus Show On Cards")]
    public GameObject canvas_bonus_add_card;
    [Header("----------------------------")]

    public bonus_type current_bonus_selected = bonus_type.none;
    public bonus_type previous_bonus_selected = bonus_type.none;//needed for undo
    public int destroy_some_card_quantity;
    public bool[] this_bonus_allows_undo;
    public int[] current_bonus_quantity;//[0] = destroy_one; [1] = destroy_some_card; [2] = bonus_key; [3] = shuffle
    public int[] max_bonus_quantity;
    public bool restore_bonus_quantity_at_restart;
    int[] start_bonus_quantity;
    public Sprite[] bonus_icons;
    public Sprite[] bonus_cards_sprites;
    public card[] latest_cards_affected_by_the_bonus;//needed for undo
    card[] shuffle_array;//the cards to shuffle
                         //shuffle rules
    public bool can_shuffle_normal_padlock;
    public bool can_shuffle_royal_padlock;
    public bool can_shuffle_keyhole_padlock;
    public bool can_shuffle_gold_card;
    public bool can_shuffle_wild_card;
    public bool can_shuffle_collectable_card;


    //combo
    int current_combo_sum_at_board_start;
    bool combo_sum_bonus_obtained;
    int total_combo_sum;//the total of all combo
    public int combo_sum_target_to_trigger_the_score_bonus; //when  current_combo_sum >= combo_sum_target_to_trigger_the_score_bonus, score += give_this_score_bonus_when_reach_the_combo_sum
    public int min_combo_lenght_to_trigger_the_sum;
    public int give_this_score_bonus_when_reach_the_combo_sum;
    public int min_combo_lenght_to_trigger_the_multiplier = 1;
    public GameObject end_combo_obj;// show the "end combo" text over the deck
    int combo_count;
    int previous_combo_count;//for undo
    public Text combo_text;
    public float combo_score_multiplier;
    int same_color_combo;//how many card with the same color in a row
    public float color_combo_score_multiplier;
    int same_suit_combo;//how many card with the same suit in a row
    public float suit_combo_score_multiplier;
    public Text total_combo_multiplier_text;
    float score_multiplier = 0;
    float undo_score_multiplier = 0;
    public enum keep_best_combo_multiplier
    {
        no,
        until_main_combo_end,
        always
    }
    public keep_best_combo_multiplier keep_best_combo_multiplier_selected;
    float best_combo_multiplier;
    int best_combo;

    [Header("Combo Sum")]
    public Slider combo_sum_slider;
    public Text combo_sum_count_text;
    public Text combo_sum_bonus_text;

    [Header("Score")]
    public StarScore star_score_bar;
    int score_gained_in_previous_boards;
    public int current_score;
    public int undo_previous_score;
    public Text score_text;
    public int normal_card_score;
    public int gold_card_score;
    public int bottom_card_score;
    public int score_for_each_deck_card_spared;
    public int target_deck_card_score;
    public enum star_score
    {
        no,
        less_card_on_board_more_stars_you_gain,
        gain_one_star_for_each_win_condition_satisfied, //require 3 win conditions
        gain_one_star_for_each_score_steps_achieved
    }
    public star_score star_score_selected = star_score.no;
    //for lose_one_star_for_each_card_left_on_board:
    public int[] card_left_on_board_to_gain_n_stars;
    public int[] star_score_steps;
    public int current_star_score;//the score of [this board]
    int total_star_score;//the sum of all the scores in the board array
    public bool all_cards_collected_in_this_board;//all cards collected in [this board]


    public bool game_started;//game autosetup itself, then set game_started = true and player_can_move = true so player can play
    public bool game_end;//true no more moves or win condition reached
    bool last_board;
    bool player_win;//if true when game_end is true, show win screen, then lose screen
    [HideInInspector]
    public bool player_can_move;//it is false before game start, after game end and through card animations

    public static deck this_deck;//this script
    public float animation_duration = 0.25f;
    //public float enhanced_animation_duration = 1f;
    public float rotation_duration = 0.25f;
    public bool enhanced_animation;
    public AnimationCurve verticalVariation;
    public float verticalVariationMultiplier;

    [Header("Bonus Add Card In Deck")]
    public bool has_Bonus_Add_Card_In_Deck;

    [Header("Sell Wild Card")]
    public bool hasSellWildCard;
    public SellWildCard wildCard;

    [Header("Streak")]
    public bool use_streak;
    public int count_of_streak_bar;
    public int current_streak_bar_using;
    public Streak streak_bar;
    [System.Serializable]
    public struct Streak_class
    {
        public int max_count_streak_want;
        public int coin_you_can_got;
    }
    public Streak_class[] streaks;

    public int count_streak;
    public bool is_reset_count_streak;
    public int deck_card_left;//how many card the player can ask

    public int deck_card_left_use_real; // if hasBonusAddCardInDeck = true กรณีมี bonus card เพิ่มการ์ดในสำรับ
    public int deck_card_left_all; //if hasBonusAddCardInDeck = true กรณีมี bonus card เพิ่มการ์ดในสำรับ

    int start_deck_card_left;
    public Text deck_card_left_text;
    int current_deck_position;//the current position in the deck array
    int current_target_deck_position;//the current position in the target deck array

    [System.Serializable]
    public class card_class
    {
        public int suit = -99;  //0 = clubs; 1 = spades; 2 = diamonds; 3 = hearts; 100 = neutre; 101 = red; 102 = black
        public int rank = -99;

        public enum card_type
        {
            normal,
            //gold, //this if for the win_condition_selected = collect_all_gold_cards into deck script
            wild, //can be collect in a separate deck (wild deck) and use when needed
            collectable_bonus

        }
        public card_type card_type_selected = card_type.normal;
        public bonus_type collectable_bonus_selected = bonus_type.none;

        public enum joker_type
        {
            none,
            normal,
            //red,
            //black
        }
        public joker_type joker_type_selected = joker_type.none;

        public card board_card = null;
    }
    public bool allowCheatWhenGiveANewCard = true;
    [Header("Bucket Decks")]
    public List<card_class> bucketDecks;//cards on board and bottom deck will take card from here when game start

    public card_class selected_card;
    public card_class current_deck_card;
    card_class previous_card;
    enum match_combo
    {
        none = -1,
        clubs = 0,
        spades = 1,
        diamonds = 2,
        hearts = 3,
        red = 101,
        black = 102
    }
    match_combo previous_match_combo;
    public Target_deck target_deck_scrip;

    public card_class[] deck_cards;//store the cards in the deck
    public List<card_class> target_deck_cards;

    [Header("Cards")]
    public Image background_image;
    public SpriteRenderer target_new_card;//the new current card to show
    public SpriteRenderer new_card_anim_sprite;//the card to show in the animantion from deck to target deck
    public Animation new_card_anim;//animation from deck to target deck

    public Sprite card_back_sprite;
    public Sprite gold_card_back_sprite;

    public int total_gold_cards; //the total number of gold card in this board
    int gold_cards_taken;//how many gold cards player have taken in this board
    int total_gold_cards_taken_in_all_boards; //the sum of all gold card taken in all boards
    //public Text gold_cards_count_text;

    public bool useParticles;
    public GameObject[] particlesObj;

    //black
    public Sprite[] clubs;
    public Sprite[] spades;

    //red
    public Sprite[] diamonds;
    public Sprite[] hearts;

    //gold version:
    //black
    public Sprite[] gold_clubs;
    public Sprite[] gold_spades;

    //red
    public Sprite[] gold_diamonds;
    public Sprite[] gold_hearts;

    //special cards
    public wild_card_deck my_wild_card_deck;
    public Sprite[] wild;

    public Sprite normal_joker;
    public Sprite red_joker;
    public Sprite black_joker;
    public Sprite gold_normal_joker;
    public Sprite gold_red_joker;
    public Sprite gold_black_joker;
    public Sprite wild_joker;

    public Sprite padlock_sprite;
    public Sprite royal_padlock_sprite;
    public Sprite keyhole_padlock_sprite;
    public Sprite padlock_card;


    //wild card deck (these values will be pass to wild_card_deck.cs when game start in order to keep all setting in the same inspector pannel)
    public int max_wild_cards_in_wild_deck;
    public float wild_deck_spacing_x;
    public float wild_deck_spacing_y;

    public Sprite[,] card_deck_sprite;//suit,rank
    public Sprite[,] gold_card_deck_sprite;//suit,rank


    //undo
    [Header("Undo")]
    public GameObject lastest_deactivate_board_card;
    public Button undo_button;
    public Text undo_count_text;
    public bool restore_undo_charges_when_go_to_next_board;
    public bool restore_undo_charges_when_retry;
    public int undo_charges;
    int start_undo_charges;
    public enum last_move
    {
        none,
        ask_new_card_from_deck,
        take_card_from_board_and_put_it_into_target_deck,
        take_card_from_board_and_put_it_into_wild_deck,
        take_card_from_wild_deck,
        damage_padlock,
        use_bonus,
        cards_matched
    }
    public last_move current_last_move = last_move.none;


    public Transform ghost_card;//it is use to show an animation when a card pass from board to target deck or conversely
    public SpriteRenderer ghost_card_SpriteRenderer;
    public Sprite card_selected_sprite;

    //GUI screen
    [Header("GUI screen")]
    public GameObject game_screen;
    public end_screen my_end_screen;
    public float show_win_or_lose_screen_after_delay;
    //goal window
    [Header("Goal Window")]
    bool goals_window_is_open;
    public enum show_goals_window_at_start
    {
        no,
        only_on_first_board,
        always
    }
    public show_goals_window_at_start show_goals_window_at_start_selected = show_goals_window_at_start.only_on_first_board;
    public bool show_goal_window_at_each_win_condition_satisfied;
    public GameObject goals_screen;
    private goal_toggle goal_toggle_list;
    public Transform goals_screen_list;
    public GameObject goal_toggle;


    [Header("SFX")]
    //sfx
    public AudioClip take_this_card_start_sfx;//take a card from the board
    public AudioClip take_this_card_end_sfx;//take a card from the board
    public AudioClip destroy_this_card_sfx;
    public AudioClip blocked_card_sfx; //this card have something over it
    public AudioClip wrong_card_sfx; //wrong card rank
    public AudioClip new_card_sfx; //take a new card from the deck
    public AudioClip win_sfx;
    public AudioClip reach_goal_sfx;
    public AudioClip lose_sfx;
    public AudioClip gui_button_sfx;

    [Header("Editor")]
    //editor (these bool open and close the menus of this script in inspector
    public bool editor_show_general_rules;
    public bool editor_show_board_array;
    public bool editor_show_max_streak_want_array;
    public bool editor_show_joker_rules;
    public bool editor_show_wild_cards_rules;
    public bool editor_show_combo_rules;
    public bool editor_show_score_rules;
    public bool editor_show_bonus_rules;
    public bool editor_show_sprites;
    public bool editor_show_sprites_normal;
    public bool editor_show_sprites_normal_clubs;
    public bool editor_show_sprites_normal_spades;
    public bool editor_show_sprites_normal_diamonds;
    public bool editor_show_sprites_normal_hearts;
    public bool editor_show_sprites_gold;
    public bool editor_show_sprites_gold_clubs;
    public bool editor_show_sprites_gold_spades;
    public bool editor_show_sprites_gold_diamonds;
    public bool editor_show_sprites_gold_hearts;
    public bool editor_show_sprites_wild;
    public bool editor_show_sprites_bonus_cards;
    public bool editor_show_sprites_padlock;
    public bool editor_show_icon_bonus;
    public bool editor_show_sfx;
    public bool editor_show_advanced;
    public bool editor_show_gui;
    public bool editor_show_animations;
    public bool editor_show_particles;
    public bool editor_show_countdown_card;
    public bool editor_show_end_card;

    void Awake()//variables to initiate only one time for scene
    {
        if (GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.number_of_win_conditons[GameMaster.my_GameMaster.current_stage] = number_of_win_conditions;
            coin_current_text.text = ""+ GameMaster.my_GameMaster.number_coin_got;
        }

        bucketDecks = new List<card_class>();
        target_deck_scrip = target_new_card.gameObject.GetComponent<Target_deck>();
        ghost_card_SpriteRenderer = ghost_card.GetComponent<SpriteRenderer>();

        win_conditions_satisfied = false;
        total_star_score = 0;
        best_combo = 0;

        //setup end screen
        my_end_screen.Setup_recap_list(false);

        //combo sum
        if (give_this_score_bonus_when_reach_the_combo_sum > 0)
        {
            //combo_sum_bonus_text.text = give_this_score_bonus_when_reach_the_combo_sum.ToString("N0");
            //combo_sum_slider.maxValue = combo_sum_target_to_trigger_the_score_bonus;
            Update_combo_sum_gui();
            //combo_sum_slider.transform.parent.gameObject.SetActive (false); //บังคับไม่ให้แสดง
        }
        //else
            //combo_sum_slider.transform.parent.gameObject.SetActive (false);

            //setup wild deck
            my_wild_card_deck.max_wild_cards = max_wild_cards_in_wild_deck;
        my_wild_card_deck.spacing_x = wild_deck_spacing_x;
        my_wild_card_deck.spacing_y = wild_deck_spacing_y;

        this_deck = this;//this script

        if (GameMaster.my_GameMaster && GameMaster.my_GameMaster.skinManager && GameMaster.my_GameMaster.skinManager.enabled)
        {
            //print(GameMaster.my_GameMaster.skinManager.backgrounds.Length + " ... "  + PlayerPrefs.GetInt("skin_selected_background"));
            if (GameMaster.my_GameMaster.skinManager.player_can_choose_background)
            {
                background_image.sprite = GameMaster.my_GameMaster.skinManager.backgrounds[PlayerPrefs.GetInt("skin_selected_background")];
            }
            Update_card_skin(GameMaster.my_GameMaster.skinManager.deckSprite[PlayerPrefs.GetInt("skin_selected_card")], GameMaster.my_GameMaster.skinManager.cards_back[PlayerPrefs.GetInt("skin_selected_card_back")]);
        }
        else
        {
            Generate_card_deck_sprite();//fuse clubs[], spades[], diamonds[] and hearts[] in one array
        }

        //setup deck sprite
        GetComponent<SpriteRenderer>().sprite = card_back_sprite;
        new_card_anim.transform.GetChild(0).transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = card_back_sprite;

        start_undo_charges = undo_charges;

        deckCustom = GetComponent<DeckCustom>();
        deckCustom.countCardAll = deckCustom.deck_custom_card_list.Count;
        deck_card_left = deckCustom.countCardPlay;

        if (!has_Bonus_Add_Card_In_Deck)
        {
            start_deck_card_left = deck_card_left;
        }
        else
        {
            deck_card_left = deck_card_left_all;
            start_deck_card_left = deck_card_left;
        }

        if (GameMaster.my_GameMaster)
        {
            Load_from_gamemaster();
        }

        //bonus
        if (bonus_container.childCount > 0)//erase container
        {
            for (int i = 0; i < bonus_container.childCount; i++)
                Destroy(bonus_container.GetChild(i));
        }
        start_bonus_quantity = new int[current_bonus_quantity.Length];
        bonus_button_script = new bonus_button[current_bonus_quantity.Length];
        //print("current_bonus_quantity.Length : "+current_bonus_quantity.Length);
        for (int i = 0; i < current_bonus_quantity.Length; i++)
        {
            start_bonus_quantity[i] = current_bonus_quantity[i];
            //generate GUI
            GameObject temp_bonus_button = Instantiate(bonus_button_obj);
            temp_bonus_button.transform.SetParent(bonus_container, false);
            bonus_button_script[i] = temp_bonus_button.GetComponent<bonus_button>();
            bonus_button_script[i].icon.sprite = bonus_icons[i];
            bonus_button_script[i].my_id = i;
        }

        Read_board();
        //print("win_target_score : " + win_target_score);
        //star_score_bar.SetupStarScore(this, win_target_score, current_score);

        if (use_streak)
        {
            current_streak_bar_using = 0;
            streak_bar.gameObject.SetActive(true);
            streak_bar.Set_Start_Streak(this, count_of_streak_bar, streaks, current_streak_bar_using);
            streak_bar.Start_Show_Streak(current_streak_bar_using);
        }
        else
        {
            streak_bar.gameObject.SetActive(false);
        }

        //this_board = board.GetComponent<Board>();
        countdown_card = this_board.card_count_down;

        Check_final_card_left();

        //print("current score [Awake] : " + current_score);
        //for (int i = 0; i < star_score_steps.Length; i++)
        //{
        //    print("star_score_steps "+i+" : "+star_score_steps[i]);
        //}
        
    }

    [SerializeField]
    private DeckCustom.custom_card newCard;

    // Use this for initialization
    void Start()
    {
        if (GameManager.instance)
        {
            my_gameManager = GameManager.instance;
        }

        Restart(false);//variables that need to be initiated at each play (false mean that this is the first time, not a restart)

        isGameOver = false;

        count_streak = 0;

        time_collect_all_card_start = time_collect_all_card;

        if (GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.game);
        }

        if(hasSellWildCard)
        {
            if (wildCard)
            {
                wildCard.SetSellWildCard(this);
            }
        }
    }

    private void Update()
    {
        if (game_started)
        {
            switch (win_condition_selected)
            {
                case deck.win_condition.collect_all_cards_n_in_times:
                    if (!isWinOnTime && time_collect_all_card > 0)
                    {
                        time.SetActive(true);
                        time_collect_all_card -= Time.deltaTime;
                        time_text.text = "" + (int)time_collect_all_card;
                    }
                    else if (!isWinOnTime && time_collect_all_card <= 0)
                    {
                        if (!isGameOver)
                        {
                            isGameOver = true;
                            Show_end_screen();
                        }
                    }
                    break;
                case deck.win_condition.collect_card_at_time_on_card_page:
                    for (int j = 0; j < countdown_card.Length; j++)
                    {
                        if (countdown_card[j].is_gameover_timeout_card)
                        {
                            if (!isGameOver)
                            {
                                if (!is_gameover_time_on_card)
                                {
                                    print("is_gameover_time_on_card");
                                    is_gameover_time_on_card = true;
                                }
                                isGameOver = true;
                                Show_end_screen();
                                return;
                            }
                        }
                        //else
                        //{
                        //    is_gameover_time_on_card = false;
                        //}
                    }
                    break;
            }

            if(GameMaster.my_GameMaster)
            {
                coin_current_text.text = "" + GameMaster.my_GameMaster.number_coin_got;
            }

            if (deck_card_left == 1)//if this was the last card
            {
                spriteRenderThisCard.enabled = false;
            }
            else
            {
                spriteRenderThisCard.enabled = true;
            }

            if (canOpenEndCard)
            {
                print("deck_card_left : " + deck_card_left);
                if (deck_card_left <= 1)
                {
                    if (!isOpenEndCard)
                    {
                        print("is Open End Card");
                        endGameCard.SetCardEndGame(this);
                        isOpenEndCard = true;
                    }
                }
            }


            if (use_streak)
            {
                if (streak_bar.is_completed_streak)
                {
                    if (count_of_streak_bar >= 0)
                    {
                        streak_bar.Reset_All_Point_Streak();
                        //print("current_streak_bar_using : " + current_streak_bar_using);
                        streak_bar.Start_Show_Streak(current_streak_bar_using);

                        streak_bar.is_completed_streak = false;
                    }
                }
            }
            else
            {
                streak_bar.gameObject.SetActive(false);
            }

        }

        if(isChangeCurrentCardToWild)
        {
            ChangeCurrentCardTargetToWild();
            isChangeCurrentCardToWild = false;
        }

    }

    public bool isChangeCurrentCardToWild;
    public void IsChangeCurrentCardToWild(bool isChange)
    {
        isChangeCurrentCardToWild = isChange;
    }

    public void ChangeCurrentCardTargetToWild()
    {
        print("ChangeCurrentCardTargetToWild");
        deck_cards[current_deck_position].rank = 0;
        deck_cards[current_deck_position].suit = 0;
        deck_cards[current_deck_position].joker_type_selected = card_class.joker_type.normal;
        deck_cards[current_deck_position].collectable_bonus_selected = bonus_type.none;
        deck_cards[current_deck_position].board_card = null;

        selected_card = deck_cards[current_deck_position];

        target_new_card.sprite = Assign_the_sprite_of_this_card(selected_card);
    }

    private int count_card_want_add_in_deck;
    public void Use_Bonus_Add_Card_In_Deck(int count_Cards_Add_In_Deck)
    {
        count_card_want_add_in_deck = count_Cards_Add_In_Deck;
        deck_card_left += count_Cards_Add_In_Deck;
        deck_card_left_use_real = deck_card_left;
        deck_card_left_text.text = "" + (deck_card_left - 1);
        if(GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.Start_Show_Debug_Text("Add " + count_Cards_Add_In_Deck + " cards in your deck!", 2);
        }
    }

    #region board generation
    //   void Load_board(int board_number)
    //{
    //	//load new board
    //	GameObject new_board = Instantiate(board_array[board_number], board.position, Quaternion.identity) as GameObject;

    //	new_board.transform.SetParent(this.transform.parent);
    //	Destroy (board.gameObject);
    //	board = new_board.transform;
    //}

    public void Update_card_skin(DeckSprites new_deckSprites, Sprite new_card_back)
    {
        //update sprites
        card_back_sprite = new_card_back;


        normal_joker = new_deckSprites.normal_joker;
        red_joker = new_deckSprites.red_joker;
        black_joker = new_deckSprites.black_joker;

        gold_normal_joker = new_deckSprites.gold_normal_joker;
        gold_red_joker = new_deckSprites.gold_red_joker;
        gold_black_joker = new_deckSprites.gold_black_joker;

        wild_joker = new_deckSprites.wild_joker;

        padlock_sprite = new_deckSprites.padlock_sprite;
        royal_padlock_sprite = new_deckSprites.royal_padlock_sprite;
        keyhole_padlock_sprite = new_deckSprites.keyhole_padlock_sprite;
        padlock_card = new_deckSprites.padlock_card;

        for (int i = 0; i < 4; i++)
            bonus_cards_sprites[i] = new_deckSprites.bonus_cards_sprites[i];

        for (int i = 0; i < 13; i++)
            wild[i] = new_deckSprites.wild[i];

        card_deck_sprite = new Sprite[4, 13];
        gold_card_deck_sprite = new Sprite[4, 13];

        for (int suit = 0; suit < 4; suit++)
        {
            for (int rank = 0; rank < 13; rank++)
            {
                if (suit == 0)
                {
                    card_deck_sprite[suit, rank] = new_deckSprites.clubs[rank];
                    gold_card_deck_sprite[suit, rank] = new_deckSprites.gold_clubs[rank];
                }
                else if (suit == 1)
                {
                    card_deck_sprite[suit, rank] = new_deckSprites.spades[rank];
                    gold_card_deck_sprite[suit, rank] = new_deckSprites.gold_spades[rank];
                }
                else if (suit == 2)
                {
                    card_deck_sprite[suit, rank] = new_deckSprites.diamonds[rank];
                    gold_card_deck_sprite[suit, rank] = new_deckSprites.gold_diamonds[rank];
                }
                else if (suit == 3)
                {
                    card_deck_sprite[suit, rank] = new_deckSprites.hearts[rank];
                    gold_card_deck_sprite[suit, rank] = new_deckSprites.gold_hearts[rank];
                }
            }
        }
    }

    public void Update_sprites_in_existing_cards()
    {
        //deck
        GetComponent<SpriteRenderer>().sprite = card_back_sprite;
        new_card_anim.transform.GetChild(0).transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = card_back_sprite;
        target_new_card.sprite = Assign_the_sprite_of_this_card(target_deck_cards[target_deck_cards.Count - 1]);

        //wild deck
        my_wild_card_deck.Update_sprites();

        //board
        for (int i = 0; i < total_card_on_board; i++)
            cards_on_board[i].Update_card_appearance();

    }

    void BuildBucketDecks(int totalCardNeeded)
    {
        //bucketDecks.Clear();

        int suitCount = 0;
        int rankCount = 0;

        for (int i = 0; i < totalCardNeeded; i++)
        {
            card_class tempCard = new card_class();

            tempCard.suit = suitCount;
            tempCard.rank = rankCount;
            bucketDecks.Add(tempCard);

            //print("BuildBucketDecks : "+i + " = " + tempCard.suit + "," + tempCard.rank);

            //rankCount++;
            //if (rankCount >= 13)
            //{
            //    rankCount = 0;
            //    suitCount++;

            //    if (suitCount >= 4)
            //        suitCount = 0;
            //}
        }
    }
    public bool isAddNewCardInDeck;
    void CreateBucketDecks()
    {
        bucketDecks.Clear();
        //print("total_card_on_board : " + total_card_on_board + " | deck_card_left : " + deck_card_left);
        int totalCardNeeded = total_card_on_board + deck_card_left;

        BuildBucketDecks(totalCardNeeded);

        //print("CreateBucketDecks() " + totalCardNeeded);

        //remove carda assigned manually
        for (int i = 0; i < total_card_on_board; i++)
        {
            if (cards_on_board[i].my_setup_selected == card.my_setup.manual && cards_on_board[i].card_suit_selected != card.card_suit.random && cards_on_board[i].card_rank_selected != card.card_rank.random)
                RemoveCardFromBucketDecks((int)cards_on_board[i].card_suit_selected, (int)cards_on_board[i].card_rank_selected);

        }

        //print("bucketDecks.Count: " + bucketDecks.Count);

        //for (int i = 0; i < bucketDecks.Count; i++)
        //{
        //    print("{"+i+"} "+ "suit : " + bucketDecks[i].suit + " | rank : " + bucketDecks[i].rank + " | type : " + bucketDecks[i].card_type_selected + " | board : " + bucketDecks[i].board_card);
        //}
    }

    void RemoveCardFromBucketDecks(int suit, int rank)
    {
        //print("Try to remove " + suit + "," + rank + " from bucketDecks " + bucketDecks.Count);
        for (int i = 0; i < bucketDecks.Count; i++)
        {
            if (bucketDecks[i].suit == suit && bucketDecks[i].rank == rank)
            {
                bucketDecks.RemoveAt(i);
                //print("    ...card " + bucketDecks[i].suit + "," + bucketDecks[i].rank + " removed from BucketDecks because already manually assigned on board");
                return;
            }
        }

        //print("   ...no card removable found");
    }

    bool OnlyKLeftInBucketDecks()
    {
        for (int i = 0; i < bucketDecks.Count; i++)
        {
            if (bucketDecks[i].rank != 12)
                return false;

        }

        return true;
    }

    public card_class PickRandomCardFromBucketDecks(bool avoidK)
    {
        //print("PickRandomCardFromBucketDecks");

        if (avoidK && OnlyKLeftInBucketDecks())//you need a new deck!
            BuildBucketDecks(52);

        if (bucketDecks.Count == 0)//you need a new deck!
            BuildBucketDecks(52);

        card_class returnThis = new card_class();

        int randomPick = Random.Range(0, bucketDecks.Count);
        int selectedCard = -99;

        //print("PickRandomCardFromBucketDecks() : " + avoidK);
        selectedCard = randomPick;
        if (selectedCard != -99)
        {
            returnThis.suit = bucketDecks[selectedCard].suit;
            returnThis.rank = bucketDecks[selectedCard].rank;
            bucketDecks.RemoveAt(selectedCard);

            //print("      = " + returnThis.suit + "," + returnThis.rank);

            return returnThis;
        }

        Debug.LogWarning("Can't find a card to pick!");
        return null;
    }

    int count_add_cards = 0;
    void Read_board()
    {
        //print("Read_board");

        total_card_on_board = this_board.transform.childCount; //count cards on board
        cards_on_board = new card[total_card_on_board];
        total_padlock_hp_on_board = 0;
        total_royal_padlock_hp_on_board = 0;


        for (int i = 0; i < total_card_on_board; i++)
        {
            cards_on_board[i] = this_board.transform.GetChild(i).GetComponent<card>();//put all board card script in an array to quick reference
            if (cards_on_board[i].padlock_model_selected == card.padlock_model.normal)
            {
                total_padlock_hp_on_board += cards_on_board[i].padlock_hp;
            }
            else if (cards_on_board[i].padlock_model_selected == card.padlock_model.royal)
            {
                total_royal_padlock_hp_on_board += cards_on_board[i].padlock_hp;
            }

            if (cards_on_board[i].collectable_bonus_selected == bonus_type.add_card_in_deck)
            {
                count_add_cards += cards_on_board[i].count_Add_Card_In_Deck;
            }
        }

        deck_card_left_use_real = deck_card_left_all - count_add_cards;

        max_number_of_card_with_the_same_rank_on_board = Mathf.CeilToInt(total_card_on_board / 13);//decide the max number of card with the same rank to put on board

        CreateBucketDecks();

        //Debug.Log("board cards = " + total_card_on_board + " ...max same rank = " + max_number_of_card_with_the_same_rank_on_board + "... total normal padlock hp = " + total_padlock_hp_on_board + " ...total_royal_padlock_hp_on_board = " + total_royal_padlock_hp_on_board);
    }

    void Restart(bool after_reset)
    {
        //print("Restart");
        //reset variables
        previous_match_combo = match_combo.none;
        current_deck_card = new card_class();
        target_deck_cards = new List<card_class>();
        target_deck_scrip.garbage_cards = new List<card_class>();
        last_board = false;
        win_condition_satisfied_in_this_board = false;
        all_gold_cards_collected_in_this_board = false;
        game_started = false;
        game_end = false;
        player_win = false;
        player_can_move = false;
        current_total_card_on_board = total_card_on_board;
        //Debug.Log("current_total_padlock_hp_on_board = " + current_total_padlock_hp_on_board + " ... total_padlock_hp_on_board: " + total_padlock_hp_on_board);
        current_total_padlock_hp_on_board = total_padlock_hp_on_board;
        //Debug.Log("current_royal_padlock_hp_on_board: " + current_royal_padlock_hp_on_board + " ... total_royal_padlock_hp_on_board: " + total_royal_padlock_hp_on_board);
        current_royal_padlock_hp_on_board = total_royal_padlock_hp_on_board;
        total_gold_cards = 0;
        current_deck_position = 0;
        current_target_deck_position = 0;
        unused_card_count = 0;
        undo_previous_score = 0;
        //bonus
        if (restore_bonus_quantity_at_restart && win_condition_must_be_reach_selected == win_condition_must_be_reach.independently_in_each_board)
        {
            for (int i = 0; i < current_bonus_quantity.Length; i++)
            {
                if (current_bonus_quantity[i] > max_bonus_quantity[i])
                    current_bonus_quantity[i] = max_bonus_quantity[i];

                current_bonus_quantity[i] = start_bonus_quantity[i];
            }
        }
        previous_bonus_selected = bonus_type.none;
        current_bonus_selected = bonus_type.none;
        Refresh_bonus_buttons();
        //combo
        if (win_condition_must_be_reach_selected == win_condition_must_be_reach.independently_in_each_board)
        {
            total_combo_sum = 0;
            combo_sum_bonus_obtained = false;
        }
        else
            total_combo_sum = current_combo_sum_at_board_start;
        //current_combo_sum = 0;
        combo_sum_bonus_obtained = false;
        previous_combo_count = 0;
        combo_count = 0;
        score_multiplier = 0;
        undo_score_multiplier = 0;
        
        best_combo_multiplier = 0;
        
        //gold cards
        gold_cards_taken = 0;
        Update_gold_cards_count();
        //score
        current_star_score = 0;
        if (This_win_conditon_is_active(win_condition.reach_target_score) && win_condition_must_be_reach_selected == win_condition_must_be_reach.independently_in_each_board)
        {
            current_score = 0;
        }

        score_gained_in_previous_boards = 0;


        score_text.text = current_score.ToString();

        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        target_new_card.gameObject.SetActive(false);
        my_end_screen.Show_end_screen(false);
        game_screen.SetActive(true);
        Update_undo_button(false);

        card_on_board_by_rank = new int[13];

        if (restore_undo_charges_when_go_to_next_board)
        {
            undo_charges = start_undo_charges;
            undo_count_text.text = undo_charges.ToString();
        }

        if (after_reset)//restart all board cards. Return in place all board card
        {
            if (restore_undo_charges_when_retry)
            {
                undo_charges = start_undo_charges;
                undo_count_text.text = undo_charges.ToString();
            }

            if (!has_Bonus_Add_Card_In_Deck)
            {
                deck_card_left = start_deck_card_left;
            }
            else
            {
                start_deck_card_left = deck_card_left;
                start_deck_card_left = deck_card_left_all;
                deck_card_left = start_deck_card_left;

                deck_card_left_use_real = deck_card_left_all - count_add_cards;
            }

            for (int i = 0; i < total_card_on_board; i++)
            {
                cards_on_board[i].Reset_me();
            }

            Setup_board_card_by_percentage();

            //this is a separate loop because Start_me() need that all in original start position in order to check if a card is cover or not
            for (int i = 0; i < total_card_on_board; i++)
            {
                cards_on_board[i].Start_me();
            }

            //reset wild deck
            my_wild_card_deck.Empty_deck();
            if (my_wild_card_deck.saved_deck_array.Length > 0) //ripristinate wild card if needed
                my_wild_card_deck.Ripristinate_wild_cards();

            endGameCard.ResetEndCard(this);
            star_score_bar.ResetScoreStarBar();
        }
        else
        {
            Setup_board_card_by_percentage();
        }

        //reset win condition count
        if (GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.win_conditions_satisfied[GameMaster.my_GameMaster.current_stage] = win_conditions_satisfied;
        }

        Update_combo_text();
        Update_combo_sum_gui();

        if (is_custom_card_on_deck)
        {
            SetCardDeckFollowCustomDeckList(false,0);
        }

        Fill_deck();

        star_score_bar.SetupStarScore(this, win_target_score, current_score, star_score_steps);

        //if (This_win_conditon_is_active(win_condition.reach_target_score))
        //{
        //    target_score_slider.gameObject.SetActive(true);
        //    target_score_slider.maxValue = win_target_score;
        //    target_score_text.text = "Target = " + win_target_score.ToString("N0");

        //    target_score_slider.value = current_score;

        //}
        //else
        //    target_score_slider.gameObject.SetActive(false);


        Setup_goals_window();
        if (!goals_window_is_open)
            Ask_new_card(false);


        //ads
        if (UnityAds.my_unity_ads)
        {
            //if (win_condition_must_be_reach_selected == win_condition_must_be_reach.independently_in_each_board || !after_reset)
            UnityAds.my_unity_ads.Check_if_show_unityADS();
        }

        if (GameMaster.my_GameMaster)
        {
            Save_in_gamemaster();
            GameMaster.my_GameMaster.Save_this_stage(GameMaster.my_GameMaster.current_stage);
        }

        //Streak Restart
        count_streak = 0;
    }


    void Setup_board_card_by_percentage()
    {
        //print("Setup_board_card_by_percentage");
        int random_start_point = 0;

        //turn up cards
        if (distribute_card_metod_for_turn_up_a_covered_card == distribute_card_metod.as_percentage_on_total_cards)
        {
            //calculate how many card turn up
            int number_of_cards_to_turn_up = Mathf.CeilToInt(total_card_on_board * (percentage_of_turn_up_a_covered_card_at_start * 0.01f));
            //turn up
            random_start_point = Random.Range(0, total_card_on_board);
            for (int i = random_start_point; i < total_card_on_board; i++)
            {
                if (number_of_cards_to_turn_up > 0)
                {
                    cards_on_board[i].causal_face_up = true;
                    number_of_cards_to_turn_up--;
                }
            }
            for (int i = 0; i < random_start_point; i++)
            {
                if (number_of_cards_to_turn_up > 0)
                {
                    cards_on_board[i].causal_face_up = true;
                    number_of_cards_to_turn_up--;
                }
            }
        }

        //wild cards
        if (distribute_card_metod_for_wild_card_on_board == distribute_card_metod.as_percentage_on_total_cards)
        {
            //calculate how many cards will be wild card
            int number_of_cards_to_make_wild = Mathf.CeilToInt(total_card_on_board * (percentage_of_wild_card_on_board * 0.01f));
            int span_between_cards_to_ad = Mathf.CeilToInt(total_card_on_board / number_of_cards_to_make_wild);
            int add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
            //make wild
            random_start_point = add_next_card_countdown;
            for (int i = random_start_point; i < total_card_on_board; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_cards_to_make_wild > 0)
                {
                    if (cards_on_board[i].my_setup_selected == card.my_setup.automatic)//avoid manual cards
                    {
                        cards_on_board[i].casual_wild = true;
                        cards_on_board[i].card_type_selected = card_class.card_type.wild;
                        number_of_cards_to_make_wild--;

                        add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    }
                }
            }
            for (int i = 0; i < random_start_point; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_cards_to_make_wild > 0)
                {
                    if (cards_on_board[i].my_setup_selected == card.my_setup.automatic)//avoid manual card
                    {
                        cards_on_board[i].casual_wild = true;
                        cards_on_board[i].card_type_selected = card_class.card_type.wild;
                        number_of_cards_to_make_wild--;

                        add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    }
                }
            }
        }

        //jokers
        if (distribute_card_metod_for_joker_on_board == distribute_card_metod.as_percentage_on_total_cards)
        {
            //calculate how many cards will be joker
            int number_of_cards_to_make_joker = Mathf.CeilToInt(total_card_on_board * (percentage_of_joker_card_on_board * 0.01f));
            int span_between_cards_to_ad = Mathf.CeilToInt(total_card_on_board / number_of_cards_to_make_joker);
            int add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
            random_start_point = add_next_card_countdown;
            for (int i = random_start_point; i < total_card_on_board; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_cards_to_make_joker > 0)
                {
                    if (cards_on_board[i].my_setup_selected == card.my_setup.automatic)//avoid manual card
                    {
                        number_of_cards_to_make_joker--;

                        cards_on_board[i].casual_joker = true;
                        cards_on_board[i].my_rank = -99;
                        if (cards_on_board[i].card_type_selected == deck.card_class.card_type.wild)//wild card can have only normal joker
                            cards_on_board[i].joker_type_selected = deck.card_class.joker_type.normal;
                        else
                        {
                            int random_joker_type = UnityEngine.Random.Range(1, 4);
                            cards_on_board[i].joker_type_selected = (deck.card_class.joker_type)random_joker_type;
                        }

                        add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    }
                }
            }
            for (int i = 0; i < random_start_point; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_cards_to_make_joker > 0)
                {
                    if (cards_on_board[i].my_setup_selected == card.my_setup.automatic)//avoid manual card
                    {
                        number_of_cards_to_make_joker--;

                        cards_on_board[i].casual_joker = true;
                        cards_on_board[i].my_rank = -99;
                        if (cards_on_board[i].card_type_selected == deck.card_class.card_type.wild)//wild card can have only normal joker
                            cards_on_board[i].joker_type_selected = deck.card_class.joker_type.normal;
                        else
                        {
                            int random_joker_type = UnityEngine.Random.Range(1, 4);
                            cards_on_board[i].joker_type_selected = (deck.card_class.joker_type)random_joker_type;
                        }

                        add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    }
                }
            }
        }
    }
    #endregion

    #region deck generation
    void Fill_deck()//decide what card put in the deck, but some can change if give_an_useful_card_not_less_than_n_new_card_from_the_deck is trigger
    {
        //print("Fill_deck()");
        deck_cards = new card_class[deck_card_left];
        int joker_type = 0;
        int cardRange = 13;
        bool avoidK = false;        

        for (int i = 0; i < deck_card_left; i++)
        {
            deck_cards[i] = new card_class();

            //wild or normal card?
            if (distribute_card_metod_for_wild_card_on_deck == distribute_card_metod.random_roll_for_each_card && UnityEngine.Random.Range(1, 100) <= chance_of_wild_card_on_deck)
            {
                deck_cards[i].card_type_selected = card_class.card_type.wild;
                deck_cards[i].suit = -99;
            }
            else//normal
            {
                deck_cards[i].card_type_selected = card_class.card_type.normal;
                deck_cards[i].suit = Random.Range(0, 4);
            }

            //joker or not?
            if (distribute_card_metod_for_joker_on_deck == distribute_card_metod.random_roll_for_each_card && UnityEngine.Random.Range(1, 100) <= chance_of_joker_on_deck)
            {
                if (deck_cards[i].card_type_selected == card_class.card_type.wild)
                    deck_cards[i].joker_type_selected = deck.card_class.joker_type.normal;
                else
                {
                    joker_type = UnityEngine.Random.Range(1, 4);
                    deck_cards[i].joker_type_selected = (deck.card_class.joker_type)joker_type;
                }
            }
            else
            {
                deck_cards[i].joker_type_selected = card_class.joker_type.none;
            }
            deck_cards[i].rank = Random.Range(0, cardRange);

        }

        //add wild card as percentage
        if (distribute_card_metod_for_wild_card_on_deck == distribute_card_metod.as_percentage_on_total_cards)
        {
            //calculate how many wild card add
            int number_of_wild_cards_to_add = Mathf.CeilToInt(deck_card_left * (percentage_of_wild_card_on_deck * 0.01f));
            int span_between_cards_to_ad = Mathf.CeilToInt(deck_card_left / number_of_wild_cards_to_add);
            int add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
            //add
            int start_random_point = add_next_card_countdown;
            for (int i = start_random_point; i < deck_card_left; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_wild_cards_to_add > 0)
                {
                    add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    number_of_wild_cards_to_add--;

                    deck_cards[i].card_type_selected = card_class.card_type.wild;
                    deck_cards[i].suit = -99;
                }
            }

            for (int i = 0; i < start_random_point; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_wild_cards_to_add > 0)
                {
                    add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    number_of_wild_cards_to_add--;

                    deck_cards[i].card_type_selected = card_class.card_type.wild;
                    deck_cards[i].suit = -99;
                }
            }
        }

        //add jokers by percentage
        if (distribute_card_metod_for_joker_on_deck == distribute_card_metod.as_percentage_on_total_cards)
        {
            //calculate how many jokers add
            int number_of_jokers_to_add = Mathf.CeilToInt(deck_card_left * (percentage_of_joker_card_on_deck * 0.01f));
            int span_between_cards_to_ad = Mathf.CeilToInt(deck_card_left / number_of_jokers_to_add);
            int add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
            //add
            int start_random_point = add_next_card_countdown;

            for (int i = start_random_point; i < deck_card_left; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_jokers_to_add > 0)
                {
                    add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    number_of_jokers_to_add--;

                    if (deck_cards[i].card_type_selected == card_class.card_type.wild)//wild card can have only normal joker
                        deck_cards[i].joker_type_selected = deck.card_class.joker_type.normal;
                    else
                    {
                        int random_joker_type = UnityEngine.Random.Range(1, 4);
                        deck_cards[i].joker_type_selected = (deck.card_class.joker_type)random_joker_type;
                    }
                }
            }

            for (int i = 0; i < start_random_point; i++)
            {
                add_next_card_countdown--;
                if (add_next_card_countdown <= 0 && number_of_jokers_to_add > 0)
                {
                    add_next_card_countdown = Random.Range(0, span_between_cards_to_ad + 1);
                    number_of_jokers_to_add--;

                    if (deck_cards[i].card_type_selected == card_class.card_type.wild)//wild card can have only normal joker
                        deck_cards[i].joker_type_selected = deck.card_class.joker_type.normal;
                    else
                    {
                        int random_joker_type = UnityEngine.Random.Range(1, 4);
                        deck_cards[i].joker_type_selected = (deck.card_class.joker_type)random_joker_type;
                    }
                }
            }
        }
        if (!has_Bonus_Add_Card_In_Deck)
        {
            deck_card_left_text.text = (deck_card_left - 1).ToString();
        }
        else
        {
            deck_card_left_text.text = (deck_card_left_use_real - 1).ToString();
        }

        if (is_custom_card_on_deck)
        {
            SetCardDeckFollowCustomDeckList(false,0);
        }

    }

    void Generate_card_deck_sprite()//fuse the card sprite arrays in one
    {
        card_deck_sprite = new Sprite[4, 13];
        gold_card_deck_sprite = new Sprite[4, 13];

        Sprite[] temp_rank = new Sprite[13];
        Sprite[] temp_rank_gold = new Sprite[13];

        for (int suit = 0; suit < 4; suit++)
        {
            if (suit == 0)
            {
                temp_rank = clubs;
                temp_rank_gold = gold_clubs;
            }
            else if (suit == 1)
            {
                temp_rank = spades;
                temp_rank_gold = gold_spades;
            }
            else if (suit == 2)
            {
                temp_rank = diamonds;
                temp_rank_gold = gold_diamonds;
            }
            else if (suit == 3)
            {
                temp_rank = hearts;
                temp_rank_gold = gold_hearts;
            }

            for (int rank = 0; rank < 13; rank++)
            {
                card_deck_sprite[suit, rank] = temp_rank[rank];
                gold_card_deck_sprite[suit, rank] = temp_rank_gold[rank];
            }
        }
    }

    #endregion

    #region bonus
    public void Refresh_bonus_buttons()
    {
        int total_bonus_count = 0;
        for (int i = 0; i < current_bonus_quantity.Length; i++)
        {
            if (current_bonus_quantity[i] < max_bonus_quantity[i])
                bonus_button_script[i].quantity_text.text = current_bonus_quantity[i].ToString();
            else
                bonus_button_script[i].quantity_text.text = "max";

            bonus_button_script[i].Show_me();

            total_bonus_count += current_bonus_quantity[i];

            bonus_button_script[i].select_icon.SetActive(false);
        }

        if (total_bonus_count > 0)
            bonus_container.gameObject.SetActive(true);
        else
            bonus_container.gameObject.SetActive(false);
    }

    public void Deselect_all_bonus_buttons()
    {
        if (current_bonus_selected != bonus_type.none)
        {
            current_bonus_selected = bonus_type.none;
            for (int i = 0; i < current_bonus_quantity.Length; i++)
                bonus_button_script[i].select_icon.SetActive(false);
        }
    }

    public void Destroy_n_casual_cards(int n)
    {
        //Debug.Log ("Destroy_n_casual_cards("+n+")");

        latest_cards_affected_by_the_bonus = new card[n];
        int random_start_point = Random.Range(0, total_card_on_board);
        int count = 0;
        for (int i = random_start_point; i < total_card_on_board; i++)
        {
            if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf && (cards_on_board[i].padlock_model_selected != card.padlock_model.keyhole || cards_on_board[i].current_padlock_hp <= 0))
            {
                latest_cards_affected_by_the_bonus[count] = cards_on_board[i];
                cards_on_board[i].Damage_this_card(true);
                count++;
                if (count >= n)
                    break;
            }
        }
        if (count < n)
        {
            for (int i = 0; i < random_start_point; i++)
            {
                if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf && (cards_on_board[i].padlock_model_selected != card.padlock_model.keyhole || cards_on_board[i].current_padlock_hp <= 0))
                {
                    latest_cards_affected_by_the_bonus[count] = cards_on_board[i];
                    cards_on_board[i].Damage_this_card(true);
                    count++;
                    if (count >= n)
                        break;
                }
            }
        }
    }

    bool Can_shuffle_this_card_type(int i)
    {
        bool return_this = false;

        switch (cards_on_board[i].card_type_selected)
        {
            case card_class.card_type.normal:
                return_this = true;
                cards_on_board[i].shuffle_me = true;
                break;

            //case card_class.card_type.gold:
            //    if (can_shuffle_gold_card)
            //    {
            //        return_this = true;
            //        cards_on_board[i].shuffle_me = true;
            //    }
            //    break;

            case card_class.card_type.wild:
                if (can_shuffle_wild_card)
                {
                    return_this = true;
                    cards_on_board[i].shuffle_me = true;
                }
                break;

            case card_class.card_type.collectable_bonus:
                if (can_shuffle_collectable_card)
                {
                    return_this = true;
                    cards_on_board[i].shuffle_me = true;
                }
                break;
        }

        return return_this;
    }

    public IEnumerator Undo_shuffle_board()
    {
        player_can_move = false;

        //rotate card face down
        for (int i = 0; i < shuffle_array.Length; i++)
        {
            shuffle_array[i].face_up = false;
            StartCoroutine(shuffle_array[i].Rotate_me(shuffle_array[i].current_my_rotation_down));
        }

        yield return new WaitForSeconds(1); //wait card rotartion down

        //restore previous position
        for (int i = 0; i < shuffle_array.Length; i++)
        {
            shuffle_array[i].Undo_shuffle();
        }

        yield return new WaitForSeconds(animation_duration);

        for (int i = 0; i < shuffle_array.Length; i++)
            shuffle_array[i].Find_cards_over_me();

        StartCoroutine(Refresh_card_rotation());


        yield return new WaitForSeconds(animation_duration);

        player_can_move = true;
    }

    public IEnumerator Shuffle_board()
    {
        player_can_move = false;

        //identify how many card shuffle
        int cards_to_shuffle = 0;
        for (int i = 0; i < total_card_on_board; i++)
        {
            if (cards_on_board[i].gameObject.activeSelf)
            {
                switch (cards_on_board[i].padlock_model_selected)
                {
                    case card.padlock_model.none:
                        if (Can_shuffle_this_card_type(i))
                            cards_to_shuffle++;
                        break;

                    case card.padlock_model.normal:
                        if (can_shuffle_normal_padlock)
                        {
                            if (Can_shuffle_this_card_type(i))
                                cards_to_shuffle++;
                        }
                        break;

                    case card.padlock_model.royal:
                        if (can_shuffle_royal_padlock)
                        {
                            if (Can_shuffle_this_card_type(i))
                                cards_to_shuffle++;
                        }
                        break;

                    case card.padlock_model.keyhole:
                        if (can_shuffle_keyhole_padlock)
                        {
                            if (Can_shuffle_this_card_type(i))
                                cards_to_shuffle++;
                        }
                        break;
                }
            }
        }

        if (cards_to_shuffle > 1)
        {
            //identify the cards to shuffle
            shuffle_array = new card[cards_to_shuffle];
            int shuffle_count = 0;
            for (int i = 0; i < total_card_on_board; i++)
            {
                if (cards_on_board[i].shuffle_me)
                {
                    shuffle_array[shuffle_count] = cards_on_board[i];

                    //rotate down all cards
                    shuffle_array[shuffle_count].face_up = false;
                    StartCoroutine(shuffle_array[shuffle_count].Rotate_me(shuffle_array[shuffle_count].current_my_rotation_down));

                    shuffle_count++;
                }
            }



            //shuffle
            bool[] card_moved = new bool[cards_to_shuffle];
            bool[] location_used = new bool[cards_to_shuffle];
            //Debug.Log ("cards_to_shuffle = " + cards_to_shuffle);




            for (int i = 0; i < cards_to_shuffle; i++)
            {
                if (card_moved[i])
                    Debug.LogWarning("XXX");
            }

            int safe_count = 1000;

            yield return new WaitForSeconds(1); //wait card rotartion down


            while (cards_to_shuffle > 0)
            {
                int random_card = Random.Range(0, shuffle_array.Length);
                int random_new_location = Random.Range(0, shuffle_array.Length);

                if (random_card == random_new_location && cards_to_shuffle > 1)
                {
                    if (random_card > 0)
                        random_card--;
                    else
                        random_card = cards_to_shuffle - 1;
                }

                if (!card_moved[random_card] && !location_used[random_new_location])
                {
                    //Debug.Log("move " + random_card);
                    shuffle_array[random_card].Shuffle_new_position(shuffle_array[random_new_location].transform.position,
                                                                        shuffle_array[random_new_location].transform.rotation,
                                                                       shuffle_array[random_new_location].current_my_rotation_up,
                                                                        shuffle_array[random_new_location].current_my_rotation_down
                                                                    );



                    card_moved[random_card] = true;
                    location_used[random_new_location] = true;
                    cards_to_shuffle--;

                    //Debug.Log("move " + shuffle_array[random_card].name + " in " + shuffle_array[random_new_location].name);

                    //Debug.Log("random_new_location = " + random_new_location);
                }

                safe_count--;
                if (safe_count < 0)
                {
                    Debug.LogWarning("!");
                    break;
                }
            }

            for (int i = 0; i < shuffle_array.Length; i++)
                shuffle_array[i].End_shuffle();

            yield return new WaitForSeconds(animation_duration);

            for (int i = 0; i < shuffle_array.Length; i++)
                shuffle_array[i].Find_cards_over_me();

            StartCoroutine(Refresh_card_rotation());


            yield return new WaitForSeconds(animation_duration);
            player_can_move = true;
        }
        else //can't shuffle
        {
            current_last_move = deck.last_move.none;
            Update_undo_button(false);//turn off undo button

            player_can_move = true;
            bonus_button_script[3].Add_a_charge();

        }
    }
    #endregion

    #region save and load from GameMaster
    void Load_from_gamemaster()
    {
        //Debug.Log("Load_from_gamemaster()");
        //bonus
        if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_bonuses; i++)
            {
                current_bonus_quantity[i] = GameMaster.my_GameMaster.bonus_quantity[GameMaster.my_GameMaster.current_stage, i];
            }
        }
        else if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.only_one_save_to_keep_among_stages)
        {
            GameMaster.my_GameMaster.Load_general_bonus();
            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_bonuses; i++)
                current_bonus_quantity[i] = GameMaster.my_GameMaster.general_bonus[i];

        }


        //wild card
        if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            my_wild_card_deck.Initiate();
            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_wildcards; i++)
            {
                if (GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i] >= 0 && GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i] < 100)
                    my_wild_card_deck.Add_wild_card(GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i], card_class.joker_type.none);
                else if (GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i] == 100)
                    my_wild_card_deck.Add_wild_card(-99, card_class.joker_type.normal);
            }
        }
        else if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.only_one_save_to_keep_among_stages)
        {
            my_wild_card_deck.Initiate();

            GameMaster.my_GameMaster.Load_general_wild_cards();
            for (int i = 0; i < GameMaster.my_GameMaster.general_wild_cards_quantity; i++)
            {
                if (GameMaster.my_GameMaster.general_wild_card[i] >= 0 && GameMaster.my_GameMaster.general_wild_card[i] < 100)
                    my_wild_card_deck.Add_wild_card(GameMaster.my_GameMaster.general_wild_card[i], card_class.joker_type.none);
                else if (GameMaster.my_GameMaster.general_wild_card[i] == 100)
                    my_wild_card_deck.Add_wild_card(-99, card_class.joker_type.normal);
            }

        }
    }

    void Save_in_gamemaster(bool go_to_next_board = false)
    {
        //Debug.Log(GameMaster.my_GameMaster.current_stage +","+ GameMaster.my_GameMaster.current_board_in_this_stage[GameMaster.my_GameMaster.current_stage] + " Save_in_gamemaster " + go_to_next_board);

        GameMaster.my_GameMaster.total_star_score[GameMaster.my_GameMaster.current_stage] = total_star_score;
        GameMaster.my_GameMaster.current_stage_total_score[GameMaster.my_GameMaster.current_stage] = current_score;
        GameMaster.my_GameMaster.canUpdateMainMenu = true;

        //bonus
        if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_bonuses; i++)
                GameMaster.my_GameMaster.bonus_quantity[GameMaster.my_GameMaster.current_stage, i] = current_bonus_quantity[i];
        }
        else if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.only_one_save_to_keep_among_stages)
        {
            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_bonuses; i++)
                GameMaster.my_GameMaster.general_bonus[i] = current_bonus_quantity[i];

            if (go_to_next_board)
                GameMaster.my_GameMaster.Save_general_bonus();
        }


        //wild card
        if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_wildcards; i++)
            {
                if (my_wild_card_deck.my_cards[i].gameObject.activeSelf)
                {
                    if (my_wild_card_deck.my_cards[i].GetComponent<wild_card>().my_joker == card_class.joker_type.none)
                        GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i] = my_wild_card_deck.my_cards[i].GetComponent<wild_card>().my_rank;
                    else
                        GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i] = 100;
                }
                else
                    GameMaster.my_GameMaster.wild_cards[GameMaster.my_GameMaster.current_stage, i] = -99;
            }
        }
        else if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.only_one_save_to_keep_among_stages)
        {
            GameMaster.my_GameMaster.general_wild_cards_quantity = my_wild_card_deck.current_wild_cards;

            for (int i = 0; i < GameMaster.my_GameMaster.max_number_of_wildcards; i++)
            {
                //print(i + " || " + GameMaster.my_GameMaster.max_number_of_wildcards);
                if (my_wild_card_deck.my_cards.Length <= 0)
                {
                    return;
                }
                if (my_wild_card_deck.my_cards[i].gameObject.activeSelf)
                {
                    if (my_wild_card_deck.my_cards[i].GetComponent<wild_card>().my_joker == card_class.joker_type.none)
                        GameMaster.my_GameMaster.general_wild_card[i] = my_wild_card_deck.my_cards[i].GetComponent<wild_card>().my_rank;
                    else
                        GameMaster.my_GameMaster.general_wild_card[i] = 100;
                }
                else
                    GameMaster.my_GameMaster.general_wild_card[i] = -99;
            }
            if (go_to_next_board)
                GameMaster.my_GameMaster.Save_general_wild_cards();
        }  
    }
    #endregion

    #region GUI buttons
    public void Pause_button()
    {
        if (GameMaster.my_GameMaster)
            GameMaster.my_GameMaster.Open_pause();
    }

    public void Quit()
    {
        Play_sfx(gui_button_sfx);

        if (GameMaster.my_GameMaster)
        {
            //GameMaster.my_GameMaster.Erase_this_stage_save(GameMaster.my_GameMaster.current_stage);//next time you'll star from board 0, because you fail.This prevent unsolvable multi board stages, when player reach last board without enough score to win.
            Go_to_stage_menu();
        }
    }

    public void Go_to_next_board()
    {
        Play_sfx(gui_button_sfx);


        //Debug.Log("No more stage. Go to game menu");
        if (GameMaster.my_GameMaster)
        {
            //print("else Save_in_gamemaster");
            Save_in_gamemaster(true);
            Go_to_stage_menu();
        }


        if (GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.Save_everything();
        }

    }

    void Go_to_stage_menu()
    {
        if (game_end && player_win)
        {
            if (GameMaster.my_GameMaster.current_icon + 1 < GameMaster.my_GameMaster.total)
            {
                //Debug.Log("unlock next stage");
                //unlock next stage
                if (GameMaster.my_GameMaster.create_a_stage_icon_for_each_board)
                {
                    GameMaster.my_GameMaster.Unlock_this_stage_icon(GameMaster.my_GameMaster.current_icon + 1);
                }
                else
                {
                    //Debug.Log("last_board : "+last_board);
                    if (last_board)
                    {
                        GameMaster.my_GameMaster.Unlock_this_stage_icon(GameMaster.my_GameMaster.current_icon + 1);
                        GameMaster.my_GameMaster.number_stage_unlocked++;
                        //Debug.Log("current_icon : "+GameMaster.my_GameMaster.current_icon);
                    }

                }
                GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.room);
            }
            else
            {
                //this is the final stage [END GAME]
                //GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.end);
                GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.room);
                GameMaster.my_GameMaster.Stage_solved(GameMaster.my_GameMaster.current_icon);
            }
        }
        else
        {
            GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.room);
        }
    }


    public void Retry()
    {
        Debug.Log("Retry");
        if (!GameMaster.my_GameMaster)
            Play_sfx(gui_button_sfx);

        //กรณีที่เงื่อไขเป็นเวลานะจ๊ะ
        time_collect_all_card = time_collect_all_card_start;
        isGameOver = false;
        current_score = 0;

        if (!has_Bonus_Add_Card_In_Deck)
        {
            deck_card_left = start_deck_card_left;
        }
        else
        {
            start_deck_card_left = deck_card_left;
            start_deck_card_left = deck_card_left_all;
            deck_card_left = start_deck_card_left;
        }

        total_gold_cards_taken_in_all_boards -= gold_cards_taken;

        total_combo_sum = current_combo_sum_at_board_start;

        total_star_score -= current_star_score;

        all_cards_collected_in_this_board = false;

        //erase win condition reached in this board
        if (win_condition_satisfied_in_this_board)
        {
            win_conditions_satisfied = false;
        }

        Restart(true);
    }
    #endregion

    public void SetCardDeckFollowCustomDeckList(bool isAddCardOnDeck,int countCadAdd)
    {
        if(!isAddCardOnDeck)
        {
            deck_custom_card = new card_class[deckCustom.deck_custom_card_list.Count];
            for (int i = 0; i < deckCustom.deck_custom_card_list.Count; i++)
            {
                deck_custom_card[i] = deckCustom.deck_custom_card_list[i].card;
            }

            deck_cards = deck_custom_card;
        }
        else
        {
            

            for (int i = 0; i < countCadAdd; i++)
            {
                deckCustom.deck_custom_card_list.Add(newCard);
            }
            deck_custom_card = new card_class[deckCustom.deck_custom_card_list.Count];
            for (int i = 0; i < deckCustom.deck_custom_card_list.Count; i++)
            {
                deck_custom_card[i] = deckCustom.deck_custom_card_list[i].card;
            }

            deck_cards = deck_custom_card;

        }       
    }

    #region gameplay interactions
    void Ask_new_card(bool allow_undo)
    {
        DeselectCard();
        print(current_deck_position);

        if (deck_cards[current_deck_position].card_type_selected == card_class.card_type.wild && my_wild_card_deck.current_wild_cards < my_wild_card_deck.max_wild_cards)//if this is a wild card and can go in wild deck
        {
            //put this card sprite on the ghost card used to show the card movement from board to target deck
            ghost_card_SpriteRenderer.sprite = Assign_the_sprite_of_this_card(deck_cards[current_deck_position]);//deck.this_deck.Assign_the_sprite_of_this_card(deck_cards[current_deck_position].suit,deck_cards[current_deck_position].rank,deck_cards[current_deck_position].card_type_selected);
                                                                                                                 //put his card in wild deck
            StartCoroutine(Move_to(transform, my_wild_card_deck.my_cards[my_wild_card_deck.current_wild_cards].transform, false));//show card movement from board to wild deck
            StartCoroutine(Add_wild_card_to_wild_deck_with_delay(deck_cards[current_deck_position].rank, deck_cards[current_deck_position].joker_type_selected));
        }
        else
        {
            if (current_deck_position == 0 && first_card_is_always_useful)//fist card is always useful
            {
                GiveAnUsefulCardForSure_cheat();
            }

            if (!deckCustom.deck_custom_card_list[current_deck_position].isManual)
            {
                New_current_deck_card(deck_cards[current_deck_position], last_move.ask_new_card_from_deck, false);
            }
            else
            {
                New_current_deck_card(deckCustom.deck_custom_card_list[current_deck_position].card, last_move.ask_new_card_from_deck, false);
            }

            if (allow_undo && previous_card.rank >= 0 && target_deck_cards.Count > 1)
            {
                //update undo
                current_last_move = last_move.ask_new_card_from_deck;
                Update_undo_button(true);
            }
            else
            {
                //update undo
                current_last_move = deck.last_move.none;
                Update_undo_button(false);//turn off undo button
            }
        }
    }

    void OnMouseDown()//click here the player ask a new card
    {
        if (game_end || !player_can_move)
            return;

        if (Input.touches.Length <= 1)
        {
            print("Click here!");
            Deselect_all_bonus_buttons();
            Reset_Count_Streak();
            if (deck_card_left > 1)//if there is a card to give
            {
                player_can_move = false;
                lastest_deactivate_board_card = null;

                //update deck count
                if (!has_Bonus_Add_Card_In_Deck)
                {
                    deck_card_left--;
                    deck_card_left_text.text = (deck_card_left - 1).ToString();
                }
                else
                {
                    deck_card_left_use_real--;

                    deck_card_left = deck_card_left_use_real;
                    deck_card_left_text.text = (deck_card_left - 1).ToString();
                }
                current_deck_position++;

                Ask_new_card(true);

                            
            }
            else// no more card, the game end
            {
                //no card
                if (spriteRenderThisCard.enabled == true)
                {
                    current_deck_card = new card_class();
                    selected_card = current_deck_card;
                    //Show_end_screen();
                    print("Play Animation Card End Game");
                }
                else
                {
                    print("This is tutorial!");
                }
            }
        }
        else
        {
            this_deck.Play_sfx(this_deck.blocked_card_sfx);
        }
    }

    public void Undo_last_move()
    {
        if (undo_charges > 0 && player_can_move)//if player can ask an undo
        {
            Deselect_all_bonus_buttons();

            undo_charges--;
            Update_undo_button(false);

            Play_sfx(gui_button_sfx);

            switch (current_last_move)
            {
                case last_move.ask_new_card_from_deck://return last card to deck
                    //print("UNDO: last_move.ask_new_card_from_deck");
                    combo_count = previous_combo_count;

                    deck_card_left++;
                    deck_card_left_text.text = (deck_card_left - 1).ToString();
                    current_deck_position--;

                    //print(target_deck_cards.Count);
                    target_deck_cards.RemoveAt(target_deck_cards.Count - 1);
                    //print(target_deck_cards.Count);
                    /*New_current_deck_card(target_deck_cards[target_deck_cards.Count-1].suit, target_deck_cards[target_deck_cards.Count-1].rank,
                                     previous_card.card_type_selected, previous_card.joker_type_selected,
                                     current_last_move, true);*/

                    New_current_deck_card(target_deck_cards[target_deck_cards.Count - 1], current_last_move, true);

                    /*
                    current_target_deck_position--;//remove last card from target deck

                    New_current_deck_card(target_deck_cards[current_target_deck_position].suit, target_deck_cards[current_target_deck_position].rank,
                                     previous_card.card_type_selected, previous_card.joker_type_selected,
                                     current_last_move, true);*/


                    if (!this.gameObject.GetComponent<SpriteRenderer>().enabled)
                        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    break;

                case last_move.take_card_from_board_and_put_it_into_target_deck://return last card to board from target deck
                    //print("UNDO: last_move.take_card_from_board_and_put_it_into_target_deck");
                    current_total_card_on_board++;

                    //current_target_deck_position--;//remove last card from target deck

                    StartCoroutine(Move_to(target_new_card.transform, lastest_deactivate_board_card.transform, true));

                    /*
                    New_current_deck_card(target_deck_cards[current_target_deck_position].suit, target_deck_cards[current_target_deck_position].rank,
                                     previous_card.card_type_selected, previous_card.joker_type_selected,
                                    current_last_move, true);*/
                    print("undo last move");
                    New_current_deck_card(target_deck_cards[target_deck_cards.Count - 2], current_last_move, true);
                    target_deck_cards.RemoveAt(deck.this_deck.target_deck_cards.Count - 1);

                    //undo combo sum
                    total_combo_sum--;
                    if (total_combo_sum < 0)
                        total_combo_sum = 0;

                    if (!combo_sum_bonus_obtained)
                        total_combo_sum--;
                    else
                    {
                        if (total_combo_sum == 0)
                        {
                            total_combo_sum = combo_sum_target_to_trigger_the_score_bonus - 1;
                            combo_sum_bonus_obtained = false;
                            current_score -= give_this_score_bonus_when_reach_the_combo_sum;
                        }
                    }


                    if (total_combo_sum < 0)
                        total_combo_sum = 0;

                    if (!combo_sum_bonus_obtained)
                        Update_combo_sum_gui();
                    break;

                case last_move.take_card_from_board_and_put_it_into_wild_deck://return last card to board from wild deck
                    //print("UNDO: last_move.take_card_from_board_and_put_it_into_wild_deck");
                    current_total_card_on_board++;

                    //move ghost card from wild deck to board
                    StartCoroutine(Move_to(my_wild_card_deck.my_cards[my_wild_card_deck.current_wild_cards - 1].transform, lastest_deactivate_board_card.transform, true));
                    //remove card from wild deck
                    my_wild_card_deck.Remove_card(my_wild_card_deck.current_wild_cards - 1);
                    break;

                case last_move.take_card_from_wild_deck: //return last card to wild deck
                    //rint("UNDO: last_move.take_card_from_wild_deck");

                    if (target_deck_cards.Count > 1)
                    {
                        print("target_deck_cards : "+target_deck_cards);
                        New_current_deck_card(target_deck_cards[target_deck_cards.Count - 2], current_last_move, true);
                    }
                    else
                    {
                        print("target_deck_cards.Count <= 1 ");
                        New_current_deck_card(null, current_last_move, false);
                    }

                    target_deck_cards.RemoveAt(deck.this_deck.target_deck_cards.Count - 1);

                    break;

                case last_move.damage_padlock:
                    //print("UNDO: last_move.damage_padlock");
                    current_target_deck_position--;//remove last card from target deck
                    StartCoroutine(Move_to(target_new_card.transform, lastest_deactivate_board_card.transform, true));

                    New_current_deck_card(target_deck_cards[target_deck_cards.Count - 2], current_last_move, true);
                    target_deck_cards.RemoveAt(deck.this_deck.target_deck_cards.Count - 1);
                    break;

                case last_move.use_bonus:
                    //print("UNDO: use_bonus");
                    bonus_button_script[(int)previous_bonus_selected].Add_a_charge();

                    if (previous_bonus_selected == bonus_type.shuffle)
                    {
                        StartCoroutine(Undo_shuffle_board());
                    }
                    else
                    {
                        if (previous_bonus_selected != bonus_type.bonus_key)
                            Undo_score();

                        for (int i = 0; i < latest_cards_affected_by_the_bonus.Length; i++)
                            StartCoroutine(latest_cards_affected_by_the_bonus[i].Undo_damage_effect());

                        if (previous_bonus_selected != bonus_type.bonus_key)
                            StartCoroutine(Refresh_card_rotation());
                    }
                    break;

            }

        }
    }

    public void Update_undo_button(bool can_undo)
    {
        if ((undo_charges == 0) || (current_last_move == last_move.none) || target_deck_cards.Count < 1 || !can_undo)
            undo_button.interactable = false;
        else
            undo_button.interactable = true;

        undo_count_text.text = undo_charges.ToString();

    }

    Vector3 currentVelocity;
    public IEnumerator Move_to_enhanced(Transform start_point, Transform end_point,
                                        card_class new_current_card, last_move this_card_came_from, bool undo)
    {

        ghost_card.position = start_point.position;
        ghost_card.rotation = start_point.rotation;
        ghost_card.gameObject.SetActive(true);


        float xPosition = ghost_card.position.x;
        float currentVelocityX = 0;
        float yPosition = ghost_card.position.y;
        float currentVelocityY = 0;
        float yPositionB = 0;
        float endRotationZ = end_point.rotation.z + 360;
        float currentRotationZ = 0;

        float upTarget = ghost_card.position.y + 1;

        bool done = false;
        float curveTime = 0;
        while (!done)
        {

            ghost_card.position = Vector3.SmoothDamp(ghost_card.position, end_point.position, ref currentVelocity, animation_duration);
            ghost_card.position += new Vector3(0, verticalVariation.Evaluate(curveTime) * verticalVariationMultiplier, 0);

            curveTime += Time.deltaTime;

            currentRotationZ = Mathf.Lerp(0, 360, curveTime * 1.5f);
            ghost_card.rotation = Quaternion.Euler(new Vector3(0, 0, currentRotationZ));

            if (ghost_card.position.y < end_point.position.y)
                ghost_card.position = new Vector3(ghost_card.position.x, end_point.position.y, ghost_card.position.z);

            if (Vector3.Distance(ghost_card.position, end_point.position) <= 0.05f)
            {
                done = true;
                if (this_card_came_from == last_move.take_card_from_board_and_put_it_into_target_deck)
                    New_current_deck_card(new_current_card, this_card_came_from, undo);
                else if (this_card_came_from == last_move.take_card_from_board_and_put_it_into_wild_deck)
                    Add_wild_card_to_wild_deck(new_current_card.rank, new_current_card.joker_type_selected);
            }

            yield return null;
        }

        ghost_card.gameObject.SetActive(false);

    }

    public IEnumerator Move_to(Transform start_point, Transform end_point, bool this_card_return_to_board)//move the ghost card
    {
        ghost_card.position = start_point.position;
        ghost_card.rotation = start_point.rotation;
        ghost_card.gameObject.SetActive(true);

        //animation
        float duration = animation_duration;
        float time = 0;

        while (time < 1)
        {
            time += Time.smoothDeltaTime / duration;
            ghost_card.position = Vector3.Lerp(start_point.position, end_point.position, time);
            ghost_card.rotation = Quaternion.Lerp(start_point.rotation, end_point.rotation, time);
            yield return null;
        }
        ghost_card.gameObject.SetActive(false);

        if (this_card_return_to_board)
        {
            lastest_deactivate_board_card.SetActive(true);
            StartCoroutine(Refresh_card_rotation());
        }
    }

    void Add_wild_card_to_wild_deck(int my_rank, card_class.joker_type my_joker)
    {
        my_wild_card_deck.Add_wild_card(my_rank, my_joker);
        Check_how_many_card_remain_on_board();
    }

    public IEnumerator Add_wild_card_to_wild_deck_with_delay(int my_rank, card_class.joker_type my_joker)//in order to update the wild deck after the Move_to animation
    {
        yield return new WaitForSeconds(animation_duration);
        my_wild_card_deck.Add_wild_card(my_rank, my_joker);
        Check_how_many_card_remain_on_board();
    }

    public void New_current_deck_card(card_class new_current_card,/*int suit, int rank, card_class.card_type card_type, card_class.joker_type joker,*/ last_move this_card_came_from, bool undo)//show the new current card in target deck
    {
        player_can_move = false;//because animation

        previous_card = new card_class();
        previous_card = current_deck_card;


        current_deck_card = new card_class();
        print("new_current_card : "+new_current_card.rank);
        current_deck_card = new_current_card;

        switch (this_card_came_from)
        {
            case last_move.ask_new_card_from_deck:
                Play_sfx(new_card_sfx);
                print("ask_new_card_from_deck");
                StartCoroutine(Take_card_from_deck(undo));
                break;

            case last_move.take_card_from_board_and_put_it_into_target_deck:
                Play_sfx(take_this_card_end_sfx);
                StartCoroutine(Take_card_from_board(undo));
                //Check_royal_padlocks(rank,undo);
                break;

            case last_move.take_card_from_board_and_put_it_into_wild_deck:
                //Play_sfx(take_this_card_sfx);
                //StartCoroutine(Take_card_from_board(card_type,undo));
                break;

            case last_move.take_card_from_wild_deck:
                Play_sfx(new_card_sfx);
                StartCoroutine(Take_card_from_wild_deck(undo));
                break;

            case last_move.damage_padlock:
                Play_sfx(take_this_card_end_sfx);
                StartCoroutine(Take_clone_card_from_padlock(undo));
                //Check_royal_padlocks(rank,undo);
                break;
        }
        
        if (main_rule_selected == main_rule.chain)
        {
            selected_card = current_deck_card;
        }
    }

    IEnumerator Take_clone_card_from_padlock(bool undo)
    {
        //print("Take_clone_card_from_padlock");
        if (undo)
        {
            target_new_card.sprite = Assign_the_sprite_of_this_card(current_deck_card);//show previous card in target deck
            target_new_card.enabled = true;

            Undo_score();

            current_total_padlock_hp_on_board++;
            yield return new WaitForSeconds(animation_duration);//wait the end of the animation
            lastest_deactivate_board_card.GetComponent<card>().Repair_padlock();

            if (previous_card.rank >= 10 && previous_card.rank <= 12)//if this is a royal card
            {
                yield return new WaitForSeconds(rotation_duration);
                Repair_last_royal_padlocks_damaged();
            }
        }
        else
        {
            current_total_padlock_hp_on_board--;
            yield return new WaitForSeconds(animation_duration);//wait the end of the animation

            current_target_deck_position++;
            Add_new_card_to_target_deck();
            target_new_card.sprite = Assign_the_sprite_of_this_card(current_deck_card);
            target_new_card.enabled = true;

            unused_card_count = 0;

            if (current_deck_card.rank >= 10 && current_deck_card.rank <= 12)//if this is a royal card
                Damage_all_free_royal_padlocks();

        }

        if (!goals_window_is_open)
            player_can_move = true;

    }

    IEnumerator Take_card_from_wild_deck(bool undo)
    {
        //print("Take_card_from_wild_deck: " + undo + "..." + current_deck_card.suit + "," +current_deck_card.rank);
        Sprite temp_sprite = Assign_the_sprite_of_this_card(current_deck_card);

        if (undo)
        {
            //animation
            //print("ghost_card animation .... " + previous_card);
            ghost_card_SpriteRenderer.sprite = Assign_the_sprite_of_this_card(previous_card);//setup ghost card sprite

            StartCoroutine(Move_to(target_new_card.transform, my_wild_card_deck.my_cards[my_wild_card_deck.current_wild_cards].transform, false));

            //show previous card in target deck
            target_new_card.sprite = temp_sprite;
            target_new_card.enabled = true;

            yield return new WaitForSeconds(animation_duration);//wait the end of the animation

            //put back will card
            my_wild_card_deck.Add_wild_card(previous_card.rank, previous_card.joker_type_selected);

        }
        else
        {
            //animation
            ghost_card_SpriteRenderer.sprite = temp_sprite;//setup ghost card sprite
            StartCoroutine(Move_to(my_wild_card_deck.my_cards[my_wild_card_deck.last_card_removed_position].transform, target_new_card.transform, false));
            yield return new WaitForSeconds(animation_duration);//wait the end of the animation

            //add wild card to target deck
            target_new_card.sprite = temp_sprite;
            target_new_card.enabled = true;
            current_target_deck_position++;
            Add_new_card_to_target_deck();

        }

        if (!goals_window_is_open)
            player_can_move = true;
    }

    void Add_new_card_to_target_deck()
    {
        card_class temp = new card_class();
        temp.suit = current_deck_card.suit;
        temp.rank = current_deck_card.rank;
        temp.card_type_selected = current_deck_card.card_type_selected;
        temp.joker_type_selected = current_deck_card.joker_type_selected;

        target_deck_cards.Add(temp);

    }

    IEnumerator Take_card_from_board(bool undo)
    {
        //print("Take_card_from_board");
        if (undo)
        {

            target_new_card.sprite = Assign_the_sprite_of_this_card(current_deck_card);
            target_new_card.enabled = true;

            yield return new WaitForSeconds(animation_duration);//wait the end of the animation

            Undo_score();

            //if (previous_card.card_type_selected == card_class.card_type.gold)
            //{
            //    gold_cards_taken--;
            //    total_gold_cards_taken_in_all_boards--;
            //    Update_gold_cards_count();
            //}

            if (previous_card.rank >= 10 && previous_card.rank <= 12)//if this is a royal card
            {
                yield return new WaitForSeconds(rotation_duration);
                Repair_last_royal_padlocks_damaged();
            }

        }
        else //card take from board and this is not an undo move
        {
            if (enhanced_animation)
                yield return new WaitForSeconds(0);
            else
                yield return new WaitForSeconds(animation_duration);//wait the end of the animation


            current_target_deck_position++;
            Add_new_card_to_target_deck();
            target_new_card.sprite = Assign_the_sprite_of_this_card(current_deck_card);//(current_card_suit,current_card_rank,card_type);
            target_new_card.enabled = true;

            unused_card_count = 0;

            //if (current_deck_card.card_type_selected == card_class.card_type.gold)
            //{
            //    gold_cards_taken++;
            //    total_gold_cards_taken_in_all_boards++;
            //    Update_gold_cards_count();
            //}

            current_total_card_on_board--;

            Check_how_many_card_remain_on_board();

            if (current_deck_card.rank >= 10 && current_deck_card.rank <= 12)//if this is a royal card
                Damage_all_free_royal_padlocks();


        }

        if (!goals_window_is_open)
            player_can_move = true;

    }


    int Check_avaible_moves(card_class target_card)
    {
        int return_this = -1;//-1 = no avaible move
        switch (previous_card.joker_type_selected)
        {
            case card_class.joker_type.none:
                int target_rank = target_card.rank;
                for (int i = 0; i < cards_on_board.Length; i++)
                {
                    if (cards_on_board[i].padlock_model_selected != card.padlock_model.keyhole)
                    {

                        if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active
                        {
                            if (cards_on_board[i].card_type_selected == card_class.card_type.wild
                                && ((cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp == 0)
                                || cards_on_board[i].padlock_model_selected == card.padlock_model.none))
                            {
                                //wild card without padlock can be always collect
                                return_this = i;
                                break;
                            }
                            else
                            {
                                if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
                                {
                                    switch (cards_on_board[i].joker_type_selected)
                                    {
                                        case card_class.joker_type.none:
                                            switch (main_rule_selected)
                                            {
                                                case main_rule.chain:
                                                    if ((cards_on_board[i].my_rank > 0) &&
                                                        (cards_on_board[i].my_rank < 12))
                                                    {
                                                        if ((target_rank + 1 == cards_on_board[i].my_rank)
                                                            || (target_rank - 1 == cards_on_board[i].my_rank))
                                                        {
                                                            return_this = i;
                                                            break;
                                                        }
                                                    }
                                                    else if (cards_on_board[i].my_rank == 12)
                                                    {
                                                        if ((target_rank == 0)
                                                            || (target_rank + 1 == cards_on_board[i].my_rank))
                                                        {
                                                            return_this = i;
                                                            break;
                                                        }
                                                    }
                                                    else if (cards_on_board[i].my_rank == 0)
                                                    {
                                                        if ((target_rank - 1 == cards_on_board[i].my_rank)
                                                            || (target_rank == 12))
                                                        {
                                                            return_this = i;
                                                            break;
                                                        }
                                                    }
                                                    break;
                                            }
                                            break;

                                        case card_class.joker_type.normal:
                                            return_this = i;
                                            //break;
                                            break;

                                        //case card_class.joker_type.red:
                                        //    if (target_card.suit == 2 || target_card.suit == 3
                                        //        || target_card.joker_type_selected == card_class.joker_type.normal || target_card.joker_type_selected == card_class.joker_type.red)
                                        //    {
                                        //        return_this = i;
                                        //        break;
                                        //    }
                                        //    break;

                                        //case card_class.joker_type.black:
                                        //    if (target_card.suit == 0 || target_card.suit == 1
                                        //        || target_card.joker_type_selected == card_class.joker_type.normal || target_card.joker_type_selected == card_class.joker_type.black)
                                        //    {
                                        //        return_this = i;
                                        //        break;
                                        //    }
                                        //    break;
                                    }
                                }
                            }

                        }
                    }

                }
                break;

            case card_class.joker_type.normal://search a free card wild or not
                for (int i = 0; i < cards_on_board.Length; i++)
                {
                    if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active
                    {
                        if (cards_on_board[i].card_type_selected == card_class.card_type.wild
                            && ((cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp == 0)
                            || cards_on_board[i].padlock_model_selected == card.padlock_model.none))
                        {
                            //wild card without padlock can be always collect
                            return_this = i;
                            break;
                        }
                        else//search a free card
                        {
                            if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
                            {
                                return_this = i;
                                break;
                            }
                        }
                    }
                }
                break;

            //case card_class.joker_type.red://search a red card or a wild card
            //    for (int i = 0; i < cards_on_board.Length; i++)
            //    {
            //        if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active
            //        {
            //            if (cards_on_board[i].card_type_selected == card_class.card_type.wild
            //                && ((cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp == 0)
            //                || cards_on_board[i].padlock_model_selected == card.padlock_model.none))
            //            {
            //                //wild card without padlock can be always collect
            //                return_this = i;
            //                break;
            //            }
            //            else//search a red card
            //            {
            //                if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
            //                {
            //                    if (cards_on_board[i].my_suit == 2 || cards_on_board[i].my_suit == 3
            //                        || cards_on_board[i].joker_type_selected == card_class.joker_type.normal || cards_on_board[i].joker_type_selected == card_class.joker_type.red)
            //                    {
            //                        return_this = i;
            //                        break;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    break;

            //case card_class.joker_type.black://search a black card or a wild card
            //    for (int i = 0; i < cards_on_board.Length; i++)
            //    {
            //        if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active
            //        {
            //            if (cards_on_board[i].card_type_selected == card_class.card_type.wild
            //                && ((cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp == 0)
            //                || cards_on_board[i].padlock_model_selected == card.padlock_model.none))
            //            {
            //                //wild card without padlock can be always collect
            //                return_this = i;
            //                break;
            //            }
            //            else//search a black card
            //            {
            //                if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
            //                {
            //                    if (cards_on_board[i].my_suit == 0 || cards_on_board[i].my_suit == 1
            //                        || cards_on_board[i].joker_type_selected == card_class.joker_type.normal || cards_on_board[i].joker_type_selected == card_class.joker_type.black)
            //                    {
            //                        return_this = i;
            //                        break;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    break;
        }

        return return_this;

    }

    public IEnumerator Shake_card(Transform this_card)//show the miss move
    {
        Vector3 start_position = this_card.position;
        float min_magnitude = -0.05f;
        float max_magnitude = 0.05f;
        int n_shakes = 5;
        while (n_shakes > 0)
        {
            n_shakes--;

            this_card.position = new Vector3(this_card.position.x + Random.Range(min_magnitude, max_magnitude),
                                             this_card.position.y + Random.Range(min_magnitude, max_magnitude),
                                             this_card.position.z);


            yield return new WaitForSeconds(0.05f);
        }
        this_card.position = start_position;
    }

    void Check_royal_padlocks(int rank, bool undo)
    {
        if (undo)
        {
            if (previous_card.rank >= 10 && previous_card.rank <= 12)//if this is a royal card
                Repair_last_royal_padlocks_damaged();
        }
        else
        {
            if (rank >= 10 && rank <= 12)//if this is a royal card
                Damage_all_free_royal_padlocks();
        }
    }

    public void Damage_all_free_royal_padlocks()
    {
        if (current_royal_padlock_hp_on_board > 0) //if there are royal padlock to damage
        {
            for (int i = 0; i < cards_on_board.Length; i++)
            {
                if (cards_on_board[i].gameObject.activeSelf == true) //if this card is active
                {
                    if (cards_on_board[i].This_card_is_free() && cards_on_board[i].padlock_model_selected == card.padlock_model.royal)//if there is a royal padlock to damage among the free cards
                    {
                        if (cards_on_board[i].current_padlock_hp > 0)
                            current_royal_padlock_hp_on_board--;

                        cards_on_board[i].Damage_royal_padlock();
                    }
                }
            }
        }
    }
    /*
    public void DestroyCurrentCardSelected()
    {
        if (selected_card.board_card)
            selected_card.board_card.DestroyMe();
        else
            target_deck_scrip.DestroyMe();

        DeselectCard();
    }*/

    public void DestroyThisMatch(card_class cardA, card_class cardB)
    {
        //print("DestroyThisMatch: " + cardA.rank + " & " + cardB.rank);

        //undo
        current_last_move = last_move.cards_matched;
        Update_undo_button(true);//turn on undo button

        if (cardA.board_card)
        {
            //print("A: " + cardA.board_card.current_padlock_hp);
            if (cardA.board_card.current_padlock_hp == 0)
                cardA.board_card.DestroyMe(false);
            else
                cardA.board_card.Damage_padlock();
        }
        else
            target_deck_scrip.DestroyMe();

        if (cardB.board_card)
        {
            //print("B: " + cardB.board_card.current_padlock_hp);
            if (cardB.board_card.current_padlock_hp == 0)
                cardB.board_card.DestroyMe(false);
            else
                cardB.board_card.Damage_padlock();
        }
        else
            target_deck_scrip.DestroyMe();

        Update_score_match(cardA, cardB);

        DeselectCard();
    }

    public void SelectCard(card_class target, Transform tr)
    {
        selected_card = target;

        ghost_card.transform.position = tr.position;
        ghost_card.transform.rotation = tr.rotation;

        ghost_card_SpriteRenderer.sprite = card_selected_sprite;

        ghost_card.gameObject.SetActive(true);

    }


    public void DeselectCard()
    {
        if (selected_card.rank == -99)
            return;

        if (selected_card.board_card)
        {
            ghost_card.gameObject.SetActive(false);
            selected_card = new deck.card_class();
        }
        else
            target_deck_scrip.SelectMe(false);

    }

    public void Repair_last_royal_padlocks_damaged()
    {

        if (total_royal_padlock_hp_on_board > 0) //if there are royal padlock
        {
            for (int i = 0; i < cards_on_board.Length; i++)
            {
                if (cards_on_board[i].gameObject.activeSelf == true) //if this card is active
                {
                    if (cards_on_board[i].This_card_is_free() && cards_on_board[i].padlock_model_selected == card.padlock_model.royal && cards_on_board[i].current_padlock_hp >= 0)//if there is a royal padlock to damage among the free cards
                    {
                        current_royal_padlock_hp_on_board++;
                        StartCoroutine(cards_on_board[i].Repair_royal_padlock());
                    }
                }
            }
        }
    }

    public IEnumerator Refresh_card_rotation()//turn face up all free cards on board
    {
        yield return new WaitForSeconds(animation_duration);

        for (int i = 0; i < cards_on_board.Length; i++)
        {
            if (cards_on_board[i].gameObject.activeSelf == true) //if this card is active
                cards_on_board[i].Turn_this_card(); //check if it can be turn up
        }
    }

    public Sprite Assign_the_sprite_of_this_card(card_class this_card)
    {
        Sprite return_this = null;

        if (this_card.card_type_selected == card_class.card_type.normal)
        {
            switch (this_card.joker_type_selected)
            {
                case card_class.joker_type.none:
                    return_this = card_deck_sprite[this_card.suit, this_card.rank];
                    break;

                case card_class.joker_type.normal:
                    return_this = normal_joker;
                    break;

                //case card_class.joker_type.red:
                //    return_this = red_joker;
                //    break;

                //case card_class.joker_type.black:
                //    return_this = black_joker;
                //    break;
            }
        }
        //else if (this_card.card_type_selected == card_class.card_type.gold)
        //{
        //    switch (this_card.joker_type_selected)
        //    {
        //        case card_class.joker_type.none:
        //            return_this = gold_card_deck_sprite[this_card.suit, this_card.rank];
        //            break;

        //        case card_class.joker_type.normal:
        //            return_this = gold_normal_joker;
        //            break;

        //        case card_class.joker_type.red:
        //            return_this = gold_red_joker;
        //            break;

        //        case card_class.joker_type.black:
        //            return_this = gold_black_joker;
        //            break;
        //    }
        //}
        else if (this_card.card_type_selected == card_class.card_type.wild)
        {
            if (this_card.joker_type_selected == card_class.joker_type.none)
                return_this = wild[this_card.rank];
            else
                return_this = wild_joker;
        }

        return return_this;
    }

    void GiveAnUsefulCardForSure_honest()
    {
        //find avaible ranks on board
        List<int> avaibleRanksOnBoard = new List<int>();
        for (int i = 0; i < cards_on_board.Length; i++)
        {
            if (cards_on_board[i].my_rank < 0)
                continue;

            if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active and not wild
            {
                if (cards_on_board[i].card_type_selected != card_class.card_type.wild //and not wild
                    || (cards_on_board[i].card_type_selected == card_class.card_type.wild && cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp > 0))      //or wild but with padlock
                {
                    if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
                    {                       
                        avaibleRanksOnBoard.Add(cards_on_board[i].my_rank);
                    }
                }
            }
        }

        //search an useful card in the deck
        int random_start_point = Random.Range(0, avaibleRanksOnBoard.Count);
        bool cardFounded = false;
        for (int i = random_start_point; i < avaibleRanksOnBoard.Count; i++)
        {
            cardFounded = FoundUsefulCardInTheDeck(avaibleRanksOnBoard[i]);
            if (cardFounded)
                return;
        }

        for (int i = 0; i < random_start_point; i++)
        {
            cardFounded = FoundUsefulCardInTheDeck(avaibleRanksOnBoard[i]);
            if (cardFounded)
                return;
        }

        //there is no card with that requirement, so cheat and create it now!
        if (allowCheatWhenGiveANewCard)
            GiveAnUsefulCardForSure_cheat();

    }

    bool FoundUsefulCardInTheDeck(int avaibleRanck)
    {

        if (deck.this_deck.main_rule_selected == main_rule.chain)
        {
            int TargetUpperRanck = avaibleRanck;
            TargetUpperRanck++;
            if (TargetUpperRanck > 12)
                TargetUpperRanck = 0;

            int TargetLoverRanck = avaibleRanck;
            TargetLoverRanck--;
            if (TargetUpperRanck < 0)
                TargetUpperRanck = 12;

            for (int d = current_deck_position; d < deck_cards.Length; d++)
            {
                if (deck_cards[d].rank == TargetUpperRanck || deck_cards[d].rank == TargetLoverRanck)
                {
                    SwapDeckCards(current_deck_position, d);
                    return true;
                }
            }
        }        

        return false;
    }

    void GiveAnUsefulCardForSure_cheat()
    {
        Debug.Log("Give_an_useful_card_for_sure(cheat) ... current_deck_position: " + current_deck_position);

        //search a face up card from the board
        int random_start_point = Random.Range(0, cards_on_board.Length);
        bool card_found = false;
        for (int i = random_start_point; i < cards_on_board.Length; i++)
        {
            if (cards_on_board[i].my_rank < 0)
                continue;

            if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active and not wild
            {
                if (cards_on_board[i].card_type_selected != card_class.card_type.wild //and not wild
                    || (cards_on_board[i].card_type_selected == card_class.card_type.wild && cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp > 0))      //or wild but with padlock
                {
                    if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
                    {
                        if(!deckCustom.deck_custom_card_list[current_deck_position].isManual)
                        {
                            print("Random");
                            deck_cards[current_deck_position].rank = cards_on_board[i].my_rank;
                        }
                        else
                        {
                            print("Manual");
                            deck_cards[current_deck_position].rank = deckCustom.deck_custom_card_list[current_deck_position].card.rank;
                        }
                        card_found = true;
                        break;
                    }
                }
            }
        }

        if (!card_found)//if not found yet, continue the search
        {
            for (int i = 0; i < random_start_point; i++)
            {
                if (cards_on_board[i].my_rank < 0)
                    continue;

                if (cards_on_board[i].This_card_is_free() && cards_on_board[i].gameObject.activeSelf == true && cards_on_board[i].face_up) //if this card is active and not wild
                {
                    if (cards_on_board[i].card_type_selected != card_class.card_type.wild //and not wild
                        || (cards_on_board[i].card_type_selected == card_class.card_type.wild && cards_on_board[i].padlock_model_selected == card.padlock_model.normal && cards_on_board[i].current_padlock_hp > 0))      //or wild but with padlock
                    {
                        if (cards_on_board[i].padlock_model_selected != card.padlock_model.royal)
                        {
                            deck_cards[current_deck_position].rank = cards_on_board[i].my_rank;
                            card_found = true;
                            break;
                        }
                    }
                }
            }
        }
        
        if (card_found)
        {
            //print("card_found");
            switch (deck.this_deck.main_rule_selected)
            {
                case main_rule.chain:
                    Change_rank_by_one();
                    break;
            }
            unused_card_count = 0;

        }
    }


    void Change_rank_by_one()
    {
        print("Change_rank_by_one");
        if(!deckCustom.deck_custom_card_list[current_deck_position].isManual)
        {
            print("[4]");
            if (Random.Range(0, 2) > 0)
            {
                deck_cards[current_deck_position].rank++;
                if (deck_cards[current_deck_position].rank > 12)
                    deck_cards[current_deck_position].rank = 0;
            }
            else
            {
                deck_cards[current_deck_position].rank--;
                if (deck_cards[current_deck_position].rank < 0)
                    deck_cards[current_deck_position].rank = 12;
            }
        }
        else
        {
            print("[5]");
            print("current_deck_position : " + current_deck_position);
            print("deck_custom_card_list RANK : " + deckCustom.deck_custom_card_list[current_deck_position].card.rank);
            deck_cards[current_deck_position].rank = deckCustom.deck_custom_card_list[current_deck_position].card.rank;
            print("deck_cards rank : "+deck_cards[current_deck_position].rank);
        }
      
    }

    void SwapDeckCards(int cardA, int cardB)
    {
        card_class a = new card_class();
        a = CopyCard(deck_cards[cardA]);

        card_class b = deck_cards[cardB];
        b = CopyCard(deck_cards[cardB]);

        deck_cards[cardA] = b;
        deck_cards[cardB] = a;

    }

    card_class CopyCard(card_class copyThis)
    {
        card_class newCard = new card_class();

        newCard.rank = copyThis.rank;
        newCard.suit = copyThis.suit;
        newCard.card_type_selected = copyThis.card_type_selected;
        newCard.collectable_bonus_selected = copyThis.collectable_bonus_selected;
        newCard.joker_type_selected = copyThis.joker_type_selected;
        newCard.board_card = copyThis.board_card;

        return newCard;
    }

    void TryToNeverGiveSameCardRankTwiceInARow()
    {
        //search upper rank
        int TargetRanck = deck_cards[current_deck_position].rank++;
        if (TargetRanck == 12)
            TargetRanck = 0;

        for (int i = current_deck_position; i < deck_cards.Length; i++)
        {
            if (deck_cards[i].rank == TargetRanck)
            {
                SwapDeckCards(current_deck_position, i);
                return;
            }
        }

        //search lover ranck
        TargetRanck = deck_cards[current_deck_position].rank--;
        if (TargetRanck < 0)
            TargetRanck = 12;

        for (int i = current_deck_position; i < deck_cards.Length; i++)
        {
            if (deck_cards[i].rank == TargetRanck)
            {
                SwapDeckCards(current_deck_position, i);
                return;
            }
        }

        //there is no card in the deck with these requisite, so cheat and create it now!
        if (allowCheatWhenGiveANewCard)
        {
            Change_rank_by_one();
        }
    }

    IEnumerator Take_card_from_deck(bool undo)
    {
        //print("Take_card_from_deck");
        if (undo)
        {
            new_card_anim.Play("undo_to_deck_card_anim");
            yield return new WaitForSeconds(0.05f);
            unused_card_count--;
            Update_combo_text();
        }
        else
        {
            print("undo : " + undo);
            current_target_deck_position++;
            if (game_started)
            {
                if (unused_card_count >= give_an_useful_card_not_less_than_n_new_card_from_the_deck)
                {
                    print("[1]");
                    GiveAnUsefulCardForSure_cheat();
                }
                else 
                {
                    if (deck_cards[current_deck_position].rank == previous_card.rank && never_give_same_card_rank_twice_in_a_row)
                    {
                        print("[2]");
                        Change_rank_by_one();
                    }
                }

                //check if the current card can be used
                int avaible_move = Check_avaible_moves(previous_card);
                if (avaible_move >= 0) //if there is a move
                {
                    if (show_unused_card_when_player_take_a_new_card_from_the_deck)
                    {
                        StartCoroutine(Shake_card(cards_on_board[avaible_move].transform));
                    }
                }
                else
                {
                    unused_card_count++;
                }
            }

            current_deck_card = new card_class();

            current_deck_card = CopyCard(deck_cards[current_deck_position]);

            current_deck_card.card_type_selected = deck_cards[current_deck_position].card_type_selected;
            current_deck_card.joker_type_selected = deck_cards[current_deck_position].joker_type_selected;
            current_deck_card.suit = deck_cards[current_deck_position].suit;
            current_deck_card.rank = deck_cards[current_deck_position].rank;

            Add_new_card_to_target_deck();

            new_card_anim_sprite.sprite = Assign_the_sprite_of_this_card(current_deck_card);
            yield return new WaitForSeconds(0.1f);

            new_card_anim.Play("new_card_anim");
            yield return new WaitForSeconds(animation_duration/* + 0.1f*/);

            previous_combo_count = combo_count;
            combo_count = 0;
            Update_combo_text();

            same_color_combo = 0;
            same_suit_combo = 0;
            if (keep_best_combo_multiplier_selected != keep_best_combo_multiplier.always)
                score_multiplier = 0;

            Update_score_multiplier();
        }

        if (!game_started)
        {
            game_started = true;

            target_new_card.gameObject.SetActive(true);

        }

        target_new_card.sprite = Assign_the_sprite_of_this_card(current_deck_card);
        target_new_card.enabled = true;

        if (!goals_window_is_open)
        {
            player_can_move = true;
        }

    }

    #endregion

    #region win and lose conditions
    void Setup_goals_window()
    {
        if (show_goals_window_at_start_selected == show_goals_window_at_start.always
            || (show_goals_window_at_start_selected == show_goals_window_at_start.only_on_first_board))
        {
            if (goal_toggle_list == null) //if you don't have create the list yet
            {
                string toggle_name = "";

                bool star_toggle = false;
                if (star_score_selected == star_score.gain_one_star_for_each_win_condition_satisfied)
                    star_toggle = true;

                GameObject temp_goal_toggle = Instantiate(goal_toggle);

                switch (win_condition_selected)
                {

                    case deck.win_condition.collect_all_cards:
                        toggle_name = "Collect all cards";
                        break;

                    case deck.win_condition.collect_all_cards_n_in_times:
                        toggle_name = "Collect all card in " + time_collect_all_card;
                        break;

                    case deck.win_condition.reach_target_score:
                        toggle_name = "Reach this score: " + win_target_score.ToString("N0");
                        break;

                    case deck.win_condition.reach_this_combo_length:
                        toggle_name = "Reach this combo: " + reach_this_combo_length_target.ToString("N0");
                        break;

                    case deck.win_condition.reach_this_combo_multiplier:
                        toggle_name = "Reach this combo multiplier: " + reach_this_combo_multiplier_target.ToString();
                        break;

                    case deck.win_condition.reach_this_combo_sum:
                        toggle_name = "Reach this combo sum: " + reach_this_combo_sum_target.ToString("N0");
                        break;

                    case deck.win_condition.reach_this_star_score_sum:
                        toggle_name = "Gain " + target_star_score_sum.ToString("N0") + " stars";
                        break;

                    case deck.win_condition.collect_card_at_time_on_card_page:
                        toggle_name = "Collect countsown card on time";
                        break;
                }
                goal_toggle_list = temp_goal_toggle.GetComponent<goal_toggle>();
                goal_toggle_list.Setup(toggle_name, win_conditions_satisfied, star_toggle);
                temp_goal_toggle.transform.SetParent(goals_screen_list, false);
            }
            //else //list already exist, just update it
            //{
            //    goal_toggle_list.Done(win_conditions_satisfied);
            //}
            goals_window_is_open = true;
        }
        else
        {
            goals_window_is_open = false;
        }
        goals_screen.SetActive(goals_window_is_open);

    }

    public void Goals_window_ok_button()
    {
        Play_sfx(gui_button_sfx);
        goals_screen.SetActive(false);
        goals_window_is_open = false;

        if (!game_started)
        {
            Ask_new_card(false);
        }
        else
        {
            player_can_move = true;
        }

    }

    bool This_win_conditon_is_active(win_condition condition)
    {
        bool return_this = false;
        if (win_condition_selected == condition)
        {
            return_this = true;
        }
        return return_this;
    }

    public void Update_gold_cards_count()
    {/*
		if (total_gold_cards > 0)
		{
			gold_cards_count_text.text = total_gold_cards.ToString() + " / " + gold_cards_taken.ToString();
			gold_cards_count_text.transform.parent.gameObject.SetActive(true);
		}
		else
			gold_cards_count_text.transform.parent.gameObject.SetActive(false);*/

        if (game_started)
            Check_how_many_gold_card_remain_on_board();
    }




    void This_win_condition_is_satisfied(win_condition condition)
    {
        if (win_condition_selected == condition && !win_conditions_satisfied)
        {
            win_conditions_satisfied = true;
            win_condition_satisfied_in_this_board = true;
        }
    }

    //int How_many_win_conditons_are_satisfied()
    //{
    //	int count = 0;
    //		if (win_conditions_satisfied)
    //			count++;

    //	return count;
    //}

    //private bool All_win_condition_are_satisfied()
    //{
    //	//Debug.Log ("All_win_condition_are_satisfied: " + How_many_win_conditons_are_satisfied() + "/" + number_of_win_conditions);

    //	if (How_many_win_conditons_are_satisfied == number_of_win_conditions)
    //		return true;
    //	else
    //		return false;

    //}

    void Check_how_many_gold_card_remain_on_board()
    {
        if (gold_cards_taken == total_gold_cards)//no gold card left on board
        {
            all_gold_cards_collected_in_this_board = true;
            collect_all_gold_cards_in_n_boards_current_count++;
        }
        //else //some gold card remain 
        //Debug.Log ("total: " + total_gold_cards + " ... taken: " + gold_cards_taken + " ... target: " + collect_n_gold_cards_target + " ... total taken in al boards: " + total_gold_cards_taken_in_all_boards);
    }

    bool isWinOnTime;

    public void Check_how_many_card_remain_on_board()
    {
        //Debug.Log ("Check_how_many_card_remain_on_board: " + current_total_card_on_board + " ... " + current_total_padlock_hp_on_board +"/" + total_padlock_hp_on_board);
        if ((current_total_card_on_board + current_total_padlock_hp_on_board) == 0)
        {
            all_cards_collected_in_this_board = true;

            collect_all_cards_in_n_boards_current_count++;
            
            //if there is only a board or each board must be win independently
            if (This_win_conditon_is_active(win_condition.collect_all_cards))
            {
                This_win_condition_is_satisfied(win_condition.collect_all_cards);
            }
            else if (time_collect_all_card > 0)
            {
                if (!isWinOnTime)
                {
                    This_win_conditon_is_active(win_condition.collect_all_cards_n_in_times);
                    This_win_condition_is_satisfied(win_condition.collect_all_cards_n_in_times);
                    isWinOnTime = true;
                }
            }
            else
            {
                isWinOnTime = false;
            }
            Show_end_screen();
        }
        
        Check_final_card_left();       
    }

    public void Check_final_card_left()
    {
        if(deck_card_left == 1)
        {
            //end_card_image.gameObject.SetActive(false);
            //deck_sprite_except_final_card.enabled = false;
            //deck_final_card.position = Vector3.zero;
        }
    }

    bool Check_if_show_win_screen_now()
    {
        Debug.Log("Check_if_show_win_screen_now");
        bool return_this = false;
        if (!game_end && !play_until_no_more_cards_then_check_win_condition)
        {
            Show_end_screen();
            return_this = true;
        }

        return return_this;
    }

    IEnumerator Update_goals_window()
    {
        if (current_total_card_on_board > 1)
        {
            //open window
            player_can_move = false;
            goals_screen.SetActive(true);
            goals_window_is_open = true;

            //update condition list
            yield return new WaitForSeconds(show_win_or_lose_screen_after_delay);
            goal_toggle_list.Done(true);
            Play_sfx(reach_goal_sfx);
        }

    }

    //int Get_condition_ID(win_condition condition)
    //{
    //    if (win_condition_selected == condition)
    //    {
    //        return i;
    //    }
    //    return -1;
    //}

    void Check_win_condition(win_condition condition, float current_value, float target_value)
    {
        //Debug.Log("Check_win_condition: " + condition);
        if (current_value >= target_value && This_win_conditon_is_active(condition))
        {

            if (!win_conditions_satisfied)
            {
                This_win_condition_is_satisfied(condition);


                if ((win_conditions_indispensable_selected == win_conditions_indispensable.all )
                || (win_conditions_indispensable_selected == win_conditions_indispensable.just_one ))
                {
                    if (!Check_if_show_win_screen_now())
                    {
                        if (show_goal_window_at_each_win_condition_satisfied)
                            StartCoroutine(Update_goals_window());
                        else
                            Play_sfx(reach_goal_sfx);
                    }
                }
                else
                {
                    Debug.Log(current_total_card_on_board);
                    if (current_total_card_on_board > 1)
                    {
                        if (show_goal_window_at_each_win_condition_satisfied)
                            StartCoroutine(Update_goals_window());
                        else
                            Play_sfx(reach_goal_sfx);
                    }
                }
            }

        }
    }

    void Show_end_screen()
    {
        //Debug.Log("Show_end_screen: " + (current_board + 1) + "/" + board_array_length);
        last_board = true;
        //Calculate_final_score();
        Start_calculate_final_score();
    }

    IEnumerator End_recap_screen(string called_from = "")
    {
        if (!game_end)
        {
            Debug.Log(called_from + " > " + "recap");

            game_end = true;
            player_can_move = false;
            if (This_win_conditon_is_active(win_condition.reach_target_score) && score_gained_in_previous_boards >= win_target_score)
            {
                player_win = true;
            }

            //if (multiple_stage_solved)
            //my_end_screen.End_screen_type(true, false);//win
            //else
            my_end_screen.End_screen_type(false, false);//lose

            yield return new WaitForSeconds(show_win_or_lose_screen_after_delay);

            game_screen.SetActive(false);

            //my_end_screen.Update_recap_list();

            my_end_screen.Show_end_screen(true);
        }
    }

    IEnumerator You_win()//show win screen
    {
        if (!game_end)
        {
            // Debug.Log("win");

            game_end = true;
            player_win = true;
            player_can_move = false;

            my_end_screen.End_screen_type(true, false);//win

            yield return new WaitForSeconds(show_win_or_lose_screen_after_delay);

            Play_sfx(win_sfx);

            game_screen.SetActive(false);


            my_end_screen.Show_end_screen(true);
            //end_screen.SetActive(true);
        }
    }

    IEnumerator You_lose()//show lose screen
    {
        if (!game_end)
        {
            Debug.Log("lose");
            game_end = true;
            player_can_move = false;
            player_win = false;

            my_end_screen.End_screen_type(false, false);//lose

            yield return new WaitForSeconds(show_win_or_lose_screen_after_delay);

            Play_sfx(lose_sfx);

            game_screen.SetActive(false);

            my_end_screen.Show_end_screen(true);
        }
    }


    #endregion

    #region score
    void Undo_score()
    {
        Debug.Log("Undo_score()");
        if (previous_bonus_selected == bonus_type.none)
        {
            combo_count--;
            Update_combo_text();

            same_color_combo--;
            if (same_color_combo < 0)
                same_color_combo = 0;

            same_suit_combo--;
            if (same_suit_combo < 0)
                same_suit_combo = 0;

            score_multiplier = undo_score_multiplier;
            Update_score_multiplier();
        }

        current_score -= undo_previous_score;
        score_text.text = current_score.ToString("N0");

        star_score_bar.UpdateValueScore(current_score);
        //target_score_slider.value = current_score;
    }


    void Calculate_final_score(StateFinalScore stateFinal)
    {
        print("Calculate_final_score : "+ stateFinal);

        //Debug.Log ("Calculate_final_score ...previous: " + score_gained_in_previous_boards);
        int total_final_score = 0;// score_gained_in_previous_boards;

        int card_in_deck_real = (deck_card_left - 1);
        int score_bonus = score_for_each_deck_card_spared * card_in_deck_real;

        int temp_current_score = current_score - score_bonus;

        total_final_score += current_score;
        if (This_win_conditon_is_active(win_condition.reach_target_score))
        {
            Check_win_condition(win_condition.reach_target_score, total_final_score, win_target_score);
        }

        current_score = total_final_score;
        //print("current score [Calculate_final_score] : " + current_score);
        Calculate_star_score();
        my_end_screen.Show_final_score(score_bonus, temp_current_score -= score_bonus, isGameOver);

        score_gained_in_previous_boards = total_final_score;

        if(GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.number_level_got++;
        }

        if ((int)stateFinal == 0)
        {
            print("You Win!");
            StartCoroutine(You_win());
        }
        else
        {
            print("You Lose!");
            StartCoroutine(You_lose());
        }
    }

    public void Start_calculate_final_score()
    {
        left_card_in_deck = deck_card_left - 1;
        StartCoroutine(Calculate_final_score_couroutine());
    }

    //public int i = 0;
    public int left_card_in_deck;
    public IEnumerator Calculate_final_score_couroutine()
    {
        print("deck card left : "+deck_card_left);
        if (win_condition_selected == win_condition.collect_all_cards_n_in_times && !isWinOnTime)
        {
            Calculate_final_score(StateFinalScore.lose_time_up);
            yield return 0;

        }
        else if (win_condition_selected == win_condition.collect_card_at_time_on_card_page && is_gameover_time_on_card)
        {
            Calculate_final_score(StateFinalScore.lose_time_up_card);
            yield return 0;
        }
        else
        {
            if (left_card_in_deck > 0)
            {
                //print("left_card_in_deck : "+ left_card_in_deck);
                left_card_in_deck--;
                yield return new WaitForSeconds(0.05f);
                deck_card_left_text.text = left_card_in_deck.ToString();
                current_score += score_for_each_deck_card_spared;
                score_text.text = "" + (current_score);
                star_score_bar.UpdateValueScore(current_score);
                if(GameMaster.my_GameMaster)
                {
                    GameMaster.my_GameMaster.Start_Show_Debug_Text("Calculate final score", 0.05f);
                }
                StartCoroutine(Calculate_final_score_couroutine());
            }
            else //left_card_in_deck <=0
            {
                //if(deck_card_left <= 0 && cards_on_board.Length > 0)
                //{
                //    //Calculate_final_score(StateFinalScore.lose_do_not_card_in_deck);
                //    print("lose_do_not_card_in_deck");
                //}
                //else
                //{
                    Calculate_final_score(StateFinalScore.win);
                //}

                yield return 0;
            }
            yield return 0;
        }       
        
    }



    void Update_combo_text()
    {
        combo_text.text = "Combo: " + combo_count.ToString();

        if (combo_count >= min_combo_lenght_to_trigger_the_multiplier)
        {
            combo_text.gameObject.SetActive(true);
            end_combo_obj.SetActive(true);
            if (best_combo < combo_count)
                best_combo = combo_count;
        }
        else
        {
            combo_text.gameObject.SetActive(false);
            end_combo_obj.SetActive(false);
        }
    }

    void Update_combo_sum_gui()
    {
        //combo_sum_count_text.text = "";//total_combo_sum.ToString() + "/" + combo_sum_target_to_trigger_the_score_bonus.ToString();
        //combo_sum_slider.value = total_combo_sum;
    }

    void Calculate_star_score()
    {
        current_star_score = 0;
        switch (star_score_selected)
        {
            case star_score.less_card_on_board_more_stars_you_gain:
                int count = 0;
                for (int i = 0; i < 3; i++)
                {
                    if (current_total_card_on_board <= card_left_on_board_to_gain_n_stars[i])
                    {
                        current_star_score = 3 - count;
                        break;
                    }
                    count++;
                }

                break;


            case star_score.gain_one_star_for_each_score_steps_achieved:
                //print("current score : " + current_score);
                if (current_score >= star_score_steps[2])
                {
                    //print("GOT 3! | " +star_score_steps[2]);
                    current_star_score = 3;
                }
                else if (current_score >= star_score_steps[1])
                {
                    //print("GOT 2! | " + star_score_steps[1]);
                    current_star_score = 2;
                }
                else if (current_score >= star_score_steps[0])
                {
                    //print("GOT 1! | " + star_score_steps[0]);
                    current_star_score = 1;
                }
                break;


            case star_score.gain_one_star_for_each_win_condition_satisfied:
                for (int i = 0; i < number_of_win_conditions; i++)
                {
                    if (win_conditions_satisfied)
                        current_star_score++;
                }

                if (This_win_conditon_is_active(win_condition.collect_all_cards) && all_cards_collected_in_this_board)
                {
                    current_star_score++;
                }

                break;
        }

        current_star_score = Mathf.Clamp(current_star_score, 0, 3);//be sure that the star score stay between 0 and 3
        total_star_score += current_star_score;

        Check_win_condition(win_condition.reach_this_star_score_sum, total_star_score, target_star_score_sum);

        //Debug.Log ("star score: " + current_star_score[current_board] + " ... total_star_score: " + total_star_score);
    }

    void Update_score_match(card_class cardA, card_class cardB)
    {
        //print("Update_score_match(" + cardA.suit + "," + cardB.suit + ") .... previous: " + previous_match_combo);
        bool color_combo = false;
        float temp_score_multiplier = 0;

        //calculate the score of this move
        int add_to_score = 0;

        if (cardA.board_card)
            add_to_score += cardA.board_card.MyScoreValue();
        else
            add_to_score += target_deck_card_score;

        if (cardB.board_card)
            add_to_score += cardB.board_card.MyScoreValue();
        else
            add_to_score += target_deck_card_score;

        //combo count
        previous_combo_count = combo_count;
        combo_count++;

        Update_combo_text();


        if (combo_count >= min_combo_lenght_to_trigger_the_multiplier && add_to_score > 0)
            temp_score_multiplier += (float)(combo_count - min_combo_lenght_to_trigger_the_multiplier) * combo_score_multiplier;


        Check_win_condition(win_condition.reach_this_combo_length, combo_count, reach_this_combo_length_target);

        if (cardA.suit == cardB.suit)
        {
            //print("same suit match");

            if ((int)previous_match_combo == cardA.suit)
            {
                //print("same suit match combo!");
                same_suit_combo++;

                if (add_to_score > 0)
                    temp_score_multiplier += (float)same_suit_combo * suit_combo_score_multiplier;

                color_combo = true;
            }

            previous_match_combo = (match_combo)cardA.suit;
        }
        //else if ((cardA.suit == 2 || cardA.suit == 3 || cardA.joker_type_selected == card_class.joker_type.red || cardA.joker_type_selected == card_class.joker_type.normal)
        //   && (cardB.suit == 2 || cardB.suit == 3 || cardB.joker_type_selected == card_class.joker_type.red || cardB.joker_type_selected == card_class.joker_type.normal))
        //{
        //    //print("red match");//0 = clubs; 1 = spades; 2 = diamonds; 3 = hearts; 100 = neutre; 101 = red; 102 = black
        //    if (previous_match_combo == match_combo.red || previous_match_combo == match_combo.diamonds || previous_match_combo == match_combo.hearts)
        //        color_combo = true;

        //    previous_match_combo = match_combo.red;

        //}
        //else if ((cardA.suit == 0 || cardA.suit == 1 || cardA.joker_type_selected == card_class.joker_type.black || cardA.joker_type_selected == card_class.joker_type.normal)
        //   && (cardB.suit == 0 || cardB.suit == 1 || cardB.joker_type_selected == card_class.joker_type.black || cardB.joker_type_selected == card_class.joker_type.normal))
        //{
        //    //print("black match");
        //    if (previous_match_combo == match_combo.black || previous_match_combo == match_combo.clubs || previous_match_combo == match_combo.spades)
        //        color_combo = true;

        //    previous_match_combo = match_combo.black;
        //}
        else
        {
            //print("no suit or color match");
            previous_match_combo = match_combo.none;
            same_suit_combo = 0;
            same_color_combo = 0;
        }

        if (color_combo)
        {
            //print("same color match combo!");
            same_color_combo++;
            if (add_to_score > 0)
                temp_score_multiplier += (float)same_color_combo * color_combo_score_multiplier;
        }

        UpdateComboSum();
        print("current_score [Update_score_match] : " + current_score + " | add_to_score : " + add_to_score);
        //CalculateNewScore(add_to_score);
    }

    public void Update_score(int add_to_score)
    {
        float temp_score_multiplier = 0;

        if (previous_bonus_selected == bonus_type.none)//combo and multiplier work only without bonus
        {
            previous_combo_count = combo_count;
            combo_count++;

            Update_combo_text();


            if (combo_count >= min_combo_lenght_to_trigger_the_multiplier && add_to_score > 0)
                temp_score_multiplier += (float)(combo_count - min_combo_lenght_to_trigger_the_multiplier) * combo_score_multiplier;


            Check_win_condition(win_condition.reach_this_combo_length, combo_count, reach_this_combo_length_target);

            if (current_deck_card.suit == previous_card.suit)
            {
                same_suit_combo++;

                if (add_to_score > 0)
                    temp_score_multiplier += (float)same_suit_combo * suit_combo_score_multiplier;

                same_color_combo++;

                if (add_to_score > 0)
                    temp_score_multiplier += (float)same_color_combo * color_combo_score_multiplier;
            }
            else //check if same color
            {
                same_suit_combo = 0;
                //0 = clubs; 1 = spades; 2 = diamonds; 3 = hearts; 100 = neutre; 101 = red; 102 = black
                if (((current_deck_card.suit == 0 || current_deck_card.suit == 1 || current_deck_card.suit == 102) && (previous_card.suit == 0 || previous_card.suit == 1 || previous_card.suit == 102))//black clor
                    || ((current_deck_card.suit == 2 || current_deck_card.suit == 3 || current_deck_card.suit == 101) && (previous_card.suit == 2 || previous_card.suit == 3 || previous_card.suit == 101)))//red color
                {
                    same_color_combo++;
                    if (add_to_score > 0)
                        temp_score_multiplier += (float)same_color_combo * color_combo_score_multiplier;
                }
                else
                {
                    same_color_combo = 0;
                }
            }

            undo_score_multiplier = score_multiplier;
            if (keep_best_combo_multiplier_selected != keep_best_combo_multiplier.no)
            {
                if (temp_score_multiplier > score_multiplier)
                {
                    score_multiplier = temp_score_multiplier;
                    best_combo_multiplier = score_multiplier;
                }
            }
            else
                score_multiplier = temp_score_multiplier;

            //Update_score_multiplier();

            Check_win_condition(win_condition.reach_this_combo_multiplier, score_multiplier, reach_this_combo_multiplier_target);

            //combo sum
            //UpdateComboSum();
        }

        //print("current_score [Update_score] : " + current_score + " | add_to_score : "+add_to_score);
        //print("current_score : " + current_score);
        CalculateNewScore(add_to_score);
        add_to_score = 0;
    }

    private int countOfCalculate;
    private bool is_calculated;
    void CalculateNewScore(int add_to_score)
    {
        //print("current_score [CalculateNewScore - start] : " + current_score);
        //print("add_to_score [CalculateNewScore - start] : " + add_to_score);
        float temp_add_to_score = add_to_score;
        //temp_add_to_score *= score_multiplier;


        //add_to_score += Mathf.FloorToInt(temp_add_to_score);

        if (previous_bonus_selected == bonus_type.destroy_some_card)
        {
            undo_previous_score += add_to_score;
        }
        else
        {
            undo_previous_score = add_to_score;
        }


        //print("add_to_score [before UpdateValueScore] : " + add_to_score);

        if (add_to_score > 0)
        {
            if (!is_calculated)
            {
                //print("countOfCalculate++");
                countOfCalculate++;
                current_score += add_to_score;
                score_text.text = current_score.ToString("N0");

                is_calculated = true;
                add_to_score = 0;
            }
        }

        //print("count of calculate : "+countOfCalculate);

        star_score_bar.UpdateValueScore(current_score);

        Check_win_condition(win_condition.reach_target_score, current_score, win_target_score);
        is_calculated = false;
    }

    void UpdateComboSum()
    {
        //print("current_score [UpdateComboSum - start] : " + current_score);
        //combo sum
        if (combo_count >= min_combo_lenght_to_trigger_the_sum)
        {
            total_combo_sum++;
            //Debug.Log("total combo sum: " + total_combo_sum);

            if (total_combo_sum >= combo_sum_target_to_trigger_the_score_bonus && !combo_sum_bonus_obtained)
            {
                //print("give_this_score_bonus_when_reach_the_combo_sum : " + give_this_score_bonus_when_reach_the_combo_sum);
                current_score += give_this_score_bonus_when_reach_the_combo_sum;
                combo_sum_bonus_obtained = true;
            }

            Update_combo_sum_gui();

            Check_win_condition(win_condition.reach_this_combo_sum, total_combo_sum, reach_this_combo_sum_target);
        }

        //print("current_score [UpdateComboSum - end] : " + current_score);
    }

    void Update_score_multiplier()
    {
        total_combo_multiplier_text.text = "x" + score_multiplier.ToString();
    }

    #endregion


    public void Play_sfx(AudioClip my_clip)
    {
        if (GameMaster.my_GameMaster)
        {
            if (!GameMaster.my_GameMaster.audio_on)
                return;
        }


        if (my_clip)
        {
            GetComponent<AudioSource>().clip = my_clip;
            GetComponent<AudioSource>().Play();
        }
    }

    #region Streak
    public void Reset_Count_Streak()
    {
        if (use_streak)
        {
            //print("Reset_Count_Streak!");
            streak_bar.Clear_streak(count_streak);
            count_streak = 0;
        }
    }

    public void Update_streak(int rank, int suit)
    {
        if (use_streak)
        {
            count_streak++;
            streak_bar.Update_point_streak(current_streak_bar_using, count_streak - 1, suit);
        }
    }

    public void Remove_streak()
    {
        if (use_streak)
        {
            count_streak--;
        }
    }
    #endregion

}
