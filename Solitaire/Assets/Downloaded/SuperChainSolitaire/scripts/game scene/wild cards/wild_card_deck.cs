﻿using UnityEngine;
using System.Collections;

public class wild_card_deck : MonoBehaviour {

	public GameObject wild_card_prefab;

	public int max_wild_cards;//how many wild card you can have
	public int current_wild_cards;//how many wild card you have now

	//[HideInInspector]
	public GameObject[] my_cards;//store card position in order to use them as target for the undo animation
	//[HideInInspector]
	public deck.card_class[] saved_deck_array;

	[HideInInspector]
	public int last_card_removed_position;//the position to target with the undo animation

	//how placing the cards
	public float spacing_x;
	public float spacing_y;
	float spacing_z = -0.01f;

    public bool initiated;

	// Use this for initialization
	void Start () {
	
		Initiate ();

	}
	

	public void Initiate()
	{
        if (initiated)
            return;

        my_cards = new GameObject[max_wild_cards];

		for (int i = 0; i < max_wild_cards; i++)
		{
			my_cards[i] =  (GameObject)Instantiate(wild_card_prefab, new Vector3(transform.position.x + spacing_x*i, transform.position.y + spacing_y*i, transform.position.z + spacing_z*i), Quaternion.identity);
			my_cards[i].GetComponent<wild_card>().my_array_position = i;
			my_cards[i].GetComponent<wild_card>().my_wild_card_deck = this;
			my_cards[i].transform.parent = this.transform;
			my_cards[i].gameObject.SetActive(false);

        }

        initiated = true;

    }

	public void Empty_deck()
	{
		for (int i = current_wild_cards; i > 0; i--)
			Remove_card (i);
	}

	public void Save_this_deck()
	{
		saved_deck_array = new deck.card_class[current_wild_cards];
		for (int i = 0; i < current_wild_cards; i++)
		{
			saved_deck_array[i] = new deck.card_class();
			saved_deck_array[i].rank = my_cards[i].GetComponent<wild_card> ().my_rank;
			saved_deck_array[i].joker_type_selected = my_cards [i].GetComponent<wild_card> ().my_joker;
		}
	}

	public void Ripristinate_wild_cards()
	{
		for (int i = current_wild_cards; i < saved_deck_array.Length; i++)
		{
			Add_wild_card(saved_deck_array[i].rank,saved_deck_array[i].joker_type_selected);

		}
	}

	public void Add_wild_card(int rank,deck.card_class.joker_type joker)
	{
        //Debug.Log("Add_wild_card: " + rank);
        if (current_wild_cards < max_wild_cards)
        {
            Assign_rank_and_sprite(rank, joker, current_wild_cards);
            current_wild_cards++;
        }
        else
            Debug.LogWarning("Can't add more wild cards");
    }

    public void Update_sprites()
    {
        for (int i = 0; i < current_wild_cards; i++)
        {
            if (my_cards[i + 1].GetComponent<wild_card>().my_joker == deck.card_class.joker_type.none)
                my_cards[i].GetComponent<SpriteRenderer>().sprite = deck.this_deck.wild[my_cards[i + 1].GetComponent<wild_card>().my_rank];
            else
                my_cards[i].GetComponent<SpriteRenderer>().sprite = deck.this_deck.wild_joker;
        }
    }

    void Assign_rank_and_sprite(int rank, deck.card_class.joker_type joker, int array_position)
	{
		my_cards[array_position].GetComponent<wild_card> ().my_rank = rank;
		my_cards [array_position].GetComponent<wild_card> ().my_joker = joker;

		if (joker == deck.card_class.joker_type.none)
			my_cards [array_position].GetComponent<SpriteRenderer> ().sprite = deck.this_deck.wild [rank];
		else
			my_cards [array_position].GetComponent<SpriteRenderer> ().sprite = deck.this_deck.wild_joker;

		my_cards[array_position].gameObject.SetActive(true);

		deck.this_deck.player_can_move = true;
	}

	public void Remove_card(int array_position)
	{
		//remove last card
		current_wild_cards--;
		my_cards[current_wild_cards].gameObject.SetActive(false);
		//scale card places 
		for (int i = array_position; i < current_wild_cards; i++)
			{
			Assign_rank_and_sprite(my_cards[i+1].GetComponent<wild_card>().my_rank,my_cards[i+1].GetComponent<wild_card>().my_joker,i);
			}
	}
	

}
