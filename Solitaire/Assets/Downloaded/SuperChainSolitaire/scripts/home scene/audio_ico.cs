﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class audio_ico : MonoBehaviour {

    public Image my_image;

    void Start()
    {
        Update_me();
    }

    public void Update_me()//show the on/off icon
    {
        if (GameMaster.my_GameMaster)
        {
            if (GameMaster.my_GameMaster.audio_on)
                my_image.fillAmount = 1;
            else
                my_image.fillAmount = 0.5f;
        }
    }
}
