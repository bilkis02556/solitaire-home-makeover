﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class stageButton : MonoBehaviour {

    public enum ButtonTypeState { OpenDefaultScene,OpenGameplayScene}
    public ButtonTypeState buttonType;

    public int number; //the number to show on this icon
    public int stage_number; //the number of the stage scene to open "stage_0", "stage_1"...
    public int progress;//the board in that stage to open
    public int total_boards_in_this_stage;//how many board it this stage

    public Text stage_number_text;
    public Text progress_text;

    public bool padlock;
    public Sprite padlock_sprite;
    public Sprite normal_sprite;

    //public bool isOpen

    //update the infos show by this button
    public void Update_me(int my_number, int my_progress, bool my_padlock, int stage_scene_number = 0)
    {

        if (GameMaster.my_GameMaster.create_a_stage_icon_for_each_board)
            {
            total_boards_in_this_stage = 1;
            stage_number = stage_scene_number;
            number = my_number;
            }
        else
            {
            stage_number = my_number;
            total_boards_in_this_stage = GameMaster.my_GameMaster.boards_in_each_stage[stage_number];
            }

        stage_number_text.text = (my_number + 1).ToString();

        Update_padlock(my_padlock);
        Update_progress(my_progress);
    }

    public void Update_progress(int my_progress)
    {
        progress = my_progress;
        progress_text.text = my_progress.ToString() + "/" + total_boards_in_this_stage.ToString();

    }

    public void Update_padlock(bool my_padlock = false)
    {
        padlock = my_padlock;
        //GameMaster.my_GameMaster.number_stage_unlocked++;
        if (padlock)
        {
            //this.gameObject.SetActive(false);
            GetComponent<Image>().sprite = padlock_sprite;
            progress_text.gameObject.SetActive(false);
        }
        else
        {
            //this.gameObject.SetActive(true);
            GetComponent<Image>().sprite = normal_sprite;

            if (total_boards_in_this_stage <= 1)
            {
                progress_text.gameObject.SetActive(false);
            }               
            else
            {
                progress_text.gameObject.SetActive(true);
            }
        }        
    }

    public void Click_me()
    {

        if (padlock)
        {
            GameMaster.my_GameMaster.Gui_sfx(GameMaster.my_GameMaster.tap_error_sfx);

        }
        else
        {

            if (GameMaster.my_GameMaster.create_a_stage_icon_for_each_board)
                {
                Debug.Log(number + "," + progress);
                GameMaster.my_GameMaster.current_icon = number;
                GameMaster.my_GameMaster.current_stage = stage_number;
                GameMaster.my_GameMaster.current_board_in_this_stage[stage_number] = progress;
                GameMaster.my_GameMaster.Erase_this_board_save(stage_number, progress);
                }
            else
                {
                GameMaster.my_GameMaster.current_icon = stage_number;
                //if (progress == total_boards_in_this_stage)//you have solved this board, so restart it
                    //GameMaster.my_GameMaster.Erase_this_stage_save(stage_number);
                GameMaster.my_GameMaster.current_stage = stage_number;
                }

                

            GameMaster.my_GameMaster.Gui_sfx(GameMaster.my_GameMaster.tap_sfx);
            Invoke("Load_stage", 0.5f);
        }
    }

    void Load_stage()
    {
        //GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.loading);
        switch(buttonType)
        {
            case ButtonTypeState.OpenDefaultScene:
                SceneManager.LoadScene("stage_" + stage_number.ToString());
                break;
            case ButtonTypeState.OpenGameplayScene:
                SceneManager.LoadScene("GameplayScene");
                break;
        }
        
        //Application.LoadLevel("stage_" + stage_number.ToString());
    }
}
