﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SkinManager : MonoBehaviour
{

    [Range(0, 12)]
    public int cardToShowInPreview;
    public GameObject card_toggle;
    public DeckSprites[] deckSprite;
    [HideInInspector]
    public Transform card_container;
    [Space]
    public Sprite[] cards_back;
    [HideInInspector]
    public Transform card_back_container;
    

    [Space]
    public bool player_can_choose_background;
    public Sprite[] backgrounds;
    [HideInInspector]
    public GameObject BK_container;
    [HideInInspector]
    public Transform  background_container;
    public GameObject background_toggle;


    int selected_card;
    Toggle[] card_toggles;
    ToggleGroup card_ToggleGroup;

    int selected_card_back;
    Toggle[] card_back_toggles;
    ToggleGroup card_back_ToggleGroup;

    int selected_background;
    Toggle[] background_toggles;
    ToggleGroup background_ToggleGroup;

    [HideInInspector]
    public GameObject skinButton;

    public void AwakeMe() //initiate variables and generate skin menu contents
    {
        skinButton.SetActive(true);

        //cards
        card_ToggleGroup = card_container.GetComponent<ToggleGroup>();
        card_toggles = new Toggle[deckSprite.Length];
        for (int i = 0; i < deckSprite.Length; i++)
            {
            GameObject temp = Instantiate(card_toggle);
            temp.transform.GetChild(0).GetComponent<Image>().sprite = deckSprite[i].hearts[cardToShowInPreview];
            temp.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = deckSprite[i].hearts[cardToShowInPreview];

            temp.transform.SetParent(card_container,false);

            card_toggles[i] = temp.GetComponent<Toggle>();
            card_toggles[i].group = card_ToggleGroup;
        }


        //cards back
        card_back_ToggleGroup = card_back_container.GetComponent<ToggleGroup>();
        card_back_toggles = new Toggle[cards_back.Length];
        for (int i = 0; i < cards_back.Length; i++)
            {
            GameObject temp = Instantiate(card_toggle);
            temp.transform.GetChild(0).GetComponent<Image>().sprite = cards_back[i];
            temp.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = cards_back[i];

            temp.transform.SetParent(card_back_container, false);

            card_back_toggles[i] = temp.GetComponent<Toggle>();
            card_back_toggles[i].group = card_back_ToggleGroup;
        }


        //background
        background_ToggleGroup = background_container.GetComponent<ToggleGroup>();
        BK_container.SetActive(player_can_choose_background);
        background_toggles = new Toggle[backgrounds.Length];
        if (player_can_choose_background)
        {
            for (int i = 0; i < backgrounds.Length; i++)
            {
                GameObject temp = Instantiate(background_toggle);
                temp.transform.GetChild(0).GetComponent<Image>().sprite = backgrounds[i];
                temp.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = backgrounds[i];

                temp.transform.SetParent(background_container, false);

                background_toggles[i] = temp.GetComponent<Toggle>();
                background_toggles[i].group = background_ToggleGroup;
            }
        }




        Load();

    }

    void Start()
    {
    }

    void Save()
    {
        for (int i = 0; i < card_toggles.Length; i++)
            {
            if (card_toggles[i].isOn)
                {
                selected_card = i;
                break;
                }
            }

        for (int i = 0; i < card_back_toggles.Length; i++)
        {
            if (card_back_toggles[i].isOn)
            {
                selected_card_back = i;
                break;
            }
        }

        for (int i = 0; i < background_toggles.Length; i++)
        {
            if (background_toggles[i].isOn)
            {
                selected_background = i;
                break;
            }
        }

        PlayerPrefs.SetInt("skin_selected_card", selected_card);
        PlayerPrefs.SetInt("skin_selected_card_back", selected_card_back);
        PlayerPrefs.SetInt("skin_selected_background", selected_background);

        UpdateSkin();
    }

    void Load()
    {

        selected_card = PlayerPrefs.GetInt("skin_selected_card");
        selected_card_back = PlayerPrefs.GetInt("skin_selected_card_back");
        selected_background = PlayerPrefs.GetInt("skin_selected_background");

        card_ToggleGroup.SetAllTogglesOff();
        card_toggles[selected_card].isOn = true;


        card_back_ToggleGroup.SetAllTogglesOff();
        card_back_toggles[selected_card_back].isOn = true;

        background_ToggleGroup.SetAllTogglesOff();
        background_toggles[selected_background].isOn = true;
    }


    public void OK_button()
    {
        Save();
    }

    void UpdateSkin()
    {
        if (!deck.this_deck)
            return;

        //if you are in a game stage:

        if (player_can_choose_background)
            deck.this_deck.background_image.sprite = backgrounds[selected_background];

        deck.this_deck.Update_card_skin(deckSprite[selected_card], cards_back[selected_card_back]);
        deck.this_deck.Update_sprites_in_existing_cards();
    }
}
