﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using TMPro;

[CustomEditor(typeof(card)), CanEditMultipleObjects]
internal class card_editor : Editor
{

	public override void OnInspectorGUI()
	{

		card my_target = (card)target;
		EditorGUI.BeginChangeCheck();
		Undo.RecordObject(my_target, "card_edit");

		//my_target.isExtraPlusCard = EditorGUILayout

		my_target.my_setup_selected = (card.my_setup)EditorGUILayout.EnumPopup("setup", my_target.my_setup_selected);
		
		//countdown card
		my_target.is_countdown_card = EditorGUILayout.Toggle("is countdown card", my_target.is_countdown_card);
		if(my_target.is_countdown_card == true)
		{
			EditorGUI.indentLevel++;
			my_target.time_countdown_card = EditorGUILayout.FloatField("time countdown : ", my_target.time_countdown_card);
			EditorGUI.indentLevel--;
		}

		if (my_target.my_setup_selected == card.my_setup.manual)
		{
			EditorGUI.indentLevel++;
			my_target.card_type_selected = (deck.card_class.card_type)EditorGUILayout.EnumPopup("card type", my_target.card_type_selected);


			EditorGUI.indentLevel++;

			if (my_target.card_type_selected == deck.card_class.card_type.collectable_bonus)
			{
				if (my_target.collectable_bonus_selected == deck.bonus_type.none)
					GUI.color = Color.red;
				else
					GUI.color = Color.white;

				my_target.collectable_bonus_selected = (deck.bonus_type)EditorGUILayout.EnumPopup("collectable", my_target.collectable_bonus_selected);

				GUI.color = Color.white;
				if(my_target.collectable_bonus_selected == deck.bonus_type.add_card_in_deck)
				{
					EditorGUI.indentLevel++;
					my_target.always_face_up = true;
					my_target.is_extra_add_card = true;

					my_target.count_Add_Card_In_Deck = EditorGUILayout.IntField("Count Card Add In Deck", my_target.count_Add_Card_In_Deck);
					//my_target.count_Add_Card_Text = EditorGUILayout.ObjectField("Count Card Add", my_target.count_Add_Card_Text, typeof(TextMeshProUGUI),true) as TextMeshProUGUI;
					EditorGUI.indentLevel--;
				}
				else
				{
					my_target.always_face_up = false;
					my_target.is_extra_add_card = false;
					my_target.count_Add_Card_In_Deck = 0;
				}
			}
			else
			{
				my_target.collectable_bonus_selected = deck.bonus_type.none;
				my_target.joker_type_selected = (deck.card_class.joker_type)EditorGUILayout.EnumPopup("joker", my_target.joker_type_selected);
				if (my_target.joker_type_selected == deck.card_class.joker_type.none)
				{
					if (my_target.card_type_selected != deck.card_class.card_type.wild)
					{
						my_target.card_suit_selected = (card.card_suit)EditorGUILayout.EnumPopup("suit", my_target.card_suit_selected);
						if (my_target.card_suit_selected != card.card_suit.random)
							my_target.my_suit = (int)my_target.card_suit_selected;
					}


					my_target.card_rank_selected = (card.card_rank)EditorGUILayout.EnumPopup("rank", my_target.card_rank_selected);
					if (my_target.card_rank_selected != card.card_rank.random)
						my_target.my_rank = (int)my_target.card_rank_selected;

				}
				else //this is a joker
				{
					if (my_target.card_type_selected == deck.card_class.card_type.wild)
						my_target.joker_type_selected = deck.card_class.joker_type.normal;
				}
			}


			my_target.always_face_up = EditorGUILayout.Toggle("always face up", my_target.always_face_up);


			EditorGUI.indentLevel--;




			EditorGUI.indentLevel--;
		}
		else //automatic
		{
			my_target.card_type_selected = deck.card_class.card_type.normal;
			my_target.collectable_bonus_selected = deck.bonus_type.none;
			my_target.card_suit_selected = card.card_suit.random;
			my_target.card_rank_selected = card.card_rank.random;
			my_target.joker_type_selected = deck.card_class.joker_type.none;
			my_target.face_up = false;
		}

		//padlock setup
		my_target.padlock_model_selected = (card.padlock_model)EditorGUILayout.EnumPopup("padlock model", my_target.padlock_model_selected);
		if (my_target.padlock_model_selected != card.padlock_model.none)
		{
			EditorGUI.indentLevel++;

			if (my_target.card_type_selected == deck.card_class.card_type.collectable_bonus)
				my_target.padlock_model_selected = card.padlock_model.royal;

			my_target.padlock_hp = EditorGUILayout.IntSlider("padlock hp", my_target.padlock_hp, 1, 10);
			EditorGUI.indentLevel--;
		}



		my_target.editor_show_overlap = EditorGUILayout.Foldout(my_target.editor_show_overlap, "overlap detector");
		if (my_target.editor_show_overlap)
		{
			EditorGUI.indentLevel++;
			if (my_target.big_radius <= 0)
				GUI.color = Color.red;
			else
				GUI.color = Color.white;
			my_target.big_radius = EditorGUILayout.FloatField("big radius", my_target.big_radius);
			GUI.color = Color.white;

			if (my_target.small_radius <= 0)
				GUI.color = Color.red;
			else
				GUI.color = Color.white;
			my_target.small_radius = EditorGUILayout.FloatField("small radius", my_target.small_radius);
			GUI.color = Color.white;

			for (int i = 0; i < my_target.overlap_small.Length; i++)
			{
				my_target.overlap_small[i] = EditorGUILayout.ObjectField("small " + i, my_target.overlap_small[i], typeof(Transform), true) as Transform;

			}

			EditorGUI.indentLevel--;
		}

		my_target.editor_show_advanced = EditorGUILayout.Foldout(my_target.editor_show_advanced, "advanced");
		if (my_target.editor_show_advanced)
		{
			EditorGUI.indentLevel++;
			my_target.padlock_front = EditorGUILayout.ObjectField("padlock front SpriteRenderer", my_target.padlock_front, typeof(SpriteRenderer), true) as SpriteRenderer;
			my_target.padlock_back = EditorGUILayout.ObjectField("padlock back SpriteRenderer", my_target.padlock_back, typeof(SpriteRenderer), true) as SpriteRenderer;

			my_target.padlock_front_text = EditorGUILayout.ObjectField("padlock front text", my_target.padlock_front_text, typeof(Text), true) as Text;
			my_target.padlock_back_text = EditorGUILayout.ObjectField("padlock back text", my_target.padlock_back_text, typeof(Text), true) as Text;
			EditorGUI.indentLevel--;
		}

		if (EditorGUI.EndChangeCheck())
			EditorUtility.SetDirty(my_target);

	}

}
