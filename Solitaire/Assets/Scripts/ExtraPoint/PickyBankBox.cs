﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickyBankBox : MonoBehaviour
{
    public Button buttonPicky;
    public Image star;

    public Sprite starAvailable;
    public Sprite starUnAvailable;

    public Text numberStar;
    public int starAll;
    public int starCurrent;
    public int starGotAll;
    public int starGotCurrent;

    [Header("Coin")]
    public int coinGot;

    public bool isPicked;
    public bool canPicked;

    private int thisStarGot;

    public void SetPickBankBox(int _starAll)
    {
        this.starAll = _starAll;

        this.starGotCurrent = PlayerPrefs.GetInt("star_got_current");

        this.starCurrent = PlayerPrefs.GetInt("star_current");
        UpdateNumberStar();
    }
    public void UpdatePickyBankBox(int _starGot)
    {
        thisStarGot = _starGot;
        print("starGotCurrent : " + starGotCurrent);
        this.starGotCurrent = PlayerPrefs.GetInt("star_got_current");
        this.starCurrent = thisStarGot - starGotCurrent;
        UpdateNumberStar();
    }

    private void Update()
    {
        if (starCurrent >= starAll)
        {
            canPicked = true;
            star.sprite = starAvailable;
            buttonPicky.interactable = true;
        }
        else
        {
            canPicked = false;
            star.sprite = starUnAvailable;
            buttonPicky.interactable = false;
        }

        if(starGotCurrent > thisStarGot)
        {
            starGotCurrent = thisStarGot;
        }
    }

    public void UpdateNumberStar()
    {
        print(starCurrent + "/" + starAll);
        numberStar.text = starCurrent + "/" + starAll;
        PlayerPrefs.SetInt("star_current", starCurrent);
    }

    public void ClearPickyBankBox()
    {
        if(canPicked)
        {
            print("ClearPickyBankBox");
            starGotCurrent += 3;
            PlayerPrefs.SetInt("star_got_current", starGotCurrent);
            if (starCurrent>= 0)
            {
                starCurrent -= starAll;
            }
            if(GameMaster.my_GameMaster)
            {
                GameMaster.my_GameMaster.number_coin_got += coinGot; 
                GameMaster.my_GameMaster.Start_Show_Debug_Text("You clear picky bank [got coin : " + coinGot+" ]", 4);

            }
            UpdateNumberStar();
        }

    }

    public bool CanPick()
    {
        return canPicked;
    }

    public void ResetCanPick()
    {
        canPicked = false;
    }

    public void SetCurrentStarGot(int number)
    {
        starGotCurrent = number;
    }

}
