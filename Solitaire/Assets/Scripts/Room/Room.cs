﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public RoomManager my_roomManager;
    public MainMenuManager my_mainMenuManager;
    public CameraController cameraController;
    public int numberRoom;
    public Furniture[] furnitures;
    public Transform parentPositionCamera;
    [SerializeField]
    private List<Transform> positionCamera;

    [Header("Canvas Status")]
    public List<GameObject> canvasStatus;

    public bool isOpenCanvasFurniture;
    public void StartRoom()
    {
        if(RoomManager.instance)
        {
            my_roomManager = RoomManager.instance;
        }

        if(CameraController.instance)
        {
            cameraController = CameraController.instance;
        }

        for (int i = 0; i < furnitures.Length; i++)
        {
            print("StartRoom : "+i);
            furnitures[i].SetStartFurniture(this,RoomManager.instance.canvasFurniturePrefab,RoomManager.instance.canvasUIStatusPrefabs);
        }

        if(parentPositionCamera.childCount > 0)
        {
            for (int i = 0; i < parentPositionCamera.childCount; i++)
            {
                positionCamera.Add(parentPositionCamera.GetChild(i).transform);
            }
        }
    }

    public void UpdateRoom()
    {
        if (CameraController.instance)
        {
            cameraController = CameraController.instance;
        }
        for (int i = 0; i < furnitures.Length; i++)
        {
            print("UpdateRoom");
            furnitures[i].UpdateStatusCanvasUI();
            furnitures[i].BoughtAuto();
        }
        if (cameraController.StayOnOriginPosition())
        {
            IsOpenCanvasStatusUI(true);
        }
        else
        {
            IsOpenCanvasStatusUI(false);
        }
    }

    public void OpenCanvasFurniture()
    {
        isOpenCanvasFurniture = true;
        if(cameraController)
        {
            print("OpenCanvasFurniture");
            cameraController.SetWasZoom(true);

            my_mainMenuManager.IsOpenCanvasRoom(false);
        }
    }
    public void CloseCanvasFurniture()
    {
        isOpenCanvasFurniture = false;
        if (cameraController)
        {
            print("CloseCanvasFurniture");
            cameraController.SetWasZoom(false);

            my_mainMenuManager.IsOpenCanvasRoom(true);
            
        }
    }

    public bool IsOpenningCanvasFurniture()
    {
        return isOpenCanvasFurniture;
    }

    public void AddCanvasStatus(GameObject gameObj)
    {
        canvasStatus.Add(gameObj);
    }

    public void IsOpenCanvasStatusUI(bool isOpen)
    {
        for (int i = 0; i < canvasStatus.Count; i++)
        {
            canvasStatus[i].SetActive(isOpen);
        }
    }

    
}
