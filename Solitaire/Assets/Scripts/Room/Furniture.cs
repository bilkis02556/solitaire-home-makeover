﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Furniture : MonoBehaviour
{
    private RoomManager roomManager;
    [SerializeField]
    private CameraController cameraController;
    private Room thisRoom;
    [Header("Detail")]    
    public string idFurniture;
    public int levelUnlock;
    public MeshFilter body;
    public MeshRenderer meshRenderer;
    [Header("UI Status")]
    public Vector3 offsetPosition;
    private CanvasFurniture canvasFurniture;
    private CanvasStatusUI canvasStatus;
    public Transform positionZoom;
    public bool isBougth;
    private bool isBoughtAuto;
    [System.Serializable]
    public struct FurnitureMesh
    {
        public Mesh meshBody;
        public Material materials;
        public Sprite picture;
    }
    [Header("Bought")]
    public FurnitureMesh[] furnitureMeshes;
    public bool isParseIdSelected;
    public string idSelected;
    public int numberIdSelected;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        isBougth = false;
        isBoughtAuto = false;
        StartSave();
    }

    public void SetStartFurniture(Room room,CanvasFurniture canvasFurniture, CanvasStatusUI canvasStatus)
    {
        if (RoomManager.instance)
        {
            roomManager = RoomManager.instance;
        }

        if (CameraController.instance)
        {
            cameraController = CameraController.instance;
        }

        thisRoom = room;

        this.canvasFurniture = Instantiate(canvasFurniture, transform);
        this.canvasFurniture.CloseCanvas();

        this.canvasStatus = Instantiate(canvasStatus, transform);
        this.canvasStatus.transform.localPosition = offsetPosition;
        this.canvasStatus.SetText(levelUnlock);
        thisRoom.AddCanvasStatus(this.canvasStatus.gameObject);

        this.canvasFurniture.buttonBuy[0].onClick.AddListener(() => OnChangeFurniture(0));
        this.canvasFurniture.buttonBuy[1].onClick.AddListener(() => OnChangeFurniture(1));
        this.canvasFurniture.buttonBuy[2].onClick.AddListener(() => OnChangeFurniture(2));

        for (int i = 0; i < 3; i++)
        {
            this.canvasFurniture.SetImage(i, furnitureMeshes[i].picture);
        }
    }

    public void UpdateStatusCanvasUI()
    {
        if (!roomManager)
        {
            return;
        }

        if (isBougth)
        {
            this.canvasStatus.SetStatusUI(statusBuyUI.Bought);
        }
        else
        {
            if (roomManager.CanBoughtByNumberLevelGot(levelUnlock))
            {
                this.canvasStatus.SetStatusUI(statusBuyUI.canBuy);
            }
            else
            {
                this.canvasStatus.SetStatusUI(statusBuyUI.cannotBuy);
            }
        }
        
    }

    public virtual void OnMouseDown()
    {
        if(canvasFurniture)
        {
            if(!roomManager)
            {
                return;
            }

            if (roomManager.CanBoughtByNumberLevelGot(levelUnlock) || isBougth)
            {
                if (cameraController)
                {
                    print("[Click] set zoom");
                    cameraController.SetZoomTransform(positionZoom);
                }
                if (!canvasFurniture.IsOpening() && !thisRoom.IsOpenningCanvasFurniture())
                {
                    print(idFurniture);
                    canvasFurniture.OpenCanvas(thisRoom);
                }
                if(!isBougth)
                {
                    roomManager.WasBought(levelUnlock);
                    UpdateSave();
                    isBougth = true;
                }
            }
            else
            {
                print("You dont have enought level unlock");
            }
        }
    }

    public void BoughtAuto()
    {
        if(isBougth)
        {
            return;
        }

        if(!isBoughtAuto)
        {
            if (canvasFurniture)
            {
                if (!roomManager)
                {
                    return;
                }


                if (roomManager.CanBoughtByNumberLevelGot(levelUnlock))
                {
                    if (cameraController)
                    {
                        print("[AUTO] set zoom");
                        cameraController.SetZoomTransform(positionZoom);
                    }
                    if (!canvasFurniture.IsOpening() && !thisRoom.IsOpenningCanvasFurniture())
                    {
                        print(idFurniture);
                        canvasFurniture.OpenCanvas(thisRoom);
                    }
                    if (!isBougth)
                    {
                        roomManager.WasBought(levelUnlock);
                        UpdateSave();
                        isBougth = true;
                    }
                }
            }
            isBoughtAuto = true;
        }        
    }

    public void OnChangeFurniture(int index)
    {
        body.mesh = furnitureMeshes[index].meshBody;
        meshRenderer.material = furnitureMeshes[index].materials;
        idSelected = index.ToString();
        UpdateSave();
    }

    public void StartSave()
    {
        if(PlayerPrefs.GetString(idFurniture) == "Bought")
        {
            isBougth = true;
            isBoughtAuto = true;

            idSelected = PlayerPrefs.GetString("idSelected_" + idFurniture);

            print("id selected : " + idSelected);
            isParseIdSelected = int.TryParse(idSelected, out numberIdSelected);

            if(isParseIdSelected)
            {
                body.mesh = furnitureMeshes[int.Parse(idSelected)].meshBody;
                meshRenderer.material = furnitureMeshes[int.Parse(idSelected)].materials;
            }
        }
        else
        {
            PlayerPrefs.SetString(idFurniture, "NotBought");
            PlayerPrefs.SetString("idSelected_" + idFurniture, "0");
        }
    }

    public void UpdateSave()
    {
        if(PlayerPrefs.GetString(idFurniture) == "NotBought")
        {
            PlayerPrefs.SetString(idFurniture, "Bought");
        }
        PlayerPrefs.SetString("idSelected_" + idFurniture, idSelected);
    }

}
