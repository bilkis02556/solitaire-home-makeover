﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckCustom : MonoBehaviour
{
    public int countCardAll; // countCardAll = deck_custom_card_list.Length
    public int countCardBonus;
    public int countCardPlay;
    public int countCardSell;

    [System.Serializable]
    public struct custom_card
    {
        public bool isManual;
        public deck.card_class card;
    }
    public List<custom_card> deck_custom_card_list = new List<custom_card>();
}
