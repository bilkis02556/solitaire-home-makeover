﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardEndGame : MonoBehaviour
{
    private GameManager my_gameManager;
    private deck thisDeck;
    public GameObject popupQuestion_gotCard;
    public GameObject popupQuestion_endGame;
    public TextMeshProUGUI numberCardGot;
    public int coinSpend;
    public int cardGot;
    public bool canClick;
    [SerializeField]
    private Animator anim;

    public void SetCardEndGame(deck deck)
    {
        if (GameManager.instance)
        {
            my_gameManager = GameManager.instance;
        }
        thisDeck = deck;
        numberCardGot.text = "+" + cardGot;
        CloseCanvasPopup();
        anim.SetTrigger("Open");
        canClick = false;
    }
    private void OnMouseDown()
    {
        if (canClick)
        {
            popupQuestion_gotCard.SetActive(true);
            print("Open Popup Question sold new card!");
        }
    }

    public void CanClick()
    {
        canClick = true;
    }

    public void CannotClick()
    {
        canClick = false;
    }

    public void CloseCanvasPopup()
    {
        popupQuestion_gotCard.SetActive(false);
        popupQuestion_endGame.SetActive(false);
    }

    public void AcceptCanvasPopupGotCard()
    {
        if (GameMaster.my_GameMaster)
        {
            if (GameMaster.my_GameMaster.number_coin_got >= coinSpend)
            {
                GameMaster.my_GameMaster.number_coin_got -= coinSpend;

                thisDeck.Use_Bonus_Add_Card_In_Deck(cardGot);
                thisDeck.SetCardDeckFollowCustomDeckList(true, cardGot);
                anim.SetTrigger("Close");

                thisDeck.canOpenEndCard = true;
                thisDeck.isOpenEndCard = false;
            }
            else
            {
                GameMaster.my_GameMaster.Start_Show_Debug_Text("You dont have enought coin!", 2);
            }
        }
    }

    public void ResetEndCard(deck deck)
    {
        deck.canOpenEndCard = true;
        deck.isOpenEndCard = false;
        anim.SetTrigger("Close");
    }

    public void EndGame()
    {
        popupQuestion_endGame.SetActive(true);
    }

    public void AcceptCanvasPopupEndGame()
    {
        if(my_gameManager)
        {
            my_gameManager.EndGame();
        }
        else
        {
            Debug.LogWarning("You dont have GameManager in this scene");
        }
    }
}
