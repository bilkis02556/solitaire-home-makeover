﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellWildCard : MonoBehaviour
{
    private deck thisDeck;
    private GameMaster my_gameMaster;
    public int coinSpend;
    public GameObject canvasPopupQuestion;
    public bool canClick;
    public BoxCollider2D collider;
    public SpriteRenderer renderer;

    public void SetSellWildCard(deck deck)
    {
        renderer.enabled = true;
        collider.enabled = true;
        thisDeck = deck;
        thisDeck.IsChangeCurrentCardToWild(false);
        if (GameMaster.my_GameMaster)
        {
            my_gameMaster = GameMaster.my_GameMaster;
        }
    }
    //public void 
    private void OnMouseDown()
    {
        print("Open Popup Question!");
        OpenPopupQuestion();
    }

    public void SetCanClick()
    {
        canClick = true;
    }

    public void SetCannotClick()
    {
        canClick = false;
    }

    public void AcceptBought()
    {
        if(my_gameMaster)
        {
            if(my_gameMaster.number_coin_got >= coinSpend)
            {
                my_gameMaster.number_coin_got -= coinSpend;
                thisDeck.IsChangeCurrentCardToWild(true);
                ClosePopupQuestion();
            }
            else
            {
                my_gameMaster.Start_Show_Debug_Text("You dont have enought coin!", 3);
            }
        }
    }

    public void OpenPopupQuestion()
    {
        canvasPopupQuestion.SetActive(true);
    }
    public void ClosePopupQuestion()
    {
        canvasPopupQuestion.SetActive(false);
    }
}
