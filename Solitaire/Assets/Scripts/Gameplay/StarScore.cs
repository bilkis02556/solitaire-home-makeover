﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarScore : MonoBehaviour
{
    public deck this_deck;
    public Slider target_score_slider;

    private int current_score;

    public Sprite star_on;
    public Sprite star_off;

    public RectTransform[] star_rectTransform;
    private int[] star_score_steps;
    // Start is called before the first frame update
    public void SetupStarScore(deck _deck,int _win_target_score,int _current_score,int[] _star_score_steps)
    {
        this_deck = _deck;
        target_score_slider.gameObject.SetActive(true);
        //print("_win_target_score : "+ _win_target_score);
        target_score_slider.maxValue = _win_target_score;
        target_score_slider.value = 0;

        ControlPositionStar();
        this.star_score_steps = _star_score_steps;
    }

    public void ControlPositionStar()
    {
        for (int i = 0; i < star_rectTransform.Length; i++)
        {
            //star_rectTransform[i].position = new Vector3(star_score_steps[i]/ target_score_slider.maxValue, 0, 0);
        }
    }

    public void UpdateValueScore(int _current_score)
    {
        //print("UpdateValueScore");
        this.current_score = _current_score;
        target_score_slider.value = current_score;

        for (int i = 0; i < star_score_steps.Length; i++)
        {
            if(current_score > star_score_steps[i])
            {
                Image image_star = star_rectTransform[i].transform.GetComponent<Image>();
                image_star.sprite = star_on;
            }
        }
    }

    public void ResetScoreStarBar()
    {
        for (int i = 0; i < star_score_steps.Length; i++)
        {
            Image image_star = star_rectTransform[i].transform.GetComponent<Image>();
            image_star.sprite = star_off;
        }
    }
}
