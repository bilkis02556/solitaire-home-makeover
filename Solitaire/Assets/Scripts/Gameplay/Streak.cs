﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Streak : MonoBehaviour
{
    public deck this_deck;
    public int count_of_streak_bar;
    public deck.Streak_class[] streaks;
    public Image[] point_streak;
    public List<Image> point_streak_list;

    public Sprite point_sprite_empty;
    public Sprite[] point_sprite_got_streak;

    private int current_streak_using;

    public bool is_completed_streak;//0:โพธิ์ดำ(clubs) 1:ดอกจิก(spades) 2:ข้าวหลามตัด(diamonds) 3:หัวใจ(hearts)
    // Start is called before the first frame update

    public void Set_Start_Streak(deck deck,int count_of_streak_bar, deck.Streak_class[] streaks,int current_streak_bar_using)
    {
        this_deck = deck;
        //print("count_of_streak_bar : " + count_of_streak_bar);
        this.count_of_streak_bar = count_of_streak_bar;
        this.streaks = new deck.Streak_class[count_of_streak_bar];
        for (int i = 0; i < count_of_streak_bar; i++)
        {
            this.streaks[i] = streaks[i];
        }
        is_completed_streak = false;

        Reset_All_Point_Streak();
    }

    public void Reset_All_Point_Streak()
    {
        for (int i = 0; i < point_streak.Length; i++)
        {
            point_streak[i].gameObject.SetActive(false);
        }
    }

    public void Start_Show_Streak(int current_streak_bar_using)
    {
        if(current_streak_bar_using > count_of_streak_bar)
        {
            current_streak_using = current_streak_bar_using;
            this_deck.use_streak = false;
            return;
        }

        for (int i = 0; i < streaks[current_streak_bar_using].max_count_streak_want; i++)
        {
            if(i< streaks[current_streak_bar_using].max_count_streak_want)
            {
                point_streak[i].gameObject.SetActive(true);
                point_streak[i].sprite = point_sprite_empty;
            }
        }
        for (int i = streaks[current_streak_bar_using].max_count_streak_want - 1; i >= 0; i--)
        {
            point_streak_list.Add(point_streak[i]);
        }
    }

    public void Update_point_streak(int current_streak_bar_using,int index_streak, int suit)
    {
        if (current_streak_bar_using > count_of_streak_bar)
        {
            current_streak_using = current_streak_bar_using;
            this_deck.use_streak = false;
            return;
        }

        point_streak_list[index_streak].sprite = point_sprite_got_streak[suit];

        if(index_streak+1 == streaks[current_streak_bar_using].max_count_streak_want)
        {
            //Complete Streak
            Completed_streak();
        }
    }

    public void Clear_streak(int number_of_streak_want_clear)
    {
        //print("number_of_streak_want_clear : " + number_of_streak_want_clear);
        for (int i = 0; i < number_of_streak_want_clear; i++)
        {
            point_streak_list[i].sprite = point_sprite_empty;
        }
    }

    public void Completed_streak()
    {
        //print("Completed_streak");
        if (current_streak_using > count_of_streak_bar)
        {
            this_deck.use_streak = false;
            return;
        }
        else
        {
            if(GameMaster.my_GameMaster)
            {
                GameMaster.my_GameMaster.number_coin_got += streaks[this_deck.current_streak_bar_using].coin_you_can_got;
                print("[Streak : ]" + GameMaster.my_GameMaster.number_coin_got);
                GameMaster.my_GameMaster.Start_Show_Debug_Text("You completed streak " + (this_deck.current_streak_bar_using) + " [" + streaks[this_deck.current_streak_bar_using].coin_you_can_got.ToString() + "]", 4);
            }

            count_of_streak_bar--;
            this_deck.count_of_streak_bar = count_of_streak_bar;
            this_deck.current_streak_bar_using++;
            this_deck.count_streak = 0;
            point_streak_list.Clear();
            is_completed_streak = true;
        }
    }

}
