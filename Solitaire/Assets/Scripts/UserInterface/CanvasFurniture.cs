﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasFurniture : MonoBehaviour
{
    private Room thisRoom;
    public Canvas thisCanvas;
    public Button[] buttonBuy;
    private bool isOpen;

    public Button buttonCloseCanvasFurniture;

    private Transform zoomTransform;

    private void Start()
    {
        thisCanvas = GetComponent<Canvas>();        
    }

    public void OpenCanvas(Room room)
    {
        thisRoom = room;
        thisCanvas.enabled = true;
        buttonCloseCanvasFurniture.interactable = true;
        isOpen = true;
        if (thisRoom)
        {
            thisRoom.OpenCanvasFurniture();
        }
    }

    public void CloseCanvas()
    {
        buttonCloseCanvasFurniture.interactable = false;
        thisCanvas.enabled = false;
        isOpen = false;
        if(thisRoom)
        {
            thisRoom.CloseCanvasFurniture();
            
        }
    }

    public bool IsOpening()
    {
        return isOpen;
    }

    public void SetImage(int i,Sprite image)
    {
        buttonBuy[i].image.sprite = image;
    }

}
