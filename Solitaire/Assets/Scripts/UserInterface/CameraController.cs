﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    public RoomManager roomManager;
    public Camera camera;
    public bool wasZoom;
    public bool stayOnOriginPosition;
    public Transform positionOrigin;
    public Transform positionOriginStatic;
    private Transform zoomTransform = null;
    public float speedMoveTo;
    public float speedRotate;

    private bool isBackToOriginPos = false;
    private Transform curentTransform;
    // Start is called before the first frame update
    void Start()
    {
        if(instance = null)
        {
            instance = this;
        }

        if(RoomManager.instance)
        {
            roomManager = RoomManager.instance;
        }

        camera = Camera.main;

        wasZoom = false;

        positionOrigin = camera.transform;
        zoomTransform = positionOrigin;
        stayOnOriginPosition = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (wasZoom)
        {
            ZoomIn();
        }
        else
        {
            ZoomOut();
        }      
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PositionCameraOrigin")
        {
            stayOnOriginPosition = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PositionCameraOrigin")
        {
            stayOnOriginPosition = false;
        }
    }

    public bool StayOnOriginPosition()
    {
        return stayOnOriginPosition;
    }

    public void ZoomIn()
    {
        print("Zoom in! :" + zoomTransform);
        camera.transform.position = Vector3.Lerp(positionOrigin.position, zoomTransform.position, speedMoveTo * Time.deltaTime);
        camera.transform.localRotation = Quaternion.Lerp(positionOrigin.localRotation, zoomTransform.localRotation, speedRotate * Time.deltaTime);
    }

    public void ZoomOut()
    {
        if (!isBackToOriginPos)
        {
            curentTransform = camera.transform;
            zoomTransform = positionOriginStatic;
            isBackToOriginPos = true;
        }
        print("Zoom out! :"+zoomTransform);
        camera.transform.position = Vector3.Lerp(curentTransform.position, positionOriginStatic.position, speedMoveTo * Time.deltaTime);
        camera.transform.localRotation = Quaternion.Lerp(curentTransform.localRotation, positionOriginStatic.localRotation, speedRotate * Time.deltaTime);
    }
    
    public bool WasZoom()
    {
        return wasZoom;
    }

    public void SetWasZoom(bool isZoom)
    {
        wasZoom = isZoom;
    }

    public void SetZoomTransform(Transform zoomTransform)
    {
        if (zoomTransform != null)
        {
            this.zoomTransform = zoomTransform;
        }
    }
}
