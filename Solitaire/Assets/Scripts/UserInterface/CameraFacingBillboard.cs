﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacingBillboard : MonoBehaviour
{
	public enum typeFacing
	{
		Ui, Object
	};
	public typeFacing typeFacingStage;
	public Camera m_Camera;
	private Quaternion cameraRot;
	//	Canvas canvas;
	// Use this for initialization
	void Awake()
	{
		m_Camera = Camera.main;
		switch (typeFacingStage)
		{
			case typeFacing.Ui:
				gameObject.GetComponent<Canvas>().worldCamera = m_Camera;
				break;

		}
	}

	// Update is called once per frame
	void Update()
	{
		if (m_Camera == null)
		{
			m_Camera = Camera.main;
		}

		if (m_Camera != null)
		{
			cameraRot = m_Camera.transform.rotation;
			transform.LookAt(transform.position + cameraRot * Vector3.forward, cameraRot * Vector3.up);
		}
	}
}
