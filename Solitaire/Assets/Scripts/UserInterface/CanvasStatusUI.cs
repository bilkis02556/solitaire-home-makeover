﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum statusBuyUI { cannotBuy, canBuy, Bought }
public class CanvasStatusUI : MonoBehaviour
{
    public Image statusUI;
    public statusBuyUI status;

    public TextMeshProUGUI textLevelUnlock;
    public Sprite statusUICanBuy;
    public Sprite statusUICannotBuy;

    public void SetText(int levelUnlock)
    {
        textLevelUnlock.text = levelUnlock.ToString();
    }

    public void SetStatusUI(statusBuyUI status)
    {
        if(status == statusBuyUI.Bought)
        {
            statusUI.gameObject.SetActive(false);
        }
        else if(status == statusBuyUI.canBuy)
        {
            statusUI.sprite = statusUICanBuy;
        }
        else if(status == statusBuyUI.cannotBuy)
        {
            statusUI.sprite = statusUICannotBuy;
        }
    }
}
