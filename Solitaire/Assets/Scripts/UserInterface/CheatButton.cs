﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatButton : MonoBehaviour
{
    public PickyBankBox bankBox;
    public void AddCoin()
    {
        if(GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.number_coin_got += 100;
        }
    }

    public void AddLevel()
    {
        if (GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.number_level_got += 1;
        }
    }

    int i = 3;
    public void AddStar()
    {
        i += 3; 
        bankBox.UpdatePickyBankBox(i);
    }
}
