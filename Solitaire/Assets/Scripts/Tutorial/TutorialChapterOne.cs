﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialChapterOne : TutorialBase
{
    public override void StartTutorial()
    {
        print("TutorialChapterOne");
        //targetCardOnDeck.position = new Vector3(0, targetCardOnDeck.position.y, targetCardOnDeck.position.z);
        for (int i = 0; i < uiTutorial.Length; i++)
        {
            uiTutorial[i].SetActive(false);
        }
    }
    public override void UpdateTutorial()
    {
        base.UpdateTutorial();
        endCardOnCeck.enabled = false;
        spriteCardThisDeck.enabled = false;
    }
}
