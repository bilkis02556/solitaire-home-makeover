﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialBase : MonoBehaviour
{
    [Header("Base Setting")]
    public Image endCardOnCeck;
    public SpriteRenderer spriteCardThisDeck;
    public Transform targetCardOnDeck;
    public Animator[] animationTutorial;
    public GameObject[] uiTutorial;
    public virtual void StartTutorial()
    {
        
    }

    public virtual void UpdateTutorial()
    {
        
    }
}
