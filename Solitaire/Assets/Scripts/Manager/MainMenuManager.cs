﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager instance;
    public CameraController cameraController;
    public RoomManager my_roomManager;
    public GameMaster my_gameMaster;
    public int numberLevelGot;
    public PickyBankBox pickyBankBox;
    [Header("Canvas Room")]
    public GameObject canvasRoom;

    [Header("Dummy")]
    public Transform[] zoomTransform;
    // Start is called before the first frame update
    public void Start()
    {
        if(instance == null)
        {
            instance = this;
        }

        if (GameMaster.my_GameMaster)
        {
            my_gameMaster = GameMaster.my_GameMaster;
        }
        

        if(RoomManager.instance)
        {
            my_roomManager = RoomManager.instance;
        }

        pickyBankBox.SetPickBankBox(3);
    }

    private void Update()
    {
        if(my_gameMaster)
        {
            if(my_gameMaster.canUpdateMainMenu)
            {
                print("UpdateDataMainMenuManager");
                UpdateDataMainMenuManager();
                my_gameMaster.canUpdateMainMenu = false;
            }
        }

        if(my_roomManager)
        {
            my_roomManager.UpdateRoomManager();
        }    
    }
    public void UpdateDataMainMenuManager()
    {
        pickyBankBox.UpdatePickyBankBox(my_gameMaster.GetTotalStarAll());
    }

    public void IsOpenCanvasRoom(bool isOpen)
    {
        canvasRoom.SetActive(isOpen);
    }


}
