﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    public static RoomManager instance;
    public CameraController cameraController;
    public GameMaster my_gameMaster;
    public CanvasFurniture canvasFurniturePrefab;
    public CanvasStatusUI canvasUIStatusPrefabs;
    //public Button buttonCloseCanvasFurniture;
    public Room[] rooms;

    private Transform zoomTransform;

    private bool canBought;
    private bool wasBought;

    // Start is called before the first frame update
    public void Start()
    {
        if(!GameMaster.my_GameMaster)
        {
            return;
        }

        if(CameraController.instance)
        {
            cameraController = CameraController.instance;
        }

        my_gameMaster = GameMaster.my_GameMaster;

        if(instance == null)
        {
            instance = this;
        }

        for (int i = 0; i < rooms.Length; i++)
        {
            if(i == my_gameMaster.currentRoom)
            {
                rooms[i].gameObject.SetActive(true);
                rooms[i].StartRoom();
            }
            else
            {
                rooms[i].gameObject.SetActive(false);
            }
        }
    }

    public void UpdateRoomManager()
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            if (i == my_gameMaster.currentRoom)
            {
                rooms[i].UpdateRoom();
            }
        }
    }

    public void BoughtFurniture()
    {

    }
    public bool CanBoughtByNumberLevelGot(int levelUnlocked)
    {
        return my_gameMaster.number_level_got >= levelUnlocked;
    }

    public void WasBought(int numberUnlock)
    {
        my_gameMaster.number_level_got-= numberUnlock;
    }

    //public void CloseCanvasFurniture()
    //{
    //    buttonCloseCanvasFurniture.enabled = false;
    //}



}
