﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public TutorialBase tutorial;
    public int current_Level;
    public GameObject[] level_Solitaire;
    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }


        for (int i = 0; i < level_Solitaire.Length; i++)
        {
            if(i==GameMaster.my_GameMaster.number_stage_unlocked)
            {
                level_Solitaire[i].SetActive(true);
                tutorial = level_Solitaire[i].GetComponent<TutorialBase>();
                if (tutorial)
                {
                    tutorial.StartTutorial();
                }
            }
            else
            {
                level_Solitaire[i].SetActive(false);
            }
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorial)
        {
            tutorial.UpdateTutorial();
        }
    }

    public void EndGame()
    {
        if(GameMaster.my_GameMaster)
        {
            GameMaster.my_GameMaster.Show_this_screen(GameMaster.current_screen.room);
        }
    }
}
