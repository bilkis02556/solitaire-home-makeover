﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private GameMaster master;
    public AudioSource audioSource;
    public AudioClip audioStartScene;
    public AudioClip audioRoomScene;
    public AudioClip audioGameplayScene;
    // Start is called before the first frame update
    void Start()
    {
        if(GameMaster.my_GameMaster)
        {
            master = GameMaster.my_GameMaster;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch(master.current_screen_selected)
        {
            case GameMaster.current_screen.start:
                audioSource.clip = audioStartScene;
                if (!audioSource.isPlaying)
                {
                    audioSource.Play();
                    audioSource.loop = true;
                }
                break;
            case GameMaster.current_screen.room:
                audioSource.clip = audioRoomScene;
                if(!audioSource.isPlaying)
                {
                    audioSource.Play();
                    audioSource.loop = true;
                }
                break;
            case GameMaster.current_screen.game:
                audioSource.clip = audioGameplayScene;
                if (!audioSource.isPlaying)
                {
                    audioSource.Play();
                    audioSource.loop = true;
                }
                break;
        }
    }
}
