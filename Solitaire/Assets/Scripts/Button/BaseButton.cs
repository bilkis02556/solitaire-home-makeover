﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class BaseButton : MonoBehaviour
{
    public Button thisButton;
    // Start is called before the first frame update
    void Start()
    {
        //thisButton = GetComponent<Button>();
        //thisButton.onClick.AddListener(() => TaskOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void TaskOnClick()
    {

    }
}
