﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[CreateAssetMenu(fileName = "new skin deck", menuName ="Solitaire skin deck")]
public class DeckSprites : ScriptableObject {

    public string name;

    public Sprite card_back_sprite;
    public Sprite gold_card_back_sprite;

    //black
    public Sprite[] clubs = new Sprite[13];
    public Sprite[] spades = new Sprite[13];

    //red
    public Sprite[] diamonds = new Sprite[13];
    public Sprite[] hearts = new Sprite[13];

    //gold version:
    //black
    public Sprite[] gold_clubs = new Sprite[13];
    public Sprite[] gold_spades = new Sprite[13];

    //red
    public Sprite[] gold_diamonds = new Sprite[13];
    public Sprite[] gold_hearts = new Sprite[13];

    //special cards
    public wild_card_deck my_wild_card_deck;
    public Sprite[] wild = new Sprite[13];

    public Sprite normal_joker;
    public Sprite red_joker;
    public Sprite black_joker;
    public Sprite gold_normal_joker;
    public Sprite gold_red_joker;
    public Sprite gold_black_joker;
    public Sprite wild_joker;

    public Sprite padlock_sprite;
    public Sprite royal_padlock_sprite;
    public Sprite keyhole_padlock_sprite;
    public Sprite padlock_card;

    public Sprite[] bonus_cards_sprites = new Sprite[4];

    public bool editor_show_sprites_normal;
    public bool editor_show_sprites_normal_clubs;
    public bool editor_show_sprites_normal_spades;
    public bool editor_show_sprites_normal_diamonds;
    public bool editor_show_sprites_normal_hearts;
    public bool editor_show_sprites_gold;
    public bool editor_show_sprites_gold_clubs;
    public bool editor_show_sprites_gold_spades;
    public bool editor_show_sprites_gold_diamonds;
    public bool editor_show_sprites_gold_hearts;
    public bool editor_show_sprites_wild;
    public bool editor_show_sprites_bonus_cards;
    public bool editor_show_sprites_padlock;

}
