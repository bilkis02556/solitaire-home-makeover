﻿using UnityEngine;
using System.Collections;
using System;

public class GameMaster : MonoBehaviour {

    public static GameMaster my_GameMaster;

    public enum current_screen
    {
        logo = 0,
        home = 1,
        stage = 2,
        loading = 3,
        game = 4, 
        pause = 5,
        end = 6,
        skin = 7
    }
    [HideInInspector]
    public current_screen current_screen_selected = current_screen.logo;

    public float show_logo_for_n_seconds;

    [Tooltip("This must be TRUE if you use multiple boards in same stage AND win condition must be reach indipendently. Otherwise must be FALSE")]
    public bool create_a_stage_icon_for_each_board;
    int total_boards;
    [Tooltip("This must match the number of board setup in the deck script of each stage")]
    public int[] boards_in_each_stage;
            int max_board;//how many board have the most  big stage
            [HideInInspector]
            public int max_stages;// = boards_in_each_stage.Length
            [HideInInspector]
            public int total; //how many stage icons
            [HideInInspector]
            public stageButton[] stageButton_array;

    [Tooltip("This must match the max wild cards value in deck script")]
    public int max_number_of_wildcards;
    [Tooltip("Default 4. Change it ONLY if you add your own bonus to deck script and to bonus_button script")]
    public int max_number_of_bonuses = 4;

 
    //save
    bool exist_a_save_state;
    [HideInInspector]
    public bool audio_on;
    bool[] padlock_open;//to know is a stage is playable
    bool[] stage_solved;//all board in this stage solved
    [HideInInspector]
    public int current_stage;
    [HideInInspector]
    public int current_icon;//create_a_stage_icon_for_each_board is true and stages have more than a board, current_icon != current_stage;
    //stage infos
    [HideInInspector]
        public int[] current_board_in_this_stage;//[stage]how many board you have solved in this stage
        [HideInInspector]
        public int[] number_of_win_conditons;//how many win condition have this stage
        [HideInInspector]
        public bool[,] win_conditions_satisfied;//[stage,win condition]
        [HideInInspector]
        public int[] total_star_score;
        [HideInInspector]
        public int[] current_stage_total_score;
        [HideInInspector]
        public int[] collect_all_cards_in_n_boards_current_count;
        [HideInInspector]
        public int[] collect_all_gold_cards_in_n_boards_current_count;
        [HideInInspector]
        public int[] best_combo;
        [HideInInspector]
        public int[] combo_count;
        [HideInInspector]
        public int[] total_combo_sum;
        [HideInInspector]
        public float[] score_multiplier;
        [HideInInspector]
        public int[] total_gold_cards_taken_in_all_boards;

        public enum save_type
            {
            no,
            different_saves_for_each_stage, //this is good with stages with win_condition_must_be_reach.multiboard and as_sum_of_all_boards_into_array
            only_one_save_to_keep_among_stages
            }
         public save_type save_bonus_selected;
            [HideInInspector]
            public int[,] bonus_quantity;//[stage,bonus_type]
                [HideInInspector]
                public int[] general_bonus;//[bonus_type]
            [HideInInspector]
            public int[,] wild_cards;//[stage,wild_card_type]
            public save_type save_wild_cards_selected;
            [HideInInspector]
                public int[] general_wild_card;//[wild_card_type]
                [HideInInspector]
                public int general_wild_cards_quantity;
    //for each board 
    [HideInInspector]
    public int[,] star_score;//[stage,board]
        [HideInInspector]
        public bool[,] all_cards_collected_in_this_board;//[stage,board]


    //audio
    [HideInInspector]
    public AudioSource sfx_source;
        public AudioClip tap_sfx;
        public AudioClip tap_error_sfx;
    [HideInInspector]
    public AudioSource music_source;
        public AudioClip music;
    //gui
    [HideInInspector]
    public GameObject[] screens_array;
    [HideInInspector]
    public GameObject stageIcoObj;
    [HideInInspector]
    public Transform stage_container;

    [HideInInspector]
    public SkinManager skinManager;

    void Awake()
    {
        Singleton();

        max_stages = boards_in_each_stage.Length;
        stage_solved = new bool[max_stages];

        skinManager = GetComponent<SkinManager>();
        if (skinManager)
            skinManager.AwakeMe();

        //find max boards in one stage
        for (int i = 0; i < max_stages; i++)
        {
            if (max_board < boards_in_each_stage[i])
                max_board = boards_in_each_stage[i];
        }
        //Debug.Log("max_stages: " + max_stages + " ...max_board: " + max_board);
        star_score = new int[max_stages, max_board];
        all_cards_collected_in_this_board = new bool[max_stages, max_board];

        //find the total of board all board in all stages
        total_boards = 0;
        for (int i = 0; i < max_stages; i++)
            total_boards += boards_in_each_stage[i];



        //initiate arrays
        if (create_a_stage_icon_for_each_board)
            total = total_boards;
        else
            total = max_stages;
        
        padlock_open = new bool[total];
        current_board_in_this_stage = new int[total];
        number_of_win_conditons = new int[total];
        current_stage_total_score = new int[total];
        total_star_score = new int[total];
        best_combo = new int[total];
        combo_count = new int[total];
        total_combo_sum = new int[total];
        score_multiplier = new float[total];
        collect_all_cards_in_n_boards_current_count = new int[total];
        collect_all_gold_cards_in_n_boards_current_count = new int[total];
        total_gold_cards_taken_in_all_boards = new int[total];
        win_conditions_satisfied = new bool[total, 10];

        if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.different_saves_for_each_stage)
            bonus_quantity = new int[total, max_number_of_bonuses];
        else if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.only_one_save_to_keep_among_stages)
            general_bonus = new int[max_number_of_bonuses];

        if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.different_saves_for_each_stage)
            wild_cards = new int[total, max_number_of_wildcards];
        else if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.only_one_save_to_keep_among_stages)
            general_wild_card = new int[max_number_of_wildcards];


        //check if load the save data or start from zero
        exist_a_save_state = Convert.ToBoolean(PlayerPrefs.GetInt("savestate"));
        if (exist_a_save_state)//copy the saves in the arrays
            {
            Load();

            }
        else //no save data, so start the game from zero
            {
            audio_on = true;
            }


        Generate_stage_screen();
        if (exist_a_save_state)
            {
            //unlock new board if previous board is already solved
            for (int i = 1; i < total; i++)
                {
                if (stage_solved[i - 1] && !padlock_open[i])
                    Unlock_this_stage_icon(i);
                }
            }

            for (int i = 0; i < screens_array.Length; i++)
            screens_array[i].SetActive(false);
    }

    void Singleton()
    {
        if (my_GameMaster == null)
        {
            my_GameMaster = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {

        StartCoroutine(Start_me());

    }

    IEnumerator Start_me()
    {
        if (show_logo_for_n_seconds > 0)
            {
            Show_this_screen(current_screen.logo);
            yield return new WaitForSeconds(show_logo_for_n_seconds);
            Show_this_screen(current_screen.home);
            Play_music();
            }
        else
            {
            Show_this_screen(current_screen.home);
            Play_music();
            }
    }


    public void Quit()
    {
        Debug.Log("Quit");
        Gui_sfx(tap_sfx);
        Application.Quit();
    }


    public void Unlock_this_stage_icon(int this_icon)
    {
        padlock_open[this_icon] = true;
        stageButton_array[this_icon].Update_padlock();
        PlayerPrefs.SetInt("padlock_" + this_icon.ToString(), Convert.ToInt32(padlock_open[this_icon]));

        if (this_icon > 0)
            Stage_solved(this_icon-1);
    }

    public void Stage_solved(int this_icon)
    {
        Debug.Log("Stage_solved: " + this_icon);
     stage_solved[this_icon] = true;
     PlayerPrefs.SetInt("stage_solved" + (this_icon).ToString(), Convert.ToInt32(stage_solved[this_icon]));     
    }

    public void Open_pause()//show pause screen
    {
        Gui_sfx(tap_sfx);
        Show_this_screen(current_screen.pause);
        Time.timeScale = 0;

        if (deck.this_deck)
            {
            deck.this_deck.player_can_move = false;
            }
    }

    public void Close_pause()//close pause screen
    {
        Time.timeScale = 1;

        if (deck.this_deck && current_screen_selected == current_screen.game)
        {
            deck.this_deck.player_can_move = true;
        }
    }

    public void Retry()//restart the current board
    {
        if (deck.this_deck)
            deck.this_deck.Retry();
    }

    public void Show_this_screen(int this_screen)
    {
        Gui_sfx(tap_sfx);
        Show_this_screen((current_screen)this_screen);
    }

    public void Show_this_screen(current_screen this_screen)
    {
        screens_array[(int)current_screen_selected].SetActive(false);
        current_screen_selected = this_screen;
        screens_array[(int)current_screen_selected].SetActive(true);
    }

    void Generate_stage_screen()
    {
        Debug.Log("Generate_stage_screen()");
        if (create_a_stage_icon_for_each_board)
            {
            stageButton_array = new stageButton[total_boards];
            int board_count = 0;
            int stage_count = 0;
            for (int i = 0; i < total_boards; i++)
            {
                GameObject temp = Instantiate(stageIcoObj);
                temp.transform.SetParent(stage_container, false);
                stageButton_array[i] = temp.GetComponent<stageButton>();
                stageButton_array[i].Update_me(i, board_count, !padlock_open[i], stage_count);
                board_count++;
                if (board_count >= boards_in_each_stage[stage_count])
                    {
                    board_count = 0;
                    stage_count++;
                    }
            }
        }
        else //create a stage icon for each scene
            {
            stageButton_array = new stageButton[max_stages];

            for (int i = 0; i < max_stages; i++)
                {
                GameObject temp = Instantiate(stageIcoObj);
                temp.transform.SetParent(stage_container, false);
                stageButton_array[i] = temp.GetComponent<stageButton>();
                stageButton_array[i].Update_me(i, current_board_in_this_stage[i], !padlock_open[i]);
                }

            }

        Unlock_this_stage_icon(0);//unlock first stage
        Save_everything();
    }


    #region Save and load system:
    void Load()
    {
        //options
        audio_on = Convert.ToBoolean(PlayerPrefs.GetInt("audio_on"));

        //general
        current_stage = PlayerPrefs.GetInt("current_stage");

        //stage icons
        for (int i = 0; i < total; i++)
            {
            padlock_open[i] = Convert.ToBoolean(PlayerPrefs.GetInt("padlock_" + i.ToString()));
            stage_solved[i] = Convert.ToBoolean(PlayerPrefs.GetInt("stage_solved" + i.ToString()));
            }


        //stages
        for (int i = 0; i < max_stages; i++)
            Load_this_stage(i);

       
    }

    void Load_this_stage(int this_stage)
    {
        if (this_stage >= boards_in_each_stage.Length)
            {
            Debug.LogWarning("IndexOutOfRange");
            return;
            }

        number_of_win_conditons[this_stage] = PlayerPrefs.GetInt("number_of_win_conditons_in_stage" + this_stage.ToString());
        for (int i = 0; i < number_of_win_conditons[this_stage]; i++)
            win_conditions_satisfied[this_stage,i] = Convert.ToBoolean(PlayerPrefs.GetInt("stage"+ this_stage.ToString() + "win_condition_n" + i.ToString()));

        current_board_in_this_stage[this_stage] = PlayerPrefs.GetInt("current_progress_in_stage_" + this_stage.ToString());

        best_combo[this_stage] = PlayerPrefs.GetInt("best_combo_" + this_stage.ToString());
        combo_count[this_stage] = PlayerPrefs.GetInt("combo_count_" + this_stage.ToString());
        total_combo_sum[this_stage] = PlayerPrefs.GetInt("total_combo_sum_" + this_stage.ToString());
        score_multiplier[this_stage] = PlayerPrefs.GetFloat("score_multiplier_" + this_stage.ToString());

        total_star_score[this_stage] = PlayerPrefs.GetInt("total_star_score_" + this_stage.ToString());

        current_stage_total_score[this_stage] = PlayerPrefs.GetInt("current_stage_total_score_" + this_stage.ToString());

        collect_all_cards_in_n_boards_current_count[this_stage] = PlayerPrefs.GetInt("collect_all_cards_in_n_boards_current_count_" + this_stage.ToString());

        collect_all_gold_cards_in_n_boards_current_count[this_stage] = PlayerPrefs.GetInt("collect_all_gold_cards_in_n_boards_current_count_" + this_stage.ToString());
        total_gold_cards_taken_in_all_boards[this_stage] = PlayerPrefs.GetInt("total_gold_cards_taken_in_all_boards_" + this_stage.ToString());

        for (int i = 0; i < boards_in_each_stage[this_stage]; i++)
        {
            star_score[this_stage, i] = PlayerPrefs.GetInt("star_score_stage" + this_stage.ToString() + "_board" + i.ToString());
            all_cards_collected_in_this_board[this_stage, i] = Convert.ToBoolean(PlayerPrefs.GetInt("all_cards_collected_in_this_board" + this_stage.ToString() + "_board" + i.ToString()));
        }



        if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            for (int i = 0; i< max_number_of_bonuses; i++)
                bonus_quantity[this_stage,i] = PlayerPrefs.GetInt(this_stage.ToString()+"bonus" + i.ToString());
        }

        if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            for (int i = 0; i < max_number_of_wildcards; i++)
                wild_cards[this_stage, i] = PlayerPrefs.GetInt(this_stage.ToString() + "wild_card" + i.ToString());
        }

    }

    public void Load_general_bonus()
    {
            for (int i = 0; i < max_number_of_bonuses; i++)
            {
                general_bonus[i] = PlayerPrefs.GetInt("general_bonus" + i.ToString());
            }

    }

    public void Load_general_wild_cards()
    {
        general_wild_cards_quantity = PlayerPrefs.GetInt("general_wild_cards_quantity");
        for (int i = 0; i < max_number_of_wildcards; i++)
            {
                general_wild_card[i] = PlayerPrefs.GetInt("general_wild_card" + i.ToString());
            }
    }


    public void Save_everything()
    {
        Save_options();

        //general
        PlayerPrefs.SetInt("current_stage", current_stage);

        //stage icons
        for (int i = 0; i < total; i++)
            PlayerPrefs.SetInt("padlock_" + i.ToString(), Convert.ToInt32(padlock_open[i]));

        //stages
        for (int i = 0; i < max_stages; i++)
            Save_this_stage(i);

        PlayerPrefs.SetInt("total_boards", total_boards);

        exist_a_save_state = true;
        PlayerPrefs.SetInt("savestate", Convert.ToInt32(exist_a_save_state));
    }

    public void Save_general_bonus()
    {

            for (int i = 0; i < max_number_of_bonuses; i++)
                {
                PlayerPrefs.SetInt("general_bonus" + i.ToString(), general_bonus[i]);
                }
          
    }

    public void Save_general_wild_cards()
    {

        PlayerPrefs.SetInt("general_wild_cards_quantity", general_wild_cards_quantity);
        for (int i = 0; i < max_number_of_wildcards; i++)
            {
                PlayerPrefs.SetInt("general_wild_card" + i.ToString(), general_wild_card[i]);
            }
        
    }



    void Save_options()
        {
        PlayerPrefs.SetInt("audio_on", Convert.ToInt32(audio_on));
        }

    public void Save_this_stage(int this_stage)
    {

        if (this_stage >= boards_in_each_stage.Length)
        {
            Debug.LogWarning("IndexOutOfRange");
            return;
        }


        PlayerPrefs.SetInt("number_of_win_conditons_in_stage" + this_stage.ToString(), number_of_win_conditons[this_stage]);
        for (int i = 0; i < number_of_win_conditons[this_stage]; i++)
            PlayerPrefs.SetInt("stage" + this_stage.ToString() + "win_condition_n" + i.ToString(), Convert.ToInt32(win_conditions_satisfied[this_stage, i]));

        PlayerPrefs.SetInt("current_progress_in_stage_" + this_stage.ToString(), current_board_in_this_stage[this_stage]);

        PlayerPrefs.SetInt("best_combo_" + this_stage.ToString(), best_combo[this_stage]);
        PlayerPrefs.SetInt("combo_count_" + this_stage.ToString(), combo_count[this_stage]);
        PlayerPrefs.SetInt("total_combo_sum_" + this_stage.ToString(), total_combo_sum[this_stage]);
        PlayerPrefs.SetFloat("score_multiplier_" + this_stage.ToString(), score_multiplier[this_stage]);

        PlayerPrefs.SetInt("total_star_score_" + this_stage.ToString(), total_star_score[this_stage]);

        PlayerPrefs.SetInt("current_stage_total_score_" + this_stage.ToString(), current_stage_total_score[this_stage]);

        PlayerPrefs.SetInt("collect_all_cards_in_n_boards_current_count_" + this_stage.ToString(), collect_all_cards_in_n_boards_current_count[this_stage]);


        PlayerPrefs.SetInt("collect_all_gold_cards_in_n_boards_current_count_" + this_stage.ToString(), collect_all_gold_cards_in_n_boards_current_count[this_stage]);
        PlayerPrefs.SetInt("total_gold_cards_taken_in_all_boards_" + this_stage.ToString(), total_gold_cards_taken_in_all_boards[this_stage]);


        for (int i = 0; i < boards_in_each_stage[this_stage]; i++)
            {
            PlayerPrefs.SetInt("star_score_stage"+ this_stage.ToString()+"_board"+ i.ToString(), star_score[this_stage, i]);
            PlayerPrefs.SetInt("all_cards_collected_in_this_board" + this_stage.ToString() + "_board" + i.ToString(), Convert.ToInt32(all_cards_collected_in_this_board[this_stage, i]));
            }

        if (GameMaster.my_GameMaster.save_bonus_selected == GameMaster.save_type.different_saves_for_each_stage)
            {
            for (int i = 0; i < max_number_of_bonuses; i++)
                PlayerPrefs.SetInt(this_stage.ToString() + "bonus" + i.ToString(), bonus_quantity[this_stage, i]);
            }

        if (GameMaster.my_GameMaster.save_wild_cards_selected == GameMaster.save_type.different_saves_for_each_stage)
        {
            for (int i = 0; i < max_number_of_wildcards; i++)
                PlayerPrefs.SetInt(this_stage.ToString() + "wild_card" + i.ToString(), wild_cards[this_stage, i]);
            }
    }

    

    public void Erase_this_board_save(int this_stage, int this_board)
    {
        star_score[this_stage, this_board] = 0;
        all_cards_collected_in_this_board[this_stage, this_board] = false;
    }

    public void Erase_this_stage_save(int this_stage)//need if player want to replay from start same stage
    {
        Debug.LogWarning("Erase_current_stage_save: " + this_stage);

        best_combo[this_stage] = 0;
        combo_count[this_stage] = 0;
        total_combo_sum[this_stage] = 0;
        score_multiplier[this_stage] = 0;

        total_star_score[this_stage] = 0;

        collect_all_cards_in_n_boards_current_count[this_stage] = 0;

        collect_all_gold_cards_in_n_boards_current_count[this_stage] = 0;
        total_gold_cards_taken_in_all_boards[this_stage] = 0;

        for (int i = 0; i < number_of_win_conditons[this_stage]; i++)
            win_conditions_satisfied[this_stage, i] = false;

        current_stage_total_score[this_stage] = 0;

        if (save_bonus_selected == save_type.different_saves_for_each_stage)
            {
            for (int i = 0; i < max_number_of_bonuses; i++)
                bonus_quantity[this_stage, i] = 0;
            }

        if (save_wild_cards_selected == save_type.different_saves_for_each_stage)
            {
            general_wild_cards_quantity = 0;
            for (int i = 0; i < max_number_of_wildcards; i++)
                wild_cards[this_stage, i] = -99;
            }

        if (!create_a_stage_icon_for_each_board)
            {
            current_board_in_this_stage[this_stage] = 0;
            stageButton_array[this_stage].Update_me(this_stage, current_board_in_this_stage[this_stage], false);

            for (int i = 0; i < boards_in_each_stage[this_stage]; i++)
                Erase_this_board_save(this_stage, i);
            }


        Save_this_stage(this_stage);
    }
    
    public void Erase_saves()
    {
        Debug.LogWarning("Erase_save data");
        PlayerPrefs.DeleteAll();
        exist_a_save_state = false;
    }

    #endregion

    #region audio
    public void Sfx_on_off()
    {
        audio_on = !audio_on;
        if (audio_on)
            {
            sfx_source.mute = false;
            music_source.mute = false;
            }
        else
            {
            sfx_source.mute = true;
            music_source.mute = true;
            }

        Save_options();
    }

    public void Gui_sfx(AudioClip gui_sound)
    {
        if (sfx_source)
        {
            if (audio_on && gui_sound)
            {
                if (!sfx_source.isPlaying)
                {
                    sfx_source.PlayOneShot(gui_sound);
                    sfx_source.loop = false;
                }
                else
                {
                    sfx_source.Stop();
                    sfx_source.PlayOneShot(gui_sound);
                    sfx_source.loop = false;
                }
            }
        }
    }

    void Play_music()
    {
        if (music && music_source)
        {
            music_source.clip = music;
            music_source.Play();
	        music_source.loop = true;
        }
    }
    #endregion
}
