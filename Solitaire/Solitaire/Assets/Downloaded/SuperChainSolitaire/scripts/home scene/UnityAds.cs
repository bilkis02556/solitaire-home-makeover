﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
#if UNITY_ADS
using UnityEngine.Advertisements; // only compile Ads code on supported platforms
#endif

public class UnityAds : MonoBehaviour {

    public static UnityAds my_unity_ads;

    public string rewardedVideoZone = "rewardedVideoZone";
    //public bool ads_test_mode;
    //string app_id_selected;
    //public string iOS_ads_app_id;
    //public string android_ads_app_id;

    //gui
    public GameObject myCanvas;
    public Text WindowMessage;
        public string ads_message;
        public string reward_message;
    public Text quantityText;
    public Image myIcon;

    public GameObject okButton;
    public GameObject noButton;
    public GameObject continueButton;

    //sprites
    public Sprite giftBox;
    public Sprite[] bonusSprite;
    public Sprite wildcardSprite;

    //general rules
    public int chance_to_show_me;
    public float minimum_time_interval_between_ads;
    float time_of_latest_ad_showed;
    //specific rules
    public int bonusChances;
        public int bonusNormalMinQuantity;
        public int bonusNormalMaxQuantity;
        public int bonusSuperMinQuantity;
        public int bonusSuperMaxQuantity;
        public int bonusChanceOfSuperQuantity;
        public int[] bonus_array_chances;
        int[] bonus_random_deck;
    //public int wildcardChances;
        public int wildcardNormalMinQuantity;
        public int wildcardNormalMaxQuantity;
        public int wildcardSuperMinQuantity;
        public int wildcardSuperMaxQuantity;
        public int wildcardChanceOfSuperQuantity;
        public float wildcard_joker_chances;

    bool reward_is_a_bonus;
    int idem_id;
    int quantity;

    //editor
    public bool editor_setup_ads;
    public bool editor_settings;
    public bool editor_sprites;
    public bool editor_advanced;
    public string[] percentual_weight;

    void Awake()
    {
        myCanvas.SetActive(false);
        Singleton();

        /*
        app_id_selected = "";
		#if UNITY_IPHONE
		app_id_selected = iOS_ads_app_id;
		#elif UNITY_ANDROID
		app_id_selected = android_ads_app_id;
		#endif
		if (app_id_selected == "" && show_debug_warnings)
		Debug.LogWarning("No app_id found");*/
    }

    void Singleton()
    {
        if (my_unity_ads == null)
        {
            my_unity_ads = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }


    // Use this for initialization
    void Start () {

        //generate bouns decks, so you can have different chances to pick different bonus
        int bonus_random_deck_lenght = 0;
        for (int i = 0; i < bonus_array_chances.Length; i++)
            bonus_random_deck_lenght += bonus_array_chances[i];

        bonus_random_deck = new int[bonus_random_deck_lenght];
        int current_step = 0;
        int count = 0;
        for (int i = 0; i < bonus_random_deck.Length; i++)
         {
            bonus_random_deck[i] = current_step;

            count++;
            if (count >= bonus_array_chances[current_step])
                {
                count = 0;
                current_step++;
                }

        }


    }
	

    public void Check_if_show_unityADS()
    {
        Debug.Log("Check_if_show_unityADS");

        //if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork && !Advertisement.isInitialized)
            //Advertisement.Initialize(app_id_selected, ads_test_mode);

        if ((minimum_time_interval_between_ads + time_of_latest_ad_showed) < Time.realtimeSinceStartup)
            {
            myIcon.sprite = giftBox;
            quantityText.gameObject.SetActive(false);
            continueButton.SetActive(false);
            okButton.SetActive(true);
            noButton.SetActive(true);
            WindowMessage.text = ads_message;

            if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)// && Advertisement.isInitialized)
                {
                if (UnityEngine.Random.Range(1, 100) <= chance_to_show_me)
                    {
                    myCanvas.SetActive(true);
                    time_of_latest_ad_showed = Time.realtimeSinceStartup;
                    }
                }
            }
    }

    void Decide_Reward()
    {
        quantity = 0;
        idem_id = -1;
        reward_is_a_bonus = false;

        if (UnityEngine.Random.Range(1, 100) <= bonusChances)//reward is a bonus
        {
            Debug.Log("bonus");
            reward_is_a_bonus = true;

            //how much?
            if (UnityEngine.Random.Range(1, 100) <= bonusChanceOfSuperQuantity) //super quantity
                quantity = UnityEngine.Random.Range(bonusSuperMinQuantity, bonusSuperMaxQuantity);
            else //normal quantity
                quantity = UnityEngine.Random.Range(bonusNormalMinQuantity, bonusNormalMaxQuantity);

            //what bonus?
            idem_id = bonus_random_deck[UnityEngine.Random.Range(0, bonus_array_chances.Length)];

        }
        else //reward is a wild card
        {
            Debug.Log("wild");
            if (UnityEngine.Random.Range(1, 100) <= wildcardChanceOfSuperQuantity) //super quantity
                quantity = UnityEngine.Random.Range(wildcardSuperMinQuantity, wildcardSuperMaxQuantity);
            else //normal quantity
                quantity = UnityEngine.Random.Range(wildcardNormalMinQuantity, wildcardNormalMaxQuantity);
        }
    }

    /* //remove this line
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                Decide_Reward();
                Give_Reward();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
    */ //remove this line

    void Give_Reward()
    {
        Debug.Log(idem_id + "..." + reward_is_a_bonus);
        if (deck.this_deck)
        {

            okButton.SetActive(false);
            noButton.SetActive(false);
            continueButton.SetActive(true);
            

            WindowMessage.text = reward_message;
            if (quantity > 1)
                {
                quantityText.text = quantity.ToString("N0");
                quantityText.gameObject.SetActive(true);

                WindowMessage.text += " " + quantity.ToString("N0");
                }
            else
                {
                quantityText.gameObject.SetActive(false);
                WindowMessage.text += " one ";
                }

            if (reward_is_a_bonus)
            {
                for (int i = 0; i < quantity; i++)
                    deck.this_deck.bonus_button_script[idem_id].Add_a_charge();

                myIcon.sprite = bonusSprite[idem_id];

                WindowMessage.text += " " + Enum.GetName(typeof(deck.bonus_type), idem_id).ToString();
            }
            else //is a wild card
            {
                myIcon.sprite = wildcardSprite;

                for (int i = 0; i < quantity; i++)
                    {
                    int my_rank = -99;
                    deck.card_class.joker_type my_joker = deck.card_class.joker_type.none;

                    if (UnityEngine.Random.Range(0f, 100f) <= wildcard_joker_chances)
                        my_joker = deck.card_class.joker_type.normal;
                    else
                        my_rank = UnityEngine.Random.Range(0,13);

                    deck.this_deck.my_wild_card_deck.Add_wild_card(my_rank, my_joker);
                    }

                if (quantity > 1)
                    WindowMessage.text += " wild cards";
                else
                    WindowMessage.text += " wild card";
            }
        }
    }

    /* //remove this line
    IEnumerator WaitForAd()
    {
        //pause the game when the ad is showing
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0;
        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }
    */ //remove this line

    //buttons
    public void Close_canvas()
    {
        myCanvas.SetActive(false);
    }

    /*//remove this line
    public void Play_Advertisement()
    {
        #if UNITY_EDITOR
        StartCoroutine(WaitForAd());
        #endif

        if (Advertisement.IsReady(rewardedVideoZone))
            {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show(rewardedVideoZone, options);//this work ONLY if "File" > "build setting" is set on iOS or Android
            }
    }
    */ //remove this line
}
