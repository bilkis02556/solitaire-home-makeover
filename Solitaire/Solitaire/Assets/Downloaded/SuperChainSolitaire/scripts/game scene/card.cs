﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class card : MonoBehaviour {

	//decide if this card will be auto-setup or if suit, ranck, face ordientation and card type will be set manually
	public enum my_setup
	{
		automatic,
		manual
	}
	public my_setup my_setup_selected = my_setup.automatic;

	public deck.card_class.card_type card_type_selected = deck.card_class.card_type.normal;
	public deck.bonus_type collectable_bonus_selected = deck.bonus_type.none;
	public bool casual_wild;//true mean that this is a wild card, but not set manually, so at restar, could be no more a wild card. See deck.Setup_board_card_by_percentage()


	//joker
	public deck.card_class.joker_type joker_type_selected = deck.card_class.joker_type.none;
	public bool casual_joker;

    deck.card_class my;

	public enum card_suit
	{
		random = -1,
		clubs = 0,
		spades = 1,
		diamonds = 2,
		hearts = 3
	}
	public card_suit card_suit_selected = card_suit.random;//decide if this card will have a random suit at each game start/restart or always the same
	

	public enum card_rank
	{
		random = -1,
		_A = 0,
		_2 = 1,
		_3 = 2,
		_4 = 3,
		_5 = 4,
		_6 = 5,
		_7 = 6,
		_8 = 7,
		_9 = 8,
		_10 = 9,
		_J = 10,
		_Q = 11,
		_K = 12
	}
	public card_rank card_rank_selected = card_rank.random;//decide if this card will have a random rank at each game start/restart or always the same

	//the final rank and suit of this card
	public int my_suit;
	public int my_rank;

	//card appearance, it will be auto-setup from deck script
	public SpriteRenderer my_face;
	public SpriteRenderer my_back;

    public bool face_up;//the current face orientation
	public bool always_face_up;//if true, this card will be always face up, otherwise will be face up only if this card not have any card over itself
	public bool causal_face_up;//this card will be face up only for until restart. See deck.Setup_board_card_by_percentage()
	public bool this_is_a_bottom_card;//this card not have any other card under itself (deck script use this for call bottom_card_score when this card is pick)

	Transform[] start_cards_over_me;//only if this is none, this card can be click
	public Transform[] current_cards_over_me;//needed for shuffle
	Transform[] temp_current_cards_over_me;

	//rotation settings for animation. The script auto-setup this
	public Quaternion current_my_rotation_up;
		Quaternion temp_my_rotation_up;
	public Quaternion current_my_rotation_down;
		Quaternion temp_my_rotation_down;
	public Quaternion start_my_rotation_up;
	Quaternion start_my_rotation_down;

	//the overlap detectors that check if there are other card over this one
	public float big_radius;
	public float small_radius;
	public Transform[] overlap_small;

	public bool editor_show_overlap;//open/close the overlap detectors menu in inspector
	public bool editor_show_advanced;

	//padlock
	public enum padlock_model
	{
		none,
		normal,//take damege when chain match on it
		royal,//take damage for each J,Q or K taken on board
		keyhole//require key bonus to open
	}
	public padlock_model padlock_model_selected = padlock_model.none;
	public int padlock_hp;
	public int current_padlock_hp;
	public SpriteRenderer padlock_front;
	public Text padlock_front_text;
	public SpriteRenderer padlock_back;
	public Text padlock_back_text;

	//start scale and rotation to restore card after destroy animation
	public Quaternion my_start_rotation;
	public Quaternion my_current_rotation;
	public Vector3 my_start_position;//to restore board after Shuffle_board()
	public Vector3 my_current_position;
    //undo shuffle
    Quaternion my_previous_rotation;
    Vector3 my_previous_position;
    Quaternion my_previous_rotation_up;
    Quaternion my_previous_rotation_down;

    Vector3 my_start_localScale;

	public bool shuffle_me;


    #region start and restore original values
    // Use this for initialization
    void Start () {

        my_start_rotation = this.transform.rotation;
		my_current_rotation = my_start_rotation;

		my_start_position = this.transform.position;
		my_current_position = my_start_position;

		my_start_localScale = this.transform.localScale;

		start_my_rotation_up = this.transform.localRotation;
		current_my_rotation_up = start_my_rotation_up;

		transform.Rotate (current_my_rotation_up.x, current_my_rotation_up.y + 180, current_my_rotation_up.z, Space.Self);
		start_my_rotation_down = this.transform.localRotation;
		current_my_rotation_down = start_my_rotation_down;

		Padlock_setup();
		Start_me ();

	}

	void Restore_original_position()
	{
		if (deck.this_deck.game_started)
			{
			Debug.Log(name + " Restore_original_position()");
			this.transform.rotation = my_start_rotation;
			this.transform.position = my_start_position;
			this.transform.localScale = my_start_localScale;
			this.transform.localRotation = start_my_rotation_up;
			current_my_rotation_up = start_my_rotation_up;

			current_cards_over_me = new Transform[start_cards_over_me.Length];
			Array.Copy (start_cards_over_me, current_cards_over_me, start_cards_over_me.Length);
			}
	}

    public void Start_me()
    {

        Generate_me();//set rank, suit and sprites

        Find_cards_over_me();

        Turn_this_card();//check if this card must be face up or down
    }


    public void Reset_me()
	{
		Restore_original_position ();

		Padlock_setup();

		transform.rotation = my_start_rotation;
		transform.localScale = my_start_localScale;

		this.gameObject.SetActive (true);
		shuffle_me = false;

        if (!always_face_up)
			{
			face_up = false;
			causal_face_up = false;
			}

		if (casual_wild)
			{
			casual_wild = false;
			card_type_selected = deck.card_class.card_type.normal;
			}

		if (casual_joker)
			{
			casual_joker = false;
			joker_type_selected = deck.card_class.joker_type.none;
			}

	}



    public void Find_cards_over_me()
    {
        //store the overlap detectors
        Collider2D[][] temp_colliders = new Collider2D[overlap_small.Length + 1][];
        int total_lengh = 0;
        //small circles
        for (int i = 0; i < overlap_small.Length; i++)
        {
            temp_colliders[i] = Physics2D.OverlapCircleAll(new Vector2(overlap_small[i].position.x, overlap_small[i].position.y), small_radius);
            total_lengh += temp_colliders[i].Length;
        }

        //big circle
        temp_colliders[overlap_small.Length] = Physics2D.OverlapCircleAll(new Vector2(this.transform.position.x, this.transform.position.y), big_radius);
        total_lengh += temp_colliders[overlap_small.Length].Length;

        //check if there are cards over this one
        Transform[] temp_cards_over_me = new Transform[total_lengh];

        Transform[] temp_cards_over_me_check = new Transform[total_lengh];
        bool is_new = true;

        int temp_count = 0;
        int total = 0;

        this_is_a_bottom_card = true;
        //check small circles
        for (int i = 0; i < overlap_small.Length; i++)
        {
            for (int ii = 0; ii < temp_colliders[i].Length; ii++)
            {
                if (temp_colliders[i][ii].transform.position.z < this.transform.position.z)//there is a card over me
                {
                    is_new = true;
                    for (int c = 0; c < temp_cards_over_me_check.Length; c++)
                    {
                        if (temp_cards_over_me_check[c] == temp_colliders[i][ii].transform)
                            is_new = false;
                    }
                    if (is_new)
                    {
                        temp_cards_over_me[temp_count] = temp_colliders[i][ii].transform;
                        temp_cards_over_me_check[temp_count] = temp_colliders[i][ii].transform;
                        total++;
                    }
                }
                else if (temp_colliders[i][ii].transform.position.z > this.transform.position.z)//there is a card under me
                {
                    this_is_a_bottom_card = false;
                }
                temp_count++;
            }
        }
        //check big circle
        for (int ii = 0; ii < temp_colliders[overlap_small.Length].Length; ii++)
        {
            if (temp_colliders[overlap_small.Length][ii].transform.position.z < this.transform.position.z)//there is a card over me
            {
                is_new = true;
                for (int c = 0; c < temp_cards_over_me_check.Length; c++)
                {
                    if (temp_cards_over_me_check[c] == temp_colliders[overlap_small.Length][ii].transform)
                        is_new = false;
                }
                if (is_new)
                {
                    temp_cards_over_me[temp_count] = temp_colliders[overlap_small.Length][ii].transform;
                    total++;
                }
            }
            else if (temp_colliders[overlap_small.Length][ii].transform.position.z > this.transform.position.z)//there is a card under me
            {
                this_is_a_bottom_card = false;
            }
        }

        //save card found in an array in order to know when the player will pick all the card above this one
        if (!deck.this_deck.game_started)
            start_cards_over_me = new Transform[total];
        else
            current_cards_over_me = new Transform[total];
        temp_count = 0;
        for (int i = 0; i < total_lengh; i++)
        {
            if (temp_cards_over_me[i] != null)
            {
                if (!deck.this_deck.game_started)
                    start_cards_over_me[temp_count] = temp_cards_over_me[i];
                else
                    current_cards_over_me[temp_count] = temp_cards_over_me[i];
                temp_count++;
            }
        }

        if (!deck.this_deck.game_started)
        {
            current_cards_over_me = new Transform[total];
            Array.Copy(start_cards_over_me, current_cards_over_me, total);
        }

        //Debug.Log (name + " = " + current_cards_over_me.Length);

    }

    //show the overlap detectors
    void OnDrawGizmos()
    {
        //main circle
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(this.transform.position, big_radius);
        //small circles
        Gizmos.color = Color.cyan;
        for (int i = 0; i < overlap_small.Length; i++)
            Gizmos.DrawWireSphere(overlap_small[i].position, small_radius);

    }
    #endregion

    #region card generation
    void Random_wild_card()//decide if this will be a random wild card
    {
        if (deck.this_deck.distribute_card_metod_for_wild_card_on_board == deck.distribute_card_metod.random_roll_for_each_card)
        {
            if (UnityEngine.Random.Range(1, 100) <= deck.this_deck.chance_of_wild_card_on_board)
            {
                casual_wild = true;
                card_type_selected = deck.card_class.card_type.wild;
            }
        }
    }

    void Random_joker()
    {
        if (deck.this_deck.distribute_card_metod_for_joker_on_board == deck.distribute_card_metod.random_roll_for_each_card)
        {
            if (UnityEngine.Random.Range(1, 100) <= deck.this_deck.chance_of_joker_on_board)
            {
                casual_joker = true;
                my_rank = -99;
                if (card_type_selected == deck.card_class.card_type.wild)//wild card can have only normal joker
                    joker_type_selected = deck.card_class.joker_type.normal;
                else
                {
                    int random_joker_type = UnityEngine.Random.Range(1, 4);
                    joker_type_selected = (deck.card_class.joker_type)random_joker_type;
                }
            }
        }
    }

    void Generate_me()
    {
        //print("Generate_me");

        if (card_type_selected == deck.card_class.card_type.normal)//only normal card can became casually wild
            Random_wild_card();

        if (my_setup_selected == my_setup.automatic)
            Random_joker();

        if (joker_type_selected == deck.card_class.joker_type.none)
        {
            deck.card_class tempCard = new deck.card_class();
            if (deck.this_deck.useMinimumAmountOfDecksToFillBoardAndBottomDeck)
            {
                bool avoidK = (deck.this_deck.main_rule_selected == deck.main_rule.sum13 && padlock_model_selected == padlock_model.normal);
                tempCard = deck.this_deck.PickRandomCardFromBucketDecks(avoidK);
            }



            if (card_suit_selected == card_suit.random)
            {
                if (deck.this_deck.useMinimumAmountOfDecksToFillBoardAndBottomDeck)
                    my_suit = tempCard.suit;
                else
                    my_suit = UnityEngine.Random.Range(0, 4);
            }

            if (card_rank_selected == card_rank.random)
            {
                if (deck.this_deck.useMinimumAmountOfDecksToFillBoardAndBottomDeck)
                {
                    my_rank = tempCard.rank;
                }
                else
                {
                    bool approved_rank = false;
                    int rank_range = 13;
                    if (deck.this_deck.main_rule_selected == deck.main_rule.sum13 && padlock_model_selected == padlock_model.normal)//avoid K in padlock card if sum13 rule
                        rank_range = 12;

                    my_rank = UnityEngine.Random.Range(0, rank_range);

                    //avoid to generate too many card with the same rank
                    int safety_stop = 100;
                    while (!approved_rank)
                    {
                        safety_stop--;
                        if (safety_stop <= 0)
                        {
                            Debug.LogWarning("safety_stop");
                            break;
                        }

                        if (deck.this_deck.card_on_board_by_rank[my_rank] <= deck.this_deck.max_number_of_card_with_the_same_rank_on_board)//if you don't have use this rank too much on the board
                        {
                            approved_rank = true;//this is a valid rank to use
                        }
                        else
                            my_rank = UnityEngine.Random.Range(0, 13);
                    }
                }

                deck.this_deck.card_on_board_by_rank[my_rank]++;
            }
        }



        Setup_card_appearance();



        my = new deck.card_class();
        my.rank = my_rank;
        my.suit = my_suit;
        my.card_type_selected = card_type_selected;
        my.collectable_bonus_selected = collectable_bonus_selected;
        my.joker_type_selected = joker_type_selected;
        my.board_card = this;

    }

    void Setup_card_appearance()
    {
        switch (card_type_selected)
        {
            case deck.card_class.card_type.normal:


                my_back.sprite = deck.this_deck.card_back_sprite;

                switch (joker_type_selected)
                {
                    case deck.card_class.joker_type.none:
                        my_face.sprite = deck.this_deck.card_deck_sprite[my_suit, my_rank];
                        break;

                    case deck.card_class.joker_type.normal:
                        my_face.sprite = deck.this_deck.normal_joker;
                        my_suit = 100;
                        break;

                    case deck.card_class.joker_type.red:
                        my_face.sprite = deck.this_deck.red_joker;
                        my_suit = 101;
                        break;

                    case deck.card_class.joker_type.black:
                        my_face.sprite = deck.this_deck.black_joker;
                        my_suit = 102;
                        break;
                }
                break;

            case deck.card_class.card_type.gold:

                my_back.sprite = deck.this_deck.gold_card_back_sprite;
                deck.this_deck.total_gold_cards++;
                deck.this_deck.Update_gold_cards_count();


                switch (joker_type_selected)
                {
                    case deck.card_class.joker_type.none:
                        my_face.sprite = deck.this_deck.gold_card_deck_sprite[my_suit, my_rank];
                        break;

                    case deck.card_class.joker_type.normal:
                        my_face.sprite = deck.this_deck.gold_normal_joker;
                        my_suit = 100;
                        break;

                    case deck.card_class.joker_type.red:
                        my_face.sprite = deck.this_deck.gold_red_joker;
                        my_suit = 101;
                        break;

                    case deck.card_class.joker_type.black:
                        my_face.sprite = deck.this_deck.gold_black_joker;
                        my_suit = 102;
                        break;
                }

                break;

            case deck.card_class.card_type.wild:

                my_suit = -99;
                my_back.sprite = deck.this_deck.card_back_sprite;

                if (joker_type_selected == deck.card_class.joker_type.none)
                {
                    my_face.sprite = deck.this_deck.wild[my_rank];
                }
                else
                {
                    my_face.sprite = deck.this_deck.wild_joker;
                }
                break;

            case deck.card_class.card_type.collectable_bonus:
                my_suit = -99;
                my_rank = -99;

                my_back.sprite = deck.this_deck.card_back_sprite;

                my_face.sprite = deck.this_deck.bonus_cards_sprites[(int)collectable_bonus_selected];

                break;
        }
    }
   
    public void Update_card_appearance()
    {
        Setup_card_appearance();

        switch (padlock_model_selected)
        {

            case padlock_model.normal:
                padlock_front.sprite = deck.this_deck.padlock_sprite;
                padlock_back.sprite = deck.this_deck.padlock_sprite;
                break;

            case padlock_model.royal:
                padlock_front.sprite = deck.this_deck.royal_padlock_sprite;
                padlock_back.sprite = deck.this_deck.royal_padlock_sprite;
                break;

            case padlock_model.keyhole:
                padlock_front.sprite = deck.this_deck.keyhole_padlock_sprite;
                padlock_back.sprite = deck.this_deck.keyhole_padlock_sprite;
                break;
        }
    }

    #endregion

    #region padlock
    void Padlock_setup()
	{

		switch(padlock_model_selected)
		{
			case padlock_model.none:
				padlock_hp = 0;
				current_padlock_hp = 0;

				Hide_padlock();
			break;

			case padlock_model.normal:
				current_padlock_hp = padlock_hp;
				Update_padlock_text();

				padlock_front.sprite = deck.this_deck.padlock_sprite;
				padlock_back.sprite = deck.this_deck.padlock_sprite;

				Show_padlock();

			break;

		case padlock_model.royal:
			current_padlock_hp = padlock_hp;
			Update_padlock_text();
			
			padlock_front.sprite = deck.this_deck.royal_padlock_sprite;
			padlock_back.sprite = deck.this_deck.royal_padlock_sprite;

			Show_padlock();
			break;

		case padlock_model.keyhole:
			current_padlock_hp = padlock_hp;
			Update_padlock_text();
			
			padlock_front.sprite = deck.this_deck.keyhole_padlock_sprite;
			padlock_back.sprite = deck.this_deck.keyhole_padlock_sprite;
			
			Show_padlock();
			break;
		}
	}

	void Update_padlock_text()
	{
		padlock_front_text.text = current_padlock_hp.ToString ();
		padlock_back_text.text = current_padlock_hp.ToString ();
	}

	void Hide_padlock()
	{
		padlock_front.enabled = false;
		padlock_front_text.enabled = false;
		padlock_back.enabled = false;
		padlock_back_text.enabled = false;
	}

	void Show_padlock()
	{
		padlock_front.enabled = true;
		padlock_front_text.enabled = true;
		padlock_back.enabled = true;
		padlock_back_text.enabled = true;
	}


    public void Repair_padlock()
    {
        Debug.Log("Repair_padlock()");
        current_padlock_hp++;
        Update_padlock_text();
        if (current_padlock_hp == 1)//if before was zero
            Show_padlock();//reactivate padlock
    }

    public IEnumerator Repair_royal_padlock()
    {
        current_padlock_hp++;
        Update_padlock_text();
        if (current_padlock_hp == 1)//if before was zero
        {
            Turn_this_card();
            yield return new WaitForSeconds(deck.this_deck.animation_duration);
            Show_padlock();//reactivate padlock
        }
    }


    public void Damage_padlock(bool damaged_by_a_bonus = false)
    {
        print("Damage_padlock");
        current_padlock_hp--;
        Update_padlock_text();

        if (!damaged_by_a_bonus)
        {

            if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                {
                //for undo
                deck.this_deck.current_last_move = deck.last_move.damage_padlock;
                deck.this_deck.lastest_deactivate_board_card = this.gameObject;
                deck.this_deck.Update_undo_button(true);//turn on undo button


                //add a clone-card to target deck - the clone card will be normal though the original is wild or gold, in order to avoid the multiplication of special cards
                deck.this_deck.undo_previous_score = 0;
                deck.this_deck.previous_bonus_selected = deck.bonus_type.none;
                deck.this_deck.Update_score(deck.this_deck.normal_card_score);
                int temp_suit = my_suit;
                if (card_type_selected == deck.card_class.card_type.wild)
                    temp_suit = UnityEngine.Random.Range(0, 4);//wild card don't have suit, so you bust assign one to clone card

                //put this card sprite on the ghost card used to show the card movement from board to target deck
                deck.this_deck.ghost_card_SpriteRenderer.sprite = my_face.sprite;//deck.this_deck.Assign_the_sprite_of_this_card(temp_suit,my_rank,deck.card_class.card_type.normal);
                                                                                 
                ParticleEffect();

                //put this card in target deck
                if (deck.this_deck.enhanced_animation)
                    deck.this_deck.StartCoroutine(deck.this_deck.Move_to_enhanced(transform, deck.this_deck.target_new_card.transform,
                                                                               my, deck.last_move.take_card_from_board_and_put_it_into_target_deck, false));
                else
                    deck.this_deck.StartCoroutine(deck.this_deck.Move_to(transform, deck.this_deck.target_new_card.transform, false));//show the card movement from board to target deck (it is an illusion, this card just became inactive and the ghost card move from this card position to target deck position)

                //the clone card will be normal though the original il wild or gold, in order to avoid the multiplication of special cards
                //deck.this_deck.New_current_deck_card(temp_suit, my_rank, deck.card_class.card_type.normal, joker_type_selected, deck.last_move.damage_padlock, false);//set this card as new top card on the target deck
                deck.card_class tempCard = new deck.card_class();
                tempCard = my;
                tempCard.suit = temp_suit;
                tempCard.card_type_selected = deck.card_class.card_type.normal;

                if (!deck.this_deck.enhanced_animation)
                    deck.this_deck.New_current_deck_card(tempCard, deck.last_move.damage_padlock, false);//set this card as new top card on the target deck

            }
        }
        else
        {
            Debug.Log("padlock damaged by a bonus");
            deck.this_deck.current_total_padlock_hp_on_board--;
        }

        //check if the padlock is destroyed
        if (current_padlock_hp <= 0)
        {
            Hide_padlock();
        }

    }

    public void Damage_royal_padlock()
    {
        current_padlock_hp--;
        Update_padlock_text();

        //check if the padlock is destroyed
        if (current_padlock_hp <= 0)
        {
            Hide_padlock();
            if (current_padlock_hp == 0)
                Turn_this_card();
        }
    }
    #endregion

    #region Shuffle
    public void Shuffle_new_position(Vector3 new_position, Quaternion new_rotation, Quaternion new_rotation_up, Quaternion new_rotation_down)//Transform[] new_cards_over_me)
	{
        //store info for undo
        my_previous_position = transform.position;
        my_previous_rotation = transform.rotation;
        my_previous_rotation_up = current_my_rotation_up;
        my_previous_rotation_down = current_my_rotation_down;

        shuffle_me = false;


        my_current_position = new_position;
		my_current_rotation = new_rotation;

		temp_my_rotation_up = new_rotation_up;
		temp_my_rotation_down = new_rotation_down;


	}

    public void Undo_shuffle()
    {
        my_current_position = my_previous_position;
        my_current_rotation = my_previous_rotation;
        current_my_rotation_up = my_previous_rotation_up;
        current_my_rotation_down = my_previous_rotation_down;

        StartCoroutine(Move_me(my_current_position));
        StartCoroutine(Rotate_me(my_current_rotation));

    }

    public void End_shuffle()
	{

		current_my_rotation_up = temp_my_rotation_up;
		current_my_rotation_down = temp_my_rotation_down;

		StartCoroutine(Move_me (my_current_position));
		StartCoroutine(Rotate_me (my_current_rotation));

	}
    #endregion


    #region player interaction
    void OnMouseDown()
	{
		//Debug.Log ("current_padlock_hp: " + current_padlock_hp + " ... face_up = " + face_up + " cards over me " + current_cards_over_me.Length);

		if (deck.this_deck.game_end || !deck.this_deck.player_can_move)
			return;

		if (Input.touches.Length <= 1)//if the palyer click or tap on this card
			{
			//Debug.Log ("S"+my_suit + "R" + my_rank + " * up = " + face_up + " * free = " +This_card_is_free() + " ... " + card_type_selected + " ... " + joker_type_selected);
			if (This_card_is_free()) //if this card is free
				{
				switch(deck.this_deck.current_bonus_selected)
					{
					case deck.bonus_type.none:

					if (current_padlock_hp <= 0)//if this card don't have a padlock
						{
						if (card_type_selected == deck.card_class.card_type.wild) //wild card can be always taken
                            {
                                if (deck.this_deck.main_rule_selected != deck.main_rule.chain)
                                    deck.this_deck.DeselectCard();

                            This_is_the_new_current_deck_card();
                            }
                    else if (card_type_selected == deck.card_class.card_type.collectable_bonus)//add this bonus to bonus container
							{
                                if (deck.this_deck.main_rule_selected != deck.main_rule.chain)
                                    deck.this_deck.DeselectCard();

                                deck.this_deck.bonus_button_script[(int)collectable_bonus_selected].Add_a_charge();
                                ParticleEffect();
                                DestroyMe(true);

                                //no undo
                                deck.this_deck.current_last_move = deck.last_move.none;
                                deck.this_deck.Update_undo_button(false);//turn off undo button

                            }
						else//you can take this card only if rank value is near to current card
							{
							switch(joker_type_selected)
								{
								case deck.card_class.joker_type.none:
                                    //check if this card can became the new current card
                                    switch (deck.this_deck.selected_card.joker_type_selected)
										{
                                        case deck.card_class.joker_type.none://if the current card not is a joker, check the rank
                                             if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                                {
                                                        if (my_rank == 0)
												            {
												            if (deck.this_deck.selected_card.rank == 12 || (deck.this_deck.selected_card.rank == (my_rank + 1)))
                                                                This_is_the_new_current_deck_card();
                                                            else
													            Wrong_card();
												            } 
											            else if (my_rank == 12)
												            {
												            if (deck.this_deck.selected_card.rank == 0 || (deck.this_deck.selected_card.rank == (my_rank - 1)))
                                                                This_is_the_new_current_deck_card();
                                                            else
													            Wrong_card();
												            }
											            else
												            {
												            if ( (deck.this_deck.selected_card.rank == (my_rank + 1)) || (deck.this_deck.selected_card.rank == (my_rank - 1)) )
                                                                This_is_the_new_current_deck_card();
                                                            else
													            Wrong_card();
												            }
                                                }
                                            else
                                                {
                                                    if (CheckIfCompareThisCard())
                                                        CompareCards();

                                                }
                                                break;
                                        break;

									case deck.card_class.joker_type.normal://normal joker take all cards
                                                print("case deck.card_class.joker_type.normal:");
                                            if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                                This_is_the_new_current_deck_card();
                                            else
                                                {
                                                    print("click this card with a joker already selected");
                                                    deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
                                                }
                                   break;

									case deck.card_class.joker_type.red://take only red card
                                                print("case deck.card_class.joker_type.red:");
                                            if (my_suit == 2 || my_suit == 3)
                                                    {
                                                    if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                                        This_is_the_new_current_deck_card();
                                                    else
                                                        deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
                                                    }
                                            else
											    Wrong_card();
                                    break;

									case deck.card_class.joker_type.black://take only black card
                                                print("case deck.card_class.joker_type.black:");
                                        if (my_suit == 0 || my_suit == 1)
                                            {
                                            if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                                This_is_the_new_current_deck_card();
                                             else
                                                deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
                                            }
                                        else
                                            Wrong_card();
                                            

                                     break;
										}
								break;

								case deck.card_class.joker_type.normal:
                                        print("deck.card_class.joker_type.normal:");
                                        if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                            This_is_the_new_current_deck_card();
                                        else
                                            {
                                            print("click on normal joker as second selection");
                                            if (CheckIfCompareThisCard())
                                                {
                                                deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
                                                }
                                            }
                                break;
								
								case deck.card_class.joker_type.red:

                                        if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                            {
                                            if (deck.this_deck.selected_card.suit == 2 || deck.this_deck.selected_card.suit == 3 || deck.this_deck.selected_card.suit == 101 //red suit
								            || deck.this_deck.selected_card.suit == 100)//normal joker
                                                This_is_the_new_current_deck_card();
                                            else
										        Wrong_card();
                                            }
                                        else
                                            {
                                            if (CheckIfCompareThisCard())
                                            {
                                                if (deck.this_deck.selected_card.suit == 2 || deck.this_deck.selected_card.suit == 3 || deck.this_deck.selected_card.suit == 101 //red suit
                                            || deck.this_deck.selected_card.suit == 100)//normal joker
                                                    deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
                                                else
                                                    Wrong_card();
                                            }
                                        }
                                break;
								
								case deck.card_class.joker_type.black:
                                    if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                        {
                                        if (deck.this_deck.selected_card.suit == 0 || deck.this_deck.selected_card.suit == 1 || deck.this_deck.selected_card.suit == 102 //black suit
								        || deck.this_deck.selected_card.suit == 100)//normal joker
                                            This_is_the_new_current_deck_card();
                                        else
										    Wrong_card();
                                        }
                                    else
                                        {
                                        if (CheckIfCompareThisCard())
                                            {
                                            if (deck.this_deck.selected_card.suit == 0 || deck.this_deck.selected_card.suit == 1 || deck.this_deck.selected_card.suit == 102 //black suit
                                            || deck.this_deck.selected_card.suit == 100)//normal joker
                                                    deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
                                                else
                                                    Wrong_card();
                                            }
                                        }
                                break;
								}

							}
						} 
					else//this card have a padlock
						{
						if (padlock_model_selected == padlock_model.normal)
							{
							switch(joker_type_selected)//there is a joker in the clicked card?
								{
								case deck.card_class.joker_type.none:
									switch(deck.this_deck.selected_card.joker_type_selected)//there is a joker in the current card?
										{
										case deck.card_class.joker_type.none:
											//check if this padlock can be damaged
                                            if (deck.this_deck.main_rule_selected == deck.main_rule.chain)
                                                {
                                                if (my_rank == 0)
												    {
												    if (deck.this_deck.selected_card.rank == 12 || (deck.this_deck.selected_card.rank == (my_rank + 1)))
													    {
													    Damage_padlock();
													    } 
												    else
													    Wrong_card();
												    } 
											    else if (my_rank == 12)
												    {
												    if (deck.this_deck.selected_card.rank == 0 || (deck.this_deck.selected_card.rank == (my_rank - 1)))
													    {
													    Damage_padlock();
													    }
												    else
													    Wrong_card();
												    }
											    else
												    {
												    if ( (deck.this_deck.selected_card.rank == (my_rank + 1)) || (deck.this_deck.selected_card.rank == (my_rank - 1)) )
													    {
													    Damage_padlock();
													    }
												    else
													    Wrong_card();
												    }
                                                }
                                            else
                                                {
                                                    if (CheckIfCompareThisCard())
                                                        CompareCards();
                                                }
                                        break;

										case deck.card_class.joker_type.normal://normal joker take all cards
											    Damage_padlock();
                                            break;
											
										case deck.card_class.joker_type.red://take only red card
											if (my_suit == 2 || my_suit == 3)
                                                Damage_padlock();
                                            else
												Wrong_card();
											break;
											
										case deck.card_class.joker_type.black://take only black card
											if (my_suit == 0 || my_suit == 1)
												Damage_padlock();
											else
												Wrong_card();
											break;
										}
								break;

								case deck.card_class.joker_type.normal:
									Damage_padlock();
								break;
									
								case deck.card_class.joker_type.red:
									if (deck.this_deck.selected_card.suit == 2 || deck.this_deck.selected_card.suit == 3 || deck.this_deck.selected_card.suit == 101 //red suit
									    || deck.this_deck.selected_card.suit == 100)//normal joker
										Damage_padlock();
									else
										Wrong_card();
								break;
									
								case deck.card_class.joker_type.black:
									if (deck.this_deck.selected_card.suit == 0 || deck.this_deck.selected_card.suit == 1 || deck.this_deck.selected_card.suit == 102 //black suit
									    || deck.this_deck.selected_card.suit == 100)//normal joker
										Damage_padlock();
									else
										Wrong_card();
								break;

								}
							} 
						} 

					break;
					
					case deck.bonus_type.destroy_one:
						if (padlock_model_selected != padlock_model.keyhole || current_padlock_hp <= 0)
							{
							//for undo:
							deck.this_deck.latest_cards_affected_by_the_bonus = new card[1];
							deck.this_deck.latest_cards_affected_by_the_bonus[0] = this;

							deck.this_deck.bonus_button_script[0].Consume_a_charge();

							Damage_this_card(true);
							}
						else
							{
							deck.this_deck.Deselect_all_bonus_buttons();
							Wrong_card();
							}
							
				
					break;

					case deck.bonus_type.bonus_key:
						if (padlock_model_selected == padlock_model.keyhole)
							{
							deck.this_deck.bonus_button_script[2].Consume_a_charge();
							//Damage_keyhole_padlock
							current_padlock_hp--;
							Update_padlock_text ();
						
							//for undo
							deck.this_deck.latest_cards_affected_by_the_bonus = new card[1];
							deck.this_deck.latest_cards_affected_by_the_bonus[0] = this;
						
							//check if the padlock is destroyed
							if (current_padlock_hp <= 0)
								{
								Hide_padlock();
								}
							}
						else
							{
							deck.this_deck.Deselect_all_bonus_buttons();
							Wrong_card();
							}
					break;
					}
				} 
			else
				Blocked_card();//this card have another card over itself
			}
	}

    void CompareCards()
    {
        if (deck.this_deck.main_rule_selected == deck.main_rule.sum13)
        {
            int sum = deck.this_deck.selected_card.rank + 1 + my.rank + 1;
            print("sum: " + sum);
            if (sum == 13)
                deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
            else
                Wrong_card();
        }
        else if (deck.this_deck.main_rule_selected == deck.main_rule.matchSameRank)
        {
            if (deck.this_deck.selected_card.rank == my.rank)
                deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, my);
            else
                Wrong_card();
        }
    }

    bool CheckIfCompareThisCard()
        {
        if (deck.this_deck.main_rule_selected == deck.main_rule.sum13)
            {
            if ((my_rank + 1) == 13)
                {
                if (current_padlock_hp > 0)
                    {
                    Wrong_card();
                    return false;
                    }

                deck.this_deck.DeselectCard();
                DestroyMe(true);
                //undo
                deck.this_deck.previous_match_card_A = my;
                deck.this_deck.previous_match_card_B = new deck.card_class();
                deck.this_deck.current_last_move = deck.last_move.sum13_destroy_K_on_board;
                deck.this_deck.Update_undo_button(true);//turn on undo button
                return false;
                }
            }

        if (deck.this_deck.selected_card.rank == -99)
            {
            print("click me as first selection");
            deck.this_deck.SelectCard(my, transform);
            return false;
            }
        else //compare me with the selected card
            {
            print("click me as second selection");
            //if you click two times on the same card -> deselect me
            if (deck.this_deck.selected_card.board_card == this)
                {
                print("deselect");
                deck.this_deck.DeselectCard();
                return false;
                }
            }
        return true;
        }

public IEnumerator Undo_damage_effect()
	{
		//Debug.Log ("current_padlock_hp: " + current_padlock_hp);
		deck.this_deck.player_can_move = false;

		gameObject.SetActive (true);

		if (padlock_model_selected == padlock_model.none || current_padlock_hp < 0)
			{
			//animation
			float duration = 0.5f;
			float time = 0;
			while (time < 1)
				{
				time += Time.smoothDeltaTime / duration;
				transform.Rotate (Vector3.back * Time.deltaTime * 1000);
				transform.localScale = Vector3.Lerp(Vector3.zero,Vector3.one,time);
				yield return null;
				}

			deck.this_deck.current_total_card_on_board++;
			if (my_rank >= 10 && my_rank <= 12)//if this is a royal card
				deck.this_deck.Repair_last_royal_padlocks_damaged();
			
			transform.rotation = my_start_rotation;
			transform.localScale = my_start_localScale;
			}
		else if (padlock_model_selected == padlock_model.normal || padlock_model_selected == padlock_model.keyhole)
			{
			deck.this_deck.current_total_padlock_hp_on_board++;
			Repair_padlock();
			}
		else if (padlock_model_selected == padlock_model.royal)
			{
			StartCoroutine(Repair_royal_padlock());
			}

		deck.this_deck.player_can_move = true;
	}

	public void Damage_this_card(bool updateScore)
	{
		if (padlock_model_selected == padlock_model.none)
            DestroyMe(updateScore);
        else
			{
			if (current_padlock_hp <= 0)
				{
				current_padlock_hp = -1;
                DestroyMe(updateScore);
                }
			else
				{
				if (padlock_model_selected == padlock_model.normal)
					Damage_padlock(true);
				else if (padlock_model_selected == padlock_model.royal)
					Damage_royal_padlock();
				}
			}

	}

    public void DestroyMe(bool updateScore)
        {
        StartCoroutine(Destroy_this_card(updateScore));
        }

    IEnumerator Destroy_this_card(bool updateScore)
		{
		deck.this_deck.player_can_move = false;

        //selected.SetActive(false);

        deck.this_deck.Play_sfx(deck.this_deck.destroy_this_card_sfx);

		//animation
		float duration = 0.5f;
		float time = 0;
		while (time < 1)
			{
			time += Time.smoothDeltaTime / duration;
			transform.Rotate (Vector3.forward * Time.deltaTime * 1000);
			transform.localScale = Vector3.Lerp(Vector3.one,Vector3.zero,time);
			yield return null;
			}

		gameObject.SetActive (false);

		if (my_rank >= 10 && my_rank <= 12)//if this is a royal card
			deck.this_deck.Damage_all_free_royal_padlocks();

		deck.this_deck.current_total_card_on_board--;
		Card_removed(updateScore);

		deck.this_deck.Check_how_many_card_remain_on_board ();

		deck.this_deck.player_can_move = true;
		}

	public bool This_card_is_free()//check if the card not have others card over itself anymore
	{
		bool return_this = true;

		if (current_cards_over_me.Length == 0)//this card neved had other card over itself
			return_this = true;
		else
			{
			for (int i = 0; i < current_cards_over_me.Length; i++)
				{
				if (current_cards_over_me[i]!= null)//there is a card over me
					{
					if (current_cards_over_me[i].gameObject.activeSelf == true)//in it is active
						{
						return_this = false;//I found a card over me, so player can't pick me
						break;
						}
					}
				}
			}
		return return_this;
	}

	public void Turn_this_card() //decide if rotate this card face up or down
	{
		//turn up random cards at start
		if (!deck.this_deck.game_started)
			{
			if (deck.this_deck.distribute_card_metod_for_turn_up_a_covered_card == deck.distribute_card_metod.random_roll_for_each_card)
				{
				if (UnityEngine.Random.Range (1, 100) <= deck.this_deck.chance_of_turn_up_a_covered_card_at_start)
					causal_face_up = true;
				}
			}


		if (always_face_up || causal_face_up) //turn up card set up manually
			face_up = true;
		else //turn up only top card
			{
			if (padlock_model_selected == padlock_model.royal && current_padlock_hp > 0)//royal padlock card will be face down until padlock is destroyed
				face_up = false;
			else
				face_up = This_card_is_free();
			}

		if (face_up)
			{
			StartCoroutine(Rotate_me(current_my_rotation_up)); //rotate face up
			}
		else //rotate face down
			{
			if (transform.rotation.y ==  current_my_rotation_up.y)
				{
				if (deck.this_deck.game_started)
					StartCoroutine(Rotate_me(current_my_rotation_down)); //with animation because this card return turn down due an UNDO
				else //automatically at game start
					{	
					transform.Rotate (current_my_rotation_up.x, current_my_rotation_up.y + 180, current_my_rotation_up.z, Space.Self);
					current_my_rotation_down = this.transform.localRotation;
					}
				}
			}
	}

	public IEnumerator Move_me(Vector3 end_point)//the move animation
	{
		Vector3 start_point = this.transform.position;
		float duration = deck.this_deck.animation_duration;
		float time = 0;
		
		while (time < 1)
		{
			time += Time.smoothDeltaTime / duration;
			transform.position = Vector3.Lerp(start_point,end_point,time);
			yield return null;
		}
	}

	public IEnumerator Rotate_me(Quaternion end_point)//the rotate animation
	{
		Quaternion start_point = this.transform.rotation;
		float duration = deck.this_deck.rotation_duration;
		float time = 0;

		while (time < 1)
		{
			time += Time.smoothDeltaTime / duration;
			transform.rotation = Quaternion.Lerp(start_point,end_point,time);
			yield return null;
		}
	}

    
    void ParticleEffect()
    {
        if (!deck.this_deck.useParticles)
            return;

        GameObject myParticle = null;

        if (collectable_bonus_selected != deck.bonus_type.none)
            myParticle = deck.this_deck.particlesObj[8];
        else if (my.card_type_selected == deck.card_class.card_type.wild)
            myParticle = deck.this_deck.particlesObj[7];
        else if (my.joker_type_selected == deck.card_class.joker_type.none)
            myParticle = deck.this_deck.particlesObj[my.suit];
        else if (my.joker_type_selected == deck.card_class.joker_type.normal)
            myParticle = deck.this_deck.particlesObj[4];
        else if (my.joker_type_selected == deck.card_class.joker_type.red)
            myParticle = deck.this_deck.particlesObj[5];
        else if (my.joker_type_selected == deck.card_class.joker_type.black)
            myParticle = deck.this_deck.particlesObj[6];

        if (myParticle)
            {
            if (myParticle.activeSelf)
                myParticle.SetActive(false);

            myParticle.transform.position = this.transform.position;
            myParticle.SetActive(true);
            }
    }

    void This_is_the_new_current_deck_card()//put this card in the target deck
	{
		deck.this_deck.undo_previous_score = 0;
		deck.this_deck.previous_bonus_selected = deck.bonus_type.none;

		//put this card sprite on the ghost card used to show the card movement from board to target deck
		deck.this_deck.ghost_card_SpriteRenderer.sprite = my_face.sprite;

		//save the card values in order to recover them if player click undo
		deck.this_deck.lastest_deactivate_board_card = this.gameObject;
		gameObject.SetActive (false);

        ParticleEffect();

        deck.this_deck.Play_sfx(deck.this_deck.take_this_card_start_sfx);

        if ((card_type_selected == deck.card_class.card_type.wild) && (deck.this_deck.my_wild_card_deck.current_wild_cards < deck.this_deck.my_wild_card_deck.max_wild_cards))//this is a wild card and wild deck is not full
			{
			deck.this_deck.current_last_move = deck.last_move.take_card_from_board_and_put_it_into_wild_deck;//for undo

			//put his card in wild deck
            if (deck.this_deck.enhanced_animation)
                {
                deck.this_deck.StartCoroutine(deck.this_deck.Move_to_enhanced(transform, deck.this_deck.my_wild_card_deck.my_cards[deck.this_deck.my_wild_card_deck.current_wild_cards].transform,
                                                                               my, deck.last_move.take_card_from_board_and_put_it_into_wild_deck, false));
                }
            else
                {
                deck.this_deck.StartCoroutine(deck.this_deck.Move_to(transform, deck.this_deck.my_wild_card_deck.my_cards[deck.this_deck.my_wild_card_deck.current_wild_cards].transform ,false));//show card movement from board to wild deck
			    deck.this_deck.StartCoroutine(deck.this_deck.Add_wild_card_to_wild_deck_with_delay(my_rank,joker_type_selected));
                }
            deck.this_deck.current_total_card_on_board--;
			}
		else //this not is a wild card, or will deck is full, so...
			{
			deck.this_deck.current_last_move = deck.last_move.take_card_from_board_and_put_it_into_target_deck;//for undo

			//put this card in target deck
            if (deck.this_deck.enhanced_animation)
                deck.this_deck.StartCoroutine(deck.this_deck.Move_to_enhanced(transform, deck.this_deck.target_new_card.transform,
                                                                               my, deck.last_move.take_card_from_board_and_put_it_into_target_deck, false));//show the card movement from board to target deck (it is an illusion, this card just became inactive and the ghost card move from this card position to target deck position)
            else
                {
                deck.this_deck.StartCoroutine (deck.this_deck.Move_to (transform,deck.this_deck.target_new_card.transform,false));//show the card movement from board to target deck (it is an illusion, this card just became inactive and the ghost card move from this card position to target deck position)
                deck.this_deck.New_current_deck_card(my, deck.last_move.take_card_from_board_and_put_it_into_target_deck, false);//set this card as new top card on the target deck
                }

            }

        deck.this_deck.Update_undo_button (true);//turn on undo button


		Card_removed(true);
	}

    public int MyScoreValue()
    {

        int return_this = 0;

        if (this_is_a_bottom_card)
            return_this += deck.this_deck.bottom_card_score;

        if (card_type_selected == deck.card_class.card_type.normal)
            return_this += deck.this_deck.normal_card_score;
        else if (card_type_selected == deck.card_class.card_type.gold)
            return_this += deck.this_deck.gold_card_score;

        return return_this;
    }

    void Card_removed(bool updateScore)
		{
        
        if (updateScore)
            {
            deck.this_deck.Update_score(MyScoreValue());
            /*
            if (card_type_selected == deck.card_class.card_type.normal)
			    {
			    if (this_is_a_bottom_card)
				    {
				    deck.this_deck.Update_score (deck.this_deck.bottom_card_score);
				    }
			    else
				    {
				    deck.this_deck.Update_score (deck.this_deck.normal_card_score);
				    }
			    }
		    else if (card_type_selected == deck.card_class.card_type.gold)
			    deck.this_deck.Update_score (deck.this_deck.gold_card_score);*/
            }

        if (deck.this_deck.current_total_card_on_board > 0)//if there are cards left
			deck.this_deck.StartCoroutine(deck.this_deck.Refresh_card_rotation ());//rotate face up the cards now free
		}

	void Blocked_card()
	{
		Debug.Log ("this card have something over it");
		deck.this_deck.Play_sfx(deck.this_deck.blocked_card_sfx);

	}

	void Wrong_card()
	{
		Debug.Log ("wrong card rank");
		deck.this_deck.Play_sfx(deck.this_deck.wrong_card_sfx);

        StartCoroutine(deck.this_deck.Shake_card(this.transform));

        if (deck.this_deck.main_rule_selected != deck.main_rule.chain)
            deck.this_deck.DeselectCard();

    }
    #endregion
}
