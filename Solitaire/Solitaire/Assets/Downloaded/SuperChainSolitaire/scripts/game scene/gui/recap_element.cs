﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class recap_element : MonoBehaviour {

	public GameObject[] stars;
	public Text my_number;
	public Text no_stars;
	
	public Image my_image;

	public Sprite bk_to_do;
	public Sprite bk_done;
	public GameObject crown_icon;

	public void Star_on(int quantity)
	{
		if (quantity == 0)
			no_stars.text = "X";
		else
			{
			no_stars.text = "";
			for (int i = 0; i < stars.Length; i++)
				{
				if (i<quantity)
					stars[i].SetActive(true);
				else
					stars[i].SetActive(false);
				}
			}
	}

	public void Star_off()
	{
		for (int i = 0; i < stars.Length; i++)
			stars[i].SetActive(false);
	}


	public void Done(bool done, bool crown)
	{
		if (done)
			my_image.sprite = bk_done;
		else
			my_image.sprite = bk_to_do;

		crown_icon.SetActive (crown);
	}
}
