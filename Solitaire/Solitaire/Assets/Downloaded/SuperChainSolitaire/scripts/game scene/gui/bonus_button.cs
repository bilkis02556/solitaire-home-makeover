﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class bonus_button : MonoBehaviour {

	public Text quantity_text;
	public Image icon;
	public GameObject select_icon;

	[HideInInspector]
	public int my_id;



	public void Show_me()
	{
		if (deck.this_deck.current_bonus_quantity[my_id] > 0)
			this.gameObject.SetActive(true);
		else
			this.gameObject.SetActive(false);
	}

	public void Click_me()
	{
		if (deck.this_deck.game_end || !deck.this_deck.player_can_move)
			return;

        deck.this_deck.DeselectCard();

        if ((int)deck.this_deck.current_bonus_selected == my_id) //if click this when it is aready selected, deselect it
			{
			deck.this_deck.current_bonus_selected = deck.bonus_type.none;
			select_icon.SetActive(false);
			}
		else //select this
			{
			deck.this_deck.Deselect_all_bonus_buttons();
			deck.this_deck.current_bonus_selected = (deck.bonus_type)my_id;


			if (deck.this_deck.current_bonus_selected == deck.bonus_type.destroy_some_card)
				{
				deck.this_deck.undo_previous_score = 0;
				Consume_a_charge();
				deck.this_deck.Destroy_n_casual_cards(deck.this_deck.destroy_some_card_quantity);
				}
			else if (deck.this_deck.current_bonus_selected == deck.bonus_type.shuffle)
				{
				deck.this_deck.undo_previous_score = 0;
				Consume_a_charge();
				StartCoroutine(deck.this_deck.Shuffle_board());
				}
			else 
				{
				select_icon.SetActive(true);
				}
			}

		//Debug.Log("bonus selected: " + deck.this_deck.current_bonus_selected);
	}

	public void Consume_a_charge()
	{
		deck.this_deck.current_bonus_quantity [my_id]--;

		deck.this_deck.previous_bonus_selected = deck.this_deck.current_bonus_selected;
		deck.this_deck.current_bonus_selected = deck.bonus_type.none;

		deck.this_deck.current_last_move = deck.last_move.use_bonus;
		deck.this_deck.Update_undo_button (deck.this_deck.this_bonus_allows_undo[my_id]);

		deck.this_deck.Refresh_bonus_buttons();
	}

	public void Add_a_charge()
	{
        if (deck.this_deck.current_bonus_quantity[my_id] < deck.this_deck.max_bonus_quantity[my_id])
        {
            deck.this_deck.current_bonus_quantity [my_id]++;
		    deck.this_deck.Refresh_bonus_buttons ();
        }
    }
}
