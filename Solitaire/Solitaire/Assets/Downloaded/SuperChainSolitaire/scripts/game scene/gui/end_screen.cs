﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class end_screen : MonoBehaviour {

	public Text end_screen_name;
	//elements
		//recap boards
		public Transform recap_list;
		int recap_list_length;
		public GameObject recap_element_prefab;
		recap_element[] recap_element_script;
		//3 star score
		public GameObject tree_star_element;
			public Toggle[] stars;
			public GameObject perfect;
		//card count
		public GameObject card_count;
			public Text card_left;
			public Text card_saved;
		//combo count
		public GameObject combo_count;
			public Text best_combo;
			public Text combo_sum;
		//score count
		public GameObject score_count;
			public Text best_multiplier;
			public Text stage_score;

	public Text final_score;

	//buttons
	public GameObject continue_button;
	public GameObject quit_button;
	
	public void Setup_recap_list(bool show)
	{
		recap_list.gameObject.SetActive(show);

	}

	public void Setup_recap_list(int length)
	{
		//Debug.Log ("Setup_recap_list" + length);
		recap_list_length = length;
		recap_list.gameObject.SetActive(true);
		recap_element_script = new recap_element[length];
		for (int i = 0; i < length; i++)
		{
			GameObject temp = Instantiate(recap_element_prefab);
			temp.transform.SetParent(recap_list,false);
			recap_element_script[i] = temp.GetComponent<recap_element>();
			recap_element_script[i].my_number.text = (i+1).ToString();
			recap_element_script[i].Star_off();
			recap_element_script[i].no_stars.text = "";
		}
	}

	public void Update_recap_list(int current_board)
	{
		bool done = false;
		for (int i = 0; i < recap_list_length; i++)
		{
			if (i <= current_board)
				done = true;
			else
				done = false;

			recap_element_script[i].Done(done,deck.this_deck.all_cards_collected_in_this_board[i]);

			if (deck.this_deck.star_score_selected != deck.star_score.no)
				{
				if (done)
					recap_element_script[i].Star_on(deck.this_deck.current_star_score[i]);
				}

		}
	}

	public void Show_end_screen(bool show)
	{
		this.gameObject.SetActive(show);
	}

	public void Show_final_score(int _cards_left, int _cards_saved,
	                             int _best_combo, int _total_combo_sum,
	                             float _best_multiplier, int _score,
	                             int _total_score)
	{

		//card count
		card_count.SetActive (true);
		card_left.text = "Card Left: " + _cards_left;
		card_saved.text = "Card Saved: " + (_cards_saved-1).ToString();
		if (deck.this_deck.score_for_each_deck_card_spared > 0 && (_cards_saved-1) > 0)
			card_saved.text += " x" + deck.this_deck.score_for_each_deck_card_spared.ToString ("N0");
		//combo count
		combo_count.SetActive (true);
		best_combo.text = "Best Combo: " + _best_combo;
		combo_sum.text = "Total Combo Sum: " + _total_combo_sum;
		//score count
		score_count.SetActive (true);
		best_multiplier.text = "Best Multiplier: " + _best_multiplier;
		stage_score.text = "Score: " + _score.ToString("N0");
		
		final_score.text = "Total: " + _total_score.ToString("N0");
	}

	public void End_screen_type(bool win, bool recap)
	{
		tree_star_element.SetActive (false);
		continue_button.SetActive(true);
		quit_button.SetActive(false);
		string my_title = "";

		if (recap)
			my_title = "Recap";
		else
			{
			if (!win)
				{
				my_title = "Lose !";
				continue_button.SetActive(false);
				quit_button.SetActive(true);
				}
			}

		if (win)
			{
			my_title += " - WIN ! - ";
			if (deck.this_deck.star_score_selected != deck.star_score.no && deck.this_deck.win_condition_must_be_reach_selected == deck.win_condition_must_be_reach.independently_in_each_board)//show star score
				{
				bool show_perfect = false;
                for (int i = 0; i < stars.Length; i++)
					{
					if (deck.this_deck.current_star_score[deck.this_deck.current_board] > i)
						{
						stars[i].isOn = true;
						if (deck.this_deck.current_star_score[deck.this_deck.current_board] == 3)
							show_perfect = true;
						}
					else
						stars[i].isOn = false;
					}

				Debug.Log("show_perfect " + show_perfect);
				perfect.SetActive (show_perfect);

				tree_star_element.SetActive (true);
				}
			}
		end_screen_name.text = my_title;
	}
}
