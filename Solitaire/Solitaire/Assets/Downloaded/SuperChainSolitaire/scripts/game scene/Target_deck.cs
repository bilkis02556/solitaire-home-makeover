﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Target_deck : MonoBehaviour {

    public SpriteRenderer selected;

    SpriteRenderer my_face;
    Transform my_transform;
    bool select;
    [HideInInspector]
    public List<deck.card_class> garbage_cards;

    // Use this for initialization
    void Start () {
        selected.enabled = false;
        selected.sprite = deck.this_deck.card_selected_sprite;
        my_face = GetComponent<SpriteRenderer>();
        my_transform = GetComponent<Transform>();
        garbage_cards = new List<deck.card_class>();
    }
	
	// Update is called once per frame
	void OnMouseDown(){

        if (deck.this_deck.main_rule_selected == deck.main_rule.chain || !deck.this_deck.player_can_move)
            return;



        if (deck.this_deck.main_rule_selected == deck.main_rule.sum13 && (deck.this_deck.current_deck_card.rank + 1) == 13)
            {
                deck.this_deck.selected_card = new deck.card_class();
                DestroyMe();
                return;
            }

            //if no card selected, select me
            if (deck.this_deck.selected_card.rank == -99)
                {
                    SelectMe(true);
                }
            else //compare me with the selected card
                {

                    if (select)//if you click two times on the same card -> deselect me
                    {
                        SelectMe(false);
                        return;
                    }

                    if (CheckJoker() || CheckMainRule())
                    {
                        print("ok");
                        //destroy me and the selected_card
                        deck.this_deck.DestroyThisMatch(deck.this_deck.selected_card, deck.this_deck.current_deck_card);
                    }
                    else
                        Wrong_card();

                }
            

        


    }

    bool CheckMainRule()
    {
        if (deck.this_deck.main_rule_selected == deck.main_rule.sum13)
        {
            int sum = deck.this_deck.selected_card.rank + 1 + deck.this_deck.current_deck_card.rank + 1;
            print("sum: " + sum);
            if (sum == 13)
                return true;
        }
        else if (deck.this_deck.main_rule_selected == deck.main_rule.matchSameRank)
        {
            if (deck.this_deck.selected_card.rank == deck.this_deck.current_deck_card.rank)
                return true;
        }

        return false;
    }

    bool CheckJoker()
    {
        if (deck.this_deck.selected_card.joker_type_selected == deck.card_class.joker_type.none)
            return false;

         if (deck.this_deck.selected_card.joker_type_selected == deck.card_class.joker_type.normal)//green joker
            return true;

        if (deck.this_deck.selected_card.joker_type_selected == deck.card_class.joker_type.red //red joker
                && (deck.this_deck.current_deck_card.suit == 2 || deck.this_deck.current_deck_card.suit == 3)) //this card is red
            return true;

        if (deck.this_deck.selected_card.joker_type_selected == deck.card_class.joker_type.black//black joker
                 && (deck.this_deck.current_deck_card.suit == 0 || deck.this_deck.current_deck_card.suit == 1))//this card is black
            return true;

        return false;
    }

    public void SelectMe(bool _select)
    {
        if(_select)
            deck.this_deck.selected_card = deck.this_deck.current_deck_card;
        else
            deck.this_deck.selected_card = new deck.card_class();

        select = _select;
        selected.enabled = _select;
    }

    public void DestroyMe()
    {
        print("destroy current deck card");

        SelectMe(false);

        //setup the ghost card to play the destroy animation
        deck.this_deck.ghost_card_SpriteRenderer.sprite = my_face.sprite;
        deck.this_deck.ghost_card.position = my_transform.position;
        deck.this_deck.ghost_card.rotation = my_transform.rotation;
        deck.this_deck.ghost_card.gameObject.SetActive(true);

        garbage_cards.Add(deck.this_deck.target_deck_cards[deck.this_deck.target_deck_cards.Count - 1]);
        deck.this_deck.target_deck_cards.RemoveAt(deck.this_deck.target_deck_cards.Count - 1);
        if (deck.this_deck.target_deck_cards.Count == 0)
            my_face.enabled = false;
        else
            my_face.sprite = deck.this_deck.Assign_the_sprite_of_this_card(deck.this_deck.target_deck_cards[deck.this_deck.target_deck_cards.Count - 1]);

        StartCoroutine(Destroy_this_card());

    }


    IEnumerator Destroy_this_card()
    {
        deck.this_deck.player_can_move = false;

        deck.this_deck.Play_sfx(deck.this_deck.destroy_this_card_sfx);

        //animation
        float duration = 0.5f;
        float time = 0;
        while (time < 1)
        {
            time += Time.smoothDeltaTime / duration;
            deck.this_deck.ghost_card.Rotate(Vector3.forward * Time.deltaTime * 1000);
            deck.this_deck.ghost_card.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, time);
            yield return null;
        }

        deck.this_deck.ghost_card.gameObject.SetActive(false);
        deck.this_deck.ghost_card.localScale = Vector3.one;

        if (deck.this_deck.selected_card.rank >= 10 && deck.this_deck.selected_card.rank <= 12)//if this is a royal card
            deck.this_deck.Damage_all_free_royal_padlocks();

        deck.this_deck.player_can_move = true;
    }

    public IEnumerator Restore_latest_card()
    {
        //Debug.Log (" Restore_latest_card() ...garbage: " + garbage_cards.Count);
        deck.this_deck.player_can_move = false;

        //update current card
        deck.this_deck.target_deck_cards.Add(garbage_cards[garbage_cards.Count - 1]);
        garbage_cards.RemoveAt(garbage_cards.Count - 1);
        deck.this_deck.current_deck_card = deck.this_deck.target_deck_cards[deck.this_deck.target_deck_cards.Count - 1];

        //setup the ghost card to play the destroy animation
        deck.this_deck.ghost_card_SpriteRenderer.sprite = deck.this_deck.Assign_the_sprite_of_this_card(deck.this_deck.current_deck_card);
        deck.this_deck.ghost_card.position = my_transform.position;
        deck.this_deck.ghost_card.rotation = my_transform.rotation;
        deck.this_deck.ghost_card.gameObject.SetActive(true);

        //animation
        float duration = 0.5f;
            float time = 0;
            while (time < 1)
            {
                time += Time.smoothDeltaTime / duration;
                deck.this_deck.ghost_card.Rotate(Vector3.back * Time.deltaTime * 1000);
                deck.this_deck.ghost_card.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, time);
                yield return null;
            }

            if (deck.this_deck.current_deck_card.rank >= 10 && deck.this_deck.current_deck_card.rank <= 12)//if this is a royal card
                deck.this_deck.Repair_last_royal_padlocks_damaged();

            deck.this_deck.ghost_card.transform.rotation = my_transform.rotation;
            deck.this_deck.ghost_card.transform.localScale = my_transform.localScale;
            deck.this_deck.ghost_card.gameObject.SetActive(false);


            my_face.enabled = true;
            my_face.sprite = deck.this_deck.ghost_card_SpriteRenderer.sprite;



        deck.this_deck.player_can_move = true;
    }

    void Wrong_card()
    {
        print("current deck card is wrong");
        deck.this_deck.Play_sfx(deck.this_deck.wrong_card_sfx);

        StartCoroutine(deck.this_deck.Shake_card(this.transform));
        deck.this_deck.DeselectCard();
    }
}


