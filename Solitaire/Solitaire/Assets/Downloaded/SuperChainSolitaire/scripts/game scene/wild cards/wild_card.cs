﻿using UnityEngine;
using System.Collections;

public class wild_card : MonoBehaviour {

	public int my_array_position;
	public int my_rank;
	public deck.card_class.joker_type my_joker;
	public wild_card_deck my_wild_card_deck;

	void OnMouseDown()
	{
        if (deck.this_deck.game_end || !deck.this_deck.player_can_move)
            return;

        if (Input.touches.Length <= 1)//if the palyer click or tap on this card
		{
			deck.this_deck.undo_previous_score = 0;
			deck.this_deck.previous_bonus_selected = deck.bonus_type.none;
			deck.this_deck.Deselect_all_bonus_buttons();

			I_become_new_current_card();
		}
	}

	void I_become_new_current_card()
	{

		//setup undo
		deck.this_deck.current_last_move = deck.last_move.take_card_from_wild_deck;
		deck.this_deck.Update_undo_button (true);

		my_wild_card_deck.last_card_removed_position = my_array_position;//this is need to know the start point of the animation

        //put me in target deck
        /*
		deck.this_deck.New_current_deck_card(-99, my_rank, 
		                                 deck.card_class.card_type.wild, my_joker,
		                                 deck.last_move.take_card_from_wild_deck, false);*/
        deck.card_class tempCard = new deck.card_class();
        tempCard.suit = 100;
        tempCard.rank = my_rank;
        tempCard.card_type_selected = deck.card_class.card_type.wild;
        tempCard.joker_type_selected = my_joker;
        deck.this_deck.New_current_deck_card(tempCard,deck.last_move.take_card_from_wild_deck, false);

        //remove me from wild card deck
        my_wild_card_deck.Remove_card (my_array_position);


	}
}
