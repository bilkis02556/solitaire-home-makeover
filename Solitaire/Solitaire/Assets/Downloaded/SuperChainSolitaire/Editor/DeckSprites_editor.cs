﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

[CustomEditor(typeof(DeckSprites))]
public class DeckSprites_editor : Editor {


    Texture2D normal_atlas;
    Sprite[] normal_atlas_cards;
    Texture2D gold_atlas;
    Sprite[] gold_atlas_cards;

    string Sprite_name(int card_value)
    {
        string return_this = "";

        if (card_value == 0)
            return_this = "A";
        else if (card_value == 10)
            return_this = "J";
        else if (card_value == 11)
            return_this = "Q";
        else if (card_value == 12)
            return_this = "K";
        else
            return_this = (card_value + 1).ToString();

        return return_this;
    }

    public override void OnInspectorGUI()
    {
        DeckSprites my_target = (DeckSprites)target;
		EditorGUI.BeginChangeCheck ();
        Undo.RecordObject(my_target,"skin_edit");


        normal_atlas = EditorGUILayout.ObjectField("normalAtlas", normal_atlas, typeof(Texture2D), false) as Texture2D;
        gold_atlas = EditorGUILayout.ObjectField("goldAtlas", gold_atlas, typeof(Texture2D), false) as Texture2D;


        if (GUILayout.Button("extract sprites from normal atlas"))
        {
            //normal cards
            UnityEngine.Object[] atlas = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(normal_atlas));

            normal_atlas_cards = new Sprite[atlas.Length];
            for (int i = 0; i < normal_atlas_cards.Length; i++)
                normal_atlas_cards[i] = atlas[i] as Sprite;

            my_target.card_back_sprite = normal_atlas_cards[50];

            my_target.normal_joker = normal_atlas_cards[59];
            my_target.red_joker = normal_atlas_cards[57];
            my_target.black_joker = normal_atlas_cards[58];
            my_target.wild_joker = normal_atlas_cards[60];


            for (int i = 0; i < my_target.hearts.Length; i++)
            {
                if (i < 10)
                    {
                    my_target.hearts[i] = normal_atlas_cards[i + 1];
                    my_target.diamonds[i] = normal_atlas_cards[i + 11];
                    my_target.clubs[i] = normal_atlas_cards[i + 21];
                    my_target.spades[i] = normal_atlas_cards[i + 31];
                    
                    }
                else //figures
                    {
                    my_target.hearts[i] = normal_atlas_cards[i + 31];
                    my_target.diamonds[i] = normal_atlas_cards[i + 34];
                    my_target.clubs[i] = normal_atlas_cards[i + 37];
                    my_target.spades[i] = normal_atlas_cards[i + 41];
                    }
            }

            for (int i = 0; i < my_target.wild.Length; i++)
            {
                if (i < 10)
                    my_target.wild[i] = normal_atlas_cards[i + 61];
                else
                    my_target.wild[i] = normal_atlas_cards[i + 44];
            }
        }


        if (GUILayout.Button("extract sprites from gold atlas"))
        {
            //gold cards
            UnityEngine.Object[] atlas = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(gold_atlas));

            gold_atlas_cards = new Sprite[atlas.Length];
            for (int i = 0; i < gold_atlas_cards.Length; i++)
                gold_atlas_cards[i] = atlas[i] as Sprite;

            my_target.gold_card_back_sprite = gold_atlas_cards[50];

            my_target.gold_normal_joker = gold_atlas_cards[59];
            my_target.gold_red_joker = gold_atlas_cards[57];
            my_target.gold_black_joker = gold_atlas_cards[58];

            for (int i = 0; i < my_target.gold_clubs.Length; i++)
            {
                if (i < 10)
                {
                    my_target.gold_hearts[i] = gold_atlas_cards[i + 1];
                    my_target.gold_diamonds[i] = gold_atlas_cards[i + 11];
                    my_target.gold_clubs[i] = gold_atlas_cards[i + 21];
                    my_target.gold_spades[i] = gold_atlas_cards[i + 31];
                }
                else
                {
                    my_target.gold_hearts[i] = gold_atlas_cards[i + 31];
                    my_target.gold_diamonds[i] = gold_atlas_cards[i + 34];
                    my_target.gold_clubs[i] = gold_atlas_cards[i + 37];
                    my_target.gold_spades[i] = gold_atlas_cards[i + 41];
                }
            }

            //special
            my_target.padlock_sprite = gold_atlas_cards[54];
            my_target.royal_padlock_sprite = gold_atlas_cards[55];
            my_target.keyhole_padlock_sprite = gold_atlas_cards[56];

            //bonus
            my_target.bonus_cards_sprites[0] = gold_atlas_cards[63];
            my_target.bonus_cards_sprites[1] = gold_atlas_cards[62];
            my_target.bonus_cards_sprites[2] = gold_atlas_cards[61];
            my_target.bonus_cards_sprites[3] = gold_atlas_cards[64];


        }


        my_target.editor_show_sprites_normal = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal, "normal");
        if (my_target.editor_show_sprites_normal)
        {
            EditorGUI.indentLevel++;
            if (!my_target.card_back_sprite)
                GUI.color = Color.red;
            else
                GUI.color = Color.white;
            my_target.card_back_sprite = EditorGUILayout.ObjectField("back", my_target.card_back_sprite, typeof(Sprite), false) as Sprite;
            GUI.color = Color.white;

            my_target.editor_show_sprites_normal_clubs = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_clubs, "clubs");
            if (my_target.editor_show_sprites_normal_clubs)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.clubs[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.clubs[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.clubs[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            my_target.editor_show_sprites_normal_spades = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_spades, "spades");
            if (my_target.editor_show_sprites_normal_spades)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.spades[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.spades[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.spades[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            my_target.editor_show_sprites_normal_diamonds = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_diamonds, "diamonds");
            if (my_target.editor_show_sprites_normal_diamonds)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.diamonds[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.diamonds[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.diamonds[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            my_target.editor_show_sprites_normal_hearts = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_hearts, "hearts");
            if (my_target.editor_show_sprites_normal_hearts)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.hearts[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.hearts[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.hearts[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            //jokers
            my_target.normal_joker = EditorGUILayout.ObjectField("joker", my_target.normal_joker, typeof(Sprite), false) as Sprite;
            my_target.red_joker = EditorGUILayout.ObjectField("red joker", my_target.red_joker, typeof(Sprite), false) as Sprite;
            my_target.black_joker = EditorGUILayout.ObjectField("black joker", my_target.black_joker, typeof(Sprite), false) as Sprite;

            EditorGUI.indentLevel--;
        }

        my_target.editor_show_sprites_gold = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold, "gold");
        if (my_target.editor_show_sprites_gold)
        {
            EditorGUI.indentLevel++;
            if (!my_target.gold_card_back_sprite)
                GUI.color = Color.red;
            else
                GUI.color = Color.white;
            my_target.gold_card_back_sprite = EditorGUILayout.ObjectField("back", my_target.gold_card_back_sprite, typeof(Sprite), false) as Sprite;
            GUI.color = Color.white;

            my_target.editor_show_sprites_gold_clubs = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_clubs, "clubs");
            if (my_target.editor_show_sprites_gold_clubs)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.gold_clubs[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.gold_clubs[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.gold_clubs[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            my_target.editor_show_sprites_gold_spades = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_spades, "spades");
            if (my_target.editor_show_sprites_gold_spades)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.gold_spades[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.gold_spades[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.gold_spades[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            my_target.editor_show_sprites_gold_diamonds = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_diamonds, "diamonds");
            if (my_target.editor_show_sprites_gold_diamonds)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.gold_diamonds[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.gold_diamonds[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.gold_diamonds[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            my_target.editor_show_sprites_gold_hearts = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_hearts, "hearts");
            if (my_target.editor_show_sprites_gold_hearts)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < 13; i++)
                {
                    if (!my_target.gold_hearts[i])
                        GUI.color = Color.red;
                    else
                        GUI.color = Color.white;

                    my_target.gold_hearts[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.gold_hearts[i], typeof(Sprite), false) as Sprite;
                    GUI.color = Color.white;
                }
                EditorGUI.indentLevel--;
            }

            //jokers
            my_target.gold_normal_joker = EditorGUILayout.ObjectField("joker", my_target.gold_normal_joker, typeof(Sprite), false) as Sprite;
            my_target.gold_red_joker = EditorGUILayout.ObjectField("red joker", my_target.gold_red_joker, typeof(Sprite), false) as Sprite;
            my_target.gold_black_joker = EditorGUILayout.ObjectField("black joker", my_target.gold_black_joker, typeof(Sprite), false) as Sprite;

            EditorGUI.indentLevel--;
        }

        my_target.editor_show_sprites_wild = EditorGUILayout.Foldout(my_target.editor_show_sprites_wild, "wild");
        if (my_target.editor_show_sprites_wild)
        {
            EditorGUI.indentLevel++;
            for (int i = 0; i < 13; i++)
            {
                my_target.wild[i] = EditorGUILayout.ObjectField(Sprite_name(i), my_target.wild[i], typeof(Sprite), false) as Sprite;
            }

            my_target.wild_joker = EditorGUILayout.ObjectField("joker", my_target.wild_joker, typeof(Sprite), false) as Sprite;
            EditorGUI.indentLevel--;
        }

        my_target.editor_show_sprites_padlock = EditorGUILayout.Foldout(my_target.editor_show_sprites_padlock, "padlock");
        if (my_target.editor_show_sprites_padlock)
        {
            EditorGUI.indentLevel++;
            my_target.padlock_sprite = EditorGUILayout.ObjectField("padlock sprite", my_target.padlock_sprite, typeof(Sprite), false) as Sprite;
            my_target.royal_padlock_sprite = EditorGUILayout.ObjectField("royal padlock sprite", my_target.royal_padlock_sprite, typeof(Sprite), false) as Sprite;
            my_target.keyhole_padlock_sprite = EditorGUILayout.ObjectField("keyhole padlock sprite", my_target.keyhole_padlock_sprite, typeof(Sprite), false) as Sprite;
            EditorGUI.indentLevel--;
        }

        my_target.editor_show_sprites_bonus_cards = EditorGUILayout.Foldout(my_target.editor_show_sprites_bonus_cards, "bonus cards");
        if (my_target.editor_show_sprites_bonus_cards)
        {
            EditorGUI.indentLevel++;

            for (int i = 0; i < my_target.bonus_cards_sprites.Length; i++)
            {
                my_target.bonus_cards_sprites[i] = EditorGUILayout.ObjectField(Enum.GetName(typeof(deck.bonus_type), i).ToString() + " card[" + i + "]", my_target.bonus_cards_sprites[i], typeof(Sprite), true) as Sprite;
            }
            EditorGUI.indentLevel--;
        }



        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }


}
