﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[CustomEditor(typeof(UnityAds))]
internal class ads_editor : Editor
{

    public override void OnInspectorGUI()
    {
        Setup_ads();
        EditorGUILayout.Space();
        Settings();
        EditorGUILayout.Space();
        Sprites();
        EditorGUILayout.Space();
        Advanced();
    }

    void Setup_ads()
    {
        UnityAds my_target = (UnityAds)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "ads_edit_setup");

        my_target.editor_setup_ads = EditorGUILayout.Foldout(my_target.editor_setup_ads, "setup ads");
        if (my_target.editor_setup_ads)
        {
            EditorGUI.indentLevel++;
            my_target.rewardedVideoZone = EditorGUILayout.TextField(my_target.rewardedVideoZone, "rewardedVideoZone");
            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Settings()
    {
        UnityAds my_target = (UnityAds)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "ads_edit_settings");

        my_target.editor_settings = EditorGUILayout.Foldout(my_target.editor_settings, "settings");
        if (my_target.editor_settings)
        {
            EditorGUI.indentLevel++;


            my_target.ads_message = EditorGUILayout.TextField("ads_message", my_target.ads_message);
            my_target.reward_message = EditorGUILayout.TextField("reward_message", my_target.reward_message);

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Rules");
                EditorGUI.indentLevel++;

                my_target.chance_to_show_me = EditorGUILayout.IntSlider("chance_to_show_me", my_target.chance_to_show_me, 0, 100);
                my_target.minimum_time_interval_between_ads = EditorGUILayout.FloatField("minimum_time_interval_between_ads in seconds", my_target.minimum_time_interval_between_ads);

                my_target.bonusChances = EditorGUILayout.IntSlider("bonus chance " + my_target.bonusChances +"%"+ " ; wild chance " + (100 - my_target.bonusChances) + "%", my_target.bonusChances, 0, 100);
                EditorGUILayout.LabelField("bonus " + my_target.bonusChances + "%");
                if (my_target.bonusChances > 0)
                    {
                    EditorGUI.indentLevel++;
                    my_target.bonusChanceOfSuperQuantity = EditorGUILayout.IntSlider("super quantity" + my_target.bonusChanceOfSuperQuantity + "%" + " ; normal quantity " + (100 - my_target.bonusChanceOfSuperQuantity) + "%", my_target.bonusChanceOfSuperQuantity, 0, 100);
                        EditorGUI.indentLevel++;
                        my_target.bonusNormalMinQuantity = EditorGUILayout.IntField("normal min", my_target.bonusNormalMinQuantity);
                        my_target.bonusNormalMaxQuantity = EditorGUILayout.IntField("normal max", my_target.bonusNormalMaxQuantity);
                        my_target.bonusSuperMinQuantity = EditorGUILayout.IntField("super min", my_target.bonusSuperMinQuantity);
                        my_target.bonusSuperMaxQuantity = EditorGUILayout.IntField("super max", my_target.bonusSuperMaxQuantity);
                        EditorGUI.indentLevel--;
                    EditorGUILayout.LabelField("bonus chance weights:");
                        EditorGUI.indentLevel++;
                        int total_weight = 0;
                        //my_target.percentual_weight = new string[my_target.bonus_array_chances.Length];

                        for (int i = 0; i < my_target.bonus_array_chances.Length; i++)
                             {
                             my_target.bonus_array_chances[i] = EditorGUILayout.IntField(Enum.GetName(typeof(deck.bonus_type), i).ToString()
                                                                                        + my_target.percentual_weight[i]
                                                                                         , my_target.bonus_array_chances[i]);
                             total_weight += my_target.bonus_array_chances[i];
                             }
                        for (int i = 0; i < my_target.bonus_array_chances.Length; i++)
                            my_target.percentual_weight[i] = ( "["+((float)(my_target.bonus_array_chances[i]*100f)/total_weight).ToString()+"%]" );
                        EditorGUI.indentLevel--;
                    EditorGUI.indentLevel--;
                    }

            EditorGUILayout.LabelField("wild card " + (100 - my_target.bonusChances) + "%");
                if ((100 - my_target.bonusChances) > 0)
                 {
                    EditorGUI.indentLevel++;
                    my_target.wildcardChanceOfSuperQuantity = EditorGUILayout.IntSlider("super quantity" + my_target.wildcardChanceOfSuperQuantity + "%" + " ; normal quantity " + (100 - my_target.wildcardChanceOfSuperQuantity) + "%", my_target.wildcardChanceOfSuperQuantity, 0, 100);
                        EditorGUI.indentLevel++;
                        my_target.wildcardNormalMinQuantity = EditorGUILayout.IntField("normal min", my_target.wildcardNormalMinQuantity);
                        my_target.wildcardNormalMaxQuantity = EditorGUILayout.IntField("normal max", my_target.wildcardNormalMaxQuantity);
                        my_target.wildcardSuperMinQuantity = EditorGUILayout.IntField("super min", my_target.wildcardSuperMinQuantity);
                        my_target.wildcardSuperMaxQuantity = EditorGUILayout.IntField("super max", my_target.wildcardSuperMaxQuantity);
                        EditorGUI.indentLevel--;
                     my_target.wildcard_joker_chances = EditorGUILayout.Slider("joker chance", my_target.wildcard_joker_chances, 0, 7.14f);
                    EditorGUI.indentLevel--;
                   }

            EditorGUI.indentLevel--;

            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Sprites()
    {
        UnityAds my_target = (UnityAds)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "ads_edit_sprite");

        my_target.editor_sprites = EditorGUILayout.Foldout(my_target.editor_sprites, "sprites");
        if (my_target.editor_sprites)
        {
            EditorGUI.indentLevel++;
            my_target.giftBox = EditorGUILayout.ObjectField("giftBox", my_target.giftBox, typeof(Sprite), false) as Sprite;
            my_target.wildcardSprite = EditorGUILayout.ObjectField("wildcardSprite", my_target.wildcardSprite, typeof(Sprite), false) as Sprite;
            for (int i = 0; i < my_target.bonusSprite.Length; i++)
                my_target.bonusSprite[i] = EditorGUILayout.ObjectField(Enum.GetName(typeof(deck.bonus_type), i).ToString(), my_target.bonusSprite[i], typeof(Sprite), true) as Sprite;
            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Advanced()
    {
        UnityAds my_target = (UnityAds)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "ads_edit_advanced");

        my_target.editor_advanced = EditorGUILayout.Foldout(my_target.editor_advanced, "advanced");
        if (my_target.editor_advanced)
        {
            EditorGUI.indentLevel++;
            my_target.myCanvas = EditorGUILayout.ObjectField("myCanvas", my_target.myCanvas, typeof(GameObject), true) as GameObject;
            my_target.WindowMessage = EditorGUILayout.ObjectField("WindowMessage", my_target.WindowMessage, typeof(Text), true) as Text;
            my_target.quantityText = EditorGUILayout.ObjectField("quantityText", my_target.quantityText, typeof(Text), true) as Text;
            my_target.myIcon = EditorGUILayout.ObjectField("myIcon", my_target.myIcon, typeof(Image), true) as Image;

            my_target.okButton = EditorGUILayout.ObjectField("okButton", my_target.okButton, typeof(GameObject), true) as GameObject;
            my_target.noButton = EditorGUILayout.ObjectField("noButton", my_target.noButton, typeof(GameObject), true) as GameObject;
            my_target.continueButton = EditorGUILayout.ObjectField("continueButton", my_target.continueButton, typeof(GameObject), true) as GameObject;
            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }


}
