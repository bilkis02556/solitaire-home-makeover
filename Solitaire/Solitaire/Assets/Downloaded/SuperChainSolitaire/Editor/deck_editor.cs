﻿using UnityEngine.UI;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

[CustomEditor(typeof(deck))]
internal class deck_editor : Editor {

	int temp_board_array_length = 2;
	int number_of_bonus = Enum.GetValues (typeof(deck.bonus_type)).Length;


	public override void OnInspectorGUI()
	{
        EditorGUILayout.LabelField("Rules:");
        EditorGUI.indentLevel++;
            General();
            Bonus();
		    Score();
            Combo();
            Special_cards();
        EditorGUI.indentLevel--;

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Settings:");
        EditorGUI.indentLevel++;
            Animations();
            Sprites();
            Particles();
            Sfx();
		    Advanced();
        EditorGUI.indentLevel--;
    }

	void General()
	{
		deck my_target = (deck)target;
		EditorGUI.BeginChangeCheck ();
        Undo.RecordObject(my_target, "deck_edit_general");

        my_target.editor_show_general_rules = EditorGUILayout.Foldout(my_target.editor_show_general_rules, "General");
        if (my_target.editor_show_general_rules)
        {

            my_target.main_rule_selected = (deck.main_rule)EditorGUILayout.EnumPopup("Main Rule", my_target.main_rule_selected);
            EditorGUILayout.Space();

            my_target.boards_to_use_selected = (deck.boards_to_use)EditorGUILayout.EnumPopup("boards to use",my_target.boards_to_use_selected);
			if (my_target.boards_to_use_selected == deck.boards_to_use.multiple) 
			{
			EditorGUI.indentLevel++;
			if (my_target.board_array_length < 2) 
				{
				my_target.board_array_length = 2;
				my_target.board_array = new GameObject[my_target.board_array_length];
				}
				
			EditorGUILayout.BeginHorizontal ();
			if (temp_board_array_length < 2)
				GUI.color = Color.red;
			else
				GUI.color = Color.white;
			temp_board_array_length = EditorGUILayout.IntField ("array length", temp_board_array_length);
			GUI.color = Color.white;

			if (GUILayout.Button ("confirm")) 
				{
				my_target.board_array_length = temp_board_array_length;

				if (my_target.board_array_length < 2)
					my_target.board_array_length = 2;

				my_target.board_array = new GameObject[my_target.board_array_length];
				}
			EditorGUILayout.EndHorizontal ();

			EditorGUI.indentLevel++;
			my_target.editor_show_board_array = EditorGUILayout.Foldout (my_target.editor_show_board_array, "show array");
			if (my_target.editor_show_board_array) 
				{
				for (int i = 0; i < my_target.board_array_length; i++) 
					{
					if (my_target.board_array [i] == null)
						GUI.color = Color.red;
					else
						GUI.color = Color.white;

					my_target.board_array [i] = EditorGUILayout.ObjectField ("board " + i, my_target.board_array [i], typeof(GameObject), true) as GameObject;

					GUI.color = Color.white;
					}
				}
			EditorGUI.indentLevel--;

			EditorGUI.indentLevel--;
			} 
		else
			my_target.win_condition_must_be_reach_selected = deck.win_condition_must_be_reach.independently_in_each_board;

			EditorGUILayout.Space ();

			EditorGUILayout.LabelField ("Win rules:");
			EditorGUI.indentLevel++;
			my_target.show_goals_window_at_start_selected = (deck.show_goals_window_at_start)EditorGUILayout.EnumPopup("Show goals window at start" ,my_target.show_goals_window_at_start_selected );
				EditorGUI.indentLevel++;
				if (my_target.show_goals_window_at_start_selected != deck.show_goals_window_at_start.no)
					my_target.show_goal_window_at_each_win_condition_satisfied = EditorGUILayout.Toggle ("show goal window at each win condition satisfied", my_target.show_goal_window_at_each_win_condition_satisfied);
				EditorGUI.indentLevel--;

			if (my_target.star_score_selected == deck.star_score.gain_one_star_for_each_win_condition_satisfied && my_target.number_of_win_conditions != 3)
			GUI.color = Color.red;
			my_target.number_of_win_conditions = EditorGUILayout.IntSlider("number of win conditions",my_target.number_of_win_conditions, 1 , Enum.GetNames(typeof(deck.win_condition)).Length);
			GUI.color = Color.white;

			if (my_target.win_condition_selected.Length != Enum.GetNames(typeof(deck.win_condition)).Length)
				my_target.win_condition_selected = new deck.win_condition[my_target.number_of_win_conditions];

			EditorGUI.indentLevel++;
			for (int i = 0; i < my_target.number_of_win_conditions; i++)
				{
				if (my_target.boards_to_use_selected == deck.boards_to_use.current_only 
			    && (my_target.win_condition_selected[i] == deck.win_condition.collect_all_cards_in_n_boards || my_target.win_condition_selected[i] == deck.win_condition.collect_all_gold_cards_in_n_boards))
			   	 	{
					GUI.color = Color.red;
					EditorGUILayout.LabelField ("WARNING! This win condition work only with - Board to use = Multiple !");
					}
			    my_target.win_condition_selected[i] = (deck.win_condition)EditorGUILayout.EnumPopup("win condition " + (i+1).ToString(),my_target.win_condition_selected[i]);
				GUI.color = Color.white;

				EditorGUI.indentLevel++;
				switch(my_target.win_condition_selected[i])
					{
					case deck.win_condition.collect_all_cards_in_n_boards:

						my_target.collect_all_cards_in_n_boards_target = EditorGUILayout.IntSlider("how many boards", my_target.collect_all_cards_in_n_boards_target,1,my_target.board_array_length);
					
					break;

					case deck.win_condition.collect_n_gold_cards:
					if (my_target.collect_n_gold_cards_target <= 0)
							GUI.color = Color.red;
						
						my_target.collect_n_gold_cards_target = EditorGUILayout.IntField("how many gold cards", my_target.collect_n_gold_cards_target);
						GUI.color = Color.white;
						
						break;

					case deck.win_condition.collect_all_gold_cards_in_n_boards:
				
						my_target.collect_all_gold_cards_in_n_boards_target = EditorGUILayout.IntSlider("how many boards", my_target.collect_all_gold_cards_in_n_boards_target,1,my_target.board_array_length);
				
					break;

					case deck.win_condition.reach_target_score:
						if (my_target.win_target_score <= 0)
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						my_target.win_target_score = EditorGUILayout.IntField("target score", my_target.win_target_score);
						GUI.color = Color.white;
					break;

					case deck.win_condition.reach_this_combo_multiplier:
						if (my_target.reach_this_combo_multiplier_target <= 0)
							GUI.color = Color.red;

						my_target.reach_this_combo_multiplier_target = EditorGUILayout.FloatField("target combo multiplier", my_target.reach_this_combo_multiplier_target);
						GUI.color = Color.white;

					break;

					case deck.win_condition.reach_this_combo_length:
						if (my_target.reach_this_combo_length_target <= 0)
							GUI.color = Color.red;
						
						my_target.reach_this_combo_length_target = EditorGUILayout.IntField("target combo length", my_target.reach_this_combo_length_target);
						GUI.color = Color.white;
						
					break;

					case deck.win_condition.reach_this_combo_sum:
					if (my_target.reach_this_combo_sum_target <= 0)
						GUI.color = Color.red;
					
					my_target.reach_this_combo_sum_target = EditorGUILayout.IntField("target combo sum", my_target.reach_this_combo_sum_target);
					GUI.color = Color.white;
					break;

					case deck.win_condition.reach_this_star_score_sum:

					if (my_target.target_star_score_sum <= 0)
						GUI.color = Color.red;
				
					my_target.target_star_score_sum = EditorGUILayout.IntField("target star score sum", my_target.target_star_score_sum);
					GUI.color = Color.white;
					
					my_target.boards_to_use_selected = deck.boards_to_use.multiple;

					if (my_target.star_score_selected == deck.star_score.no || my_target.star_score_selected == deck.star_score.gain_one_star_for_each_win_condition_satisfied)
					EditorGUILayout.LabelField("WARNING: Star score can't be: -no- or -gain_one_star_for_each_win_condition_satisfied-");

					break;
					}
				EditorGUI.indentLevel--;
				}
			EditorGUI.indentLevel--;

			my_target.play_until_no_more_cards_then_check_win_condition = EditorGUILayout.Toggle("play until no more cards, then check win condition", my_target.play_until_no_more_cards_then_check_win_condition);

			if (my_target.boards_to_use_selected == deck.boards_to_use.multiple)
				{
				bool incompatibility = false;

				if(my_target.win_condition_must_be_reach_selected == deck.win_condition_must_be_reach.independently_in_each_board)
					{
					for (int i = 0; i < my_target.number_of_win_conditions; i++)
						{
						if ( my_target.win_condition_selected[i] == deck.win_condition.collect_all_cards_in_n_boards ||  my_target.win_condition_selected[i] == deck.win_condition.collect_all_gold_cards_in_n_boards ||  my_target.win_condition_selected[i] == deck.win_condition.reach_this_star_score_sum)
							incompatibility = true;
						}
					}
				if (incompatibility)
					{
					GUI.color = Color.red;
					EditorGUILayout.LabelField("WARNING: You can't use independently_in_each_board with win codition: collect_all_cards_in_n_boards or collect_all_gold_cards_in_n_boards");
					}
				else
					GUI.color = Color.white;

				my_target.win_condition_must_be_reach_selected = (deck.win_condition_must_be_reach)EditorGUILayout.EnumPopup ("win condition must be reach", my_target.win_condition_must_be_reach_selected);
				GUI.color = Color.white;
				}
			else
				my_target.win_condition_must_be_reach_selected = deck.win_condition_must_be_reach.independently_in_each_board;

			if (my_target.number_of_win_conditions == 1)
				my_target.win_conditions_indispensable_selected = deck.win_conditions_indispensable.all;
			else
				my_target.win_conditions_indispensable_selected = (deck.win_conditions_indispensable)EditorGUILayout.EnumPopup ("win conditions indispensable", my_target.win_conditions_indispensable_selected);

			EditorGUI.indentLevel--;


			EditorGUILayout.LabelField("Deck");
			EditorGUI.indentLevel++;
				my_target.deck_card_left = EditorGUILayout.IntField("total cards", my_target.deck_card_left);
                EditorGUILayout.LabelField("preferences:");
                EditorGUI.indentLevel++;
                my_target.give_an_useful_card_not_less_than_n_new_card_from_the_deck = EditorGUILayout.IntField("max number of useless card to discover before find a good card", my_target.give_an_useful_card_not_less_than_n_new_card_from_the_deck);
                my_target.first_card_is_always_useful = EditorGUILayout.Toggle("first card is always useful", my_target.first_card_is_always_useful);
                my_target.never_give_same_card_rank_twice_in_a_row = EditorGUILayout.Toggle("never give same card rank twice in a row", my_target.never_give_same_card_rank_twice_in_a_row);
                my_target.allowCheatWhenGiveANewCard = EditorGUILayout.Toggle("Deck can cheat to fulfill these preferences above", my_target.allowCheatWhenGiveANewCard);
                EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;


			my_target.undo_charges = EditorGUILayout.IntField("undo charges", my_target.undo_charges);
            EditorGUI.indentLevel++;
                my_target.restore_undo_charges_when_retry = EditorGUILayout.Toggle("restore undo charges when retry", my_target.restore_undo_charges_when_retry);
                if (my_target.boards_to_use_selected == deck.boards_to_use.multiple)
                    my_target.restore_undo_charges_when_go_to_next_board = EditorGUILayout.Toggle("restore undo charges when go to next board", my_target.restore_undo_charges_when_go_to_next_board);
            EditorGUI.indentLevel--;

            my_target.distribute_card_metod_for_turn_up_a_covered_card = (deck.distribute_card_metod) EditorGUILayout.EnumPopup ("turn up a covered card at start", my_target.distribute_card_metod_for_turn_up_a_covered_card);
				EditorGUI.indentLevel++;
				if (my_target.distribute_card_metod_for_turn_up_a_covered_card == deck.distribute_card_metod.random_roll_for_each_card)
					my_target.chance_of_turn_up_a_covered_card_at_start = EditorGUILayout.IntSlider("chance",my_target.chance_of_turn_up_a_covered_card_at_start, 0 , 100);
				else if(my_target.distribute_card_metod_for_turn_up_a_covered_card == deck.distribute_card_metod.as_percentage_on_total_cards)
					my_target.percentage_of_turn_up_a_covered_card_at_start = EditorGUILayout.IntSlider("percentage",my_target.percentage_of_turn_up_a_covered_card_at_start, 0 , 100);
				EditorGUI.indentLevel--;

            my_target.useMinimumAmountOfDecksToFillBoardAndBottomDeck = EditorGUILayout.Toggle("Use the minimum amount of decks To fill board and BottomDeck", my_target.useMinimumAmountOfDecksToFillBoardAndBottomDeck);
        }

        if (EditorGUI.EndChangeCheck ())
			EditorUtility.SetDirty(my_target);

	}

    void Bonus()
    {
        deck my_target = (deck)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "deck_edit_bonus");

        my_target.editor_show_bonus_rules = EditorGUILayout.Foldout(my_target.editor_show_bonus_rules, "Bonus");
        if (my_target.editor_show_bonus_rules)
        {
            EditorGUI.indentLevel++;

            if (my_target.current_bonus_quantity.Length != number_of_bonus - 1)
                my_target.current_bonus_quantity = new int[number_of_bonus - 1];

            if (my_target.max_bonus_quantity.Length != number_of_bonus - 1)
                my_target.max_bonus_quantity = new int[number_of_bonus - 1];


            if (my_target.this_bonus_allows_undo.Length != number_of_bonus - 1)
                my_target.this_bonus_allows_undo = new bool[number_of_bonus - 1];

            for (int i = 0; i < number_of_bonus - 1; i++)
            {
                EditorGUILayout.LabelField(Enum.GetName(typeof(deck.bonus_type), i).ToString());

                EditorGUI.indentLevel++;
                if (my_target.current_bonus_quantity[i] < 0)
                    GUI.color = Color.red;

                if (my_target.current_bonus_quantity[i] > my_target.max_bonus_quantity[i])
                    my_target.current_bonus_quantity[i] = my_target.max_bonus_quantity[i];

                my_target.current_bonus_quantity[i] = EditorGUILayout.IntField("start quantity", my_target.current_bonus_quantity[i]);
                GUI.color = Color.white;

                if (my_target.max_bonus_quantity[i] < 0)
                    GUI.color = Color.red;

                my_target.max_bonus_quantity[i] = EditorGUILayout.IntField("max quantity", my_target.max_bonus_quantity[i]);
                GUI.color = Color.white;

                my_target.this_bonus_allows_undo[i] = EditorGUILayout.Toggle("can undo", my_target.this_bonus_allows_undo[i]);

                if (i == 1)//destroy_some_card
                    my_target.destroy_some_card_quantity = EditorGUILayout.IntSlider("power", my_target.destroy_some_card_quantity, 1, 10);
                else if (i == 3)//shuffle
                {
                    EditorGUILayout.LabelField("can shuffle...");
                    EditorGUI.indentLevel++;
                    my_target.can_shuffle_normal_padlock = EditorGUILayout.Toggle("normal padlock", my_target.can_shuffle_normal_padlock);
                    my_target.can_shuffle_royal_padlock = EditorGUILayout.Toggle("royal padlock", my_target.can_shuffle_royal_padlock);
                    my_target.can_shuffle_keyhole_padlock = EditorGUILayout.Toggle("keyhole padlock", my_target.can_shuffle_keyhole_padlock);
                    my_target.can_shuffle_gold_card = EditorGUILayout.Toggle("gold card", my_target.can_shuffle_gold_card);
                    my_target.can_shuffle_wild_card = EditorGUILayout.Toggle("wild card", my_target.can_shuffle_wild_card);
                    my_target.can_shuffle_collectable_card = EditorGUILayout.Toggle("collectable card", my_target.can_shuffle_collectable_card);
                    EditorGUI.indentLevel--;
                }
                EditorGUI.indentLevel--;
            }
            my_target.restore_bonus_quantity_at_restart = EditorGUILayout.Toggle("restore bonus quantity at restart", my_target.restore_bonus_quantity_at_restart);

            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Score()
	{
		deck my_target = (deck)target;
		EditorGUI.BeginChangeCheck ();
        Undo.RecordObject(my_target, "deck_edit_score");

        my_target.editor_show_score_rules = EditorGUILayout.Foldout(my_target.editor_show_score_rules, "Score");
        if (my_target.editor_show_score_rules)
        {
            EditorGUI.indentLevel++;
			my_target.normal_card_score = EditorGUILayout.IntField("normal card", my_target.normal_card_score);
			my_target.gold_card_score = EditorGUILayout.IntField("gold card", my_target.gold_card_score);
			my_target.bottom_card_score = EditorGUILayout.IntField("bottom card", my_target.bottom_card_score);		
			my_target.score_for_each_deck_card_spared = EditorGUILayout.IntField("for each deck card spared", my_target.score_for_each_deck_card_spared);
            my_target.target_deck_card_score = EditorGUILayout.IntField("match a card in target deck", my_target.target_deck_card_score);

            my_target.star_score_selected = (deck.star_score)EditorGUILayout.EnumPopup ("Star score", my_target.star_score_selected);
			EditorGUI.indentLevel++;

			if (my_target.star_score_selected == deck.star_score.gain_one_star_for_each_score_steps_achieved)
				{
				if (my_target.star_score_steps.Length != 3)
					my_target.star_score_steps = new int[3];
					EditorGUILayout.LabelField("1 star = target score in win condition (" + my_target.win_target_score + ")");
					for (int i = 0; i < my_target.star_score_steps.Length; i++)
						{
						if (i == 0)
							my_target.star_score_steps[i] = my_target.win_target_score;
						else
							{
							if (my_target.star_score_steps[i] <= my_target.star_score_steps[i-1])
								GUI.color = Color.red;

							my_target.star_score_steps[i] = EditorGUILayout.IntField((i+1) + " star = ", my_target.star_score_steps[i]);

							GUI.color = Color.white;
							}
							
						}
				}
		else if (my_target.star_score_selected == deck.star_score.less_card_on_board_more_stars_you_gain)
			{
			if (my_target.card_left_on_board_to_gain_n_stars.Length != 3)
				my_target.card_left_on_board_to_gain_n_stars = new int[3];

			bool show_card_left_on_board_to_gain_n_stars_warning = false;

			for (int i = 0; i < my_target.card_left_on_board_to_gain_n_stars.Length; i++)
				{
				if ((i+1) < my_target.card_left_on_board_to_gain_n_stars.Length) 
					{
				    if (my_target.card_left_on_board_to_gain_n_stars[i] >= my_target.card_left_on_board_to_gain_n_stars[i+1])
						show_card_left_on_board_to_gain_n_stars_warning = true;
					}

				my_target.card_left_on_board_to_gain_n_stars[i] = EditorGUILayout.IntField("Gain " + (3-i) + " stars if cards left on board =", my_target.card_left_on_board_to_gain_n_stars[i]);
				}
			if (show_card_left_on_board_to_gain_n_stars_warning)					
				EditorGUILayout.LabelField ("WARNING! these values MUST be progressive");
			}
			EditorGUI.indentLevel--;
		EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck ())
			EditorUtility.SetDirty(my_target);

	}


    void Combo()
    {
        deck my_target = (deck)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "deck_edit_combo");

        my_target.editor_show_combo_rules = EditorGUILayout.Foldout(my_target.editor_show_combo_rules, "Combo");
        if (my_target.editor_show_combo_rules)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("sum rules:");
            EditorGUI.indentLevel++;
            my_target.min_combo_lenght_to_trigger_the_sum = EditorGUILayout.IntSlider("min combo lenght to trigger the sum", my_target.min_combo_lenght_to_trigger_the_sum, 1, 10);
            my_target.combo_sum_target_to_trigger_the_score_bonus = EditorGUILayout.IntSlider("target sum", my_target.combo_sum_target_to_trigger_the_score_bonus, 1, 100);
            EditorGUI.indentLevel++;
            my_target.give_this_score_bonus_when_reach_the_combo_sum = EditorGUILayout.IntField("score bonus to give when reach the target sum", my_target.give_this_score_bonus_when_reach_the_combo_sum);
            EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("multiplier rules:");
            EditorGUI.indentLevel++;
            my_target.combo_score_multiplier = EditorGUILayout.FloatField("normal multiplier", my_target.combo_score_multiplier);
            EditorGUI.indentLevel++;
            my_target.min_combo_lenght_to_trigger_the_multiplier = EditorGUILayout.IntSlider("min combo lenght to trigger the multiplier", my_target.min_combo_lenght_to_trigger_the_multiplier, 1, 10);
            EditorGUI.indentLevel--;
            my_target.color_combo_score_multiplier = EditorGUILayout.FloatField("same color multiplier", my_target.color_combo_score_multiplier);
            my_target.suit_combo_score_multiplier = EditorGUILayout.FloatField("same suit multiplier", my_target.suit_combo_score_multiplier);
            my_target.keep_best_combo_multiplier_selected = (deck.keep_best_combo_multiplier)EditorGUILayout.EnumPopup("keep best combo multiplier", my_target.keep_best_combo_multiplier_selected);
            EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Special_cards()
    {
        deck my_target = (deck)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "deck_edit_special_cards");


        my_target.editor_show_wild_cards_rules = EditorGUILayout.Foldout(my_target.editor_show_wild_cards_rules, "Wild card");
        if (my_target.editor_show_wild_cards_rules)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("wild deck settings");
            EditorGUI.indentLevel++;

            if (my_target.max_wild_cards_in_wild_deck < 0)
                GUI.color = Color.red;
            else
                GUI.color = Color.white;
            my_target.max_wild_cards_in_wild_deck = EditorGUILayout.IntField("max wild cars", my_target.max_wild_cards_in_wild_deck);
            GUI.color = Color.white;

            EditorGUILayout.LabelField("spacing");
            EditorGUI.indentLevel++;
            my_target.wild_deck_spacing_x = EditorGUILayout.FloatField("x", my_target.wild_deck_spacing_x);
            my_target.wild_deck_spacing_y = EditorGUILayout.FloatField("y", my_target.wild_deck_spacing_y);
            EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;

            EditorGUILayout.LabelField("rules");
            EditorGUI.indentLevel++;
            my_target.distribute_card_metod_for_wild_card_on_board = (deck.distribute_card_metod)EditorGUILayout.EnumPopup("on board", my_target.distribute_card_metod_for_wild_card_on_board);
            EditorGUI.indentLevel++;
            if (my_target.distribute_card_metod_for_wild_card_on_board == deck.distribute_card_metod.random_roll_for_each_card)
                my_target.chance_of_wild_card_on_board = EditorGUILayout.IntSlider("chance", my_target.chance_of_wild_card_on_board, 0, 100);
            else if (my_target.distribute_card_metod_for_wild_card_on_board == deck.distribute_card_metod.as_percentage_on_total_cards)
                my_target.percentage_of_wild_card_on_board = EditorGUILayout.IntSlider("percentage", my_target.percentage_of_wild_card_on_board, 0, 100);
            EditorGUI.indentLevel--;

            my_target.distribute_card_metod_for_wild_card_on_deck = (deck.distribute_card_metod)EditorGUILayout.EnumPopup("on deck", my_target.distribute_card_metod_for_wild_card_on_deck);
            EditorGUI.indentLevel++;
            if (my_target.distribute_card_metod_for_wild_card_on_deck == deck.distribute_card_metod.random_roll_for_each_card)
                my_target.chance_of_wild_card_on_deck = EditorGUILayout.IntSlider("chance", my_target.chance_of_wild_card_on_deck, 0, 100);
            else if (my_target.distribute_card_metod_for_wild_card_on_deck == deck.distribute_card_metod.as_percentage_on_total_cards)
                my_target.percentage_of_wild_card_on_deck = EditorGUILayout.IntSlider("percentage", my_target.percentage_of_wild_card_on_deck, 0, 100);
            EditorGUI.indentLevel--;

            EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;
        }


        my_target.editor_show_joker_rules = EditorGUILayout.Foldout(my_target.editor_show_joker_rules, "Joker");
        if (my_target.editor_show_joker_rules)
        {
            EditorGUI.indentLevel++;
            my_target.distribute_card_metod_for_joker_on_board = (deck.distribute_card_metod)EditorGUILayout.EnumPopup("on board", my_target.distribute_card_metod_for_joker_on_board);
            EditorGUI.indentLevel++;
            if (my_target.distribute_card_metod_for_joker_on_board == deck.distribute_card_metod.random_roll_for_each_card)
                my_target.chance_of_joker_on_board = EditorGUILayout.IntSlider("chance", my_target.chance_of_joker_on_board, 0, 100);
            else if (my_target.distribute_card_metod_for_joker_on_board == deck.distribute_card_metod.as_percentage_on_total_cards)
                my_target.percentage_of_joker_card_on_board = EditorGUILayout.IntSlider("percentage", my_target.percentage_of_joker_card_on_board, 0, 100);
            EditorGUI.indentLevel--;

            my_target.distribute_card_metod_for_joker_on_deck = (deck.distribute_card_metod)EditorGUILayout.EnumPopup("on deck", my_target.distribute_card_metod_for_joker_on_deck);
            EditorGUI.indentLevel++;
            if (my_target.distribute_card_metod_for_joker_on_deck == deck.distribute_card_metod.random_roll_for_each_card)
                my_target.chance_of_joker_on_deck = EditorGUILayout.IntSlider("chance", my_target.chance_of_joker_on_deck, 0, 100);
            else if (my_target.distribute_card_metod_for_joker_on_deck == deck.distribute_card_metod.as_percentage_on_total_cards)
                my_target.percentage_of_joker_card_on_deck = EditorGUILayout.IntSlider("percentage", my_target.percentage_of_joker_card_on_deck, 0, 100);
            EditorGUI.indentLevel--;
            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }


    //settings:
    void Animations()
    {
        deck my_target = (deck)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "deck_edit_animations");

        my_target.editor_show_animations = EditorGUILayout.Foldout(my_target.editor_show_animations, "Animations");
        if (my_target.editor_show_animations)
        {
            my_target.animation_duration = EditorGUILayout.FloatField("animation_duration",my_target.animation_duration);
            my_target.rotation_duration = EditorGUILayout.FloatField("rotation_duration", my_target.rotation_duration);

            my_target.enhanced_animation = EditorGUILayout.Toggle("enhanced_animation", my_target.enhanced_animation);
            my_target.verticalVariation = EditorGUILayout.CurveField("vertical variation",my_target.verticalVariation);
            //my_target.enhanced_animation_duration = EditorGUILayout.FloatField("enhanced_animation_duration",my_target.enhanced_animation_duration);
            my_target.verticalVariationMultiplier = EditorGUILayout.FloatField("verticalVariationMultiplier", my_target.verticalVariationMultiplier);
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Sprites()
	{
		deck my_target = (deck)target;
		EditorGUI.BeginChangeCheck ();
        Undo.RecordObject(my_target, "deck_edit_sprites");

        my_target.editor_show_sprites = EditorGUILayout.Foldout(my_target.editor_show_sprites, "Sprites");
		if (my_target.editor_show_sprites)
		{

			EditorGUI.indentLevel++;
			my_target.editor_show_sprites_normal = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal, "normal");
			if (my_target.editor_show_sprites_normal)
				{
				EditorGUI.indentLevel++;
                if (!my_target.card_back_sprite)
                    GUI.color = Color.red;
                else
                    GUI.color = Color.white;
                my_target.card_selected_sprite = EditorGUILayout.ObjectField("card selected", my_target.card_selected_sprite, typeof(Sprite), false) as Sprite;
                GUI.color = Color.white;

                if (!my_target.card_back_sprite)
					GUI.color = Color.red;
				else
					GUI.color = Color.white;
				my_target.card_back_sprite = EditorGUILayout.ObjectField("back",my_target.card_back_sprite, typeof(Sprite), false) as Sprite;
				GUI.color = Color.white;

                my_target.editor_show_sprites_normal_clubs = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_clubs, "clubs");
				if (my_target.editor_show_sprites_normal_clubs)
					{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
						{
						if (!my_target.clubs[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;

						my_target.clubs[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.clubs[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
						}
					EditorGUI.indentLevel--;
					}

				my_target.editor_show_sprites_normal_spades = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_spades, "spades");
				if (my_target.editor_show_sprites_normal_spades)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.spades[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;

						my_target.spades[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.spades[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}

				my_target.editor_show_sprites_normal_diamonds = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_diamonds, "diamonds");
				if (my_target.editor_show_sprites_normal_diamonds)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.diamonds[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						
						my_target.diamonds[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.diamonds[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}

				my_target.editor_show_sprites_normal_hearts = EditorGUILayout.Foldout(my_target.editor_show_sprites_normal_hearts, "hearts");
				if (my_target.editor_show_sprites_normal_hearts)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.hearts[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						
						my_target.hearts[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.hearts[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}

				//jokers
				my_target.normal_joker = EditorGUILayout.ObjectField("joker",my_target.normal_joker, typeof(Sprite), false) as Sprite;
				my_target.red_joker = EditorGUILayout.ObjectField("red joker",my_target.red_joker, typeof(Sprite), false) as Sprite;
				my_target.black_joker = EditorGUILayout.ObjectField("black joker",my_target.black_joker, typeof(Sprite), false) as Sprite;
				
				EditorGUI.indentLevel--;
				}

			my_target.editor_show_sprites_gold = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold, "gold");
			if (my_target.editor_show_sprites_gold)
			{
				EditorGUI.indentLevel++;
				if (!my_target.gold_card_back_sprite)
					GUI.color = Color.red;
				else
					GUI.color = Color.white;
				my_target.gold_card_back_sprite = EditorGUILayout.ObjectField("back",my_target.gold_card_back_sprite, typeof(Sprite), false) as Sprite;
				GUI.color = Color.white;

				my_target.editor_show_sprites_gold_clubs = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_clubs, "clubs");
				if (my_target.editor_show_sprites_gold_clubs)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.gold_clubs[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						
						my_target.gold_clubs[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.gold_clubs[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}
				
				my_target.editor_show_sprites_gold_spades = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_spades, "spades");
				if (my_target.editor_show_sprites_gold_spades)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.gold_spades[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						
						my_target.gold_spades[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.gold_spades[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}
				
				my_target.editor_show_sprites_gold_diamonds = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_diamonds, "diamonds");
				if (my_target.editor_show_sprites_gold_diamonds)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.gold_diamonds[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						
						my_target.gold_diamonds[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.gold_diamonds[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}
				
				my_target.editor_show_sprites_gold_hearts = EditorGUILayout.Foldout(my_target.editor_show_sprites_gold_hearts, "hearts");
				if (my_target.editor_show_sprites_gold_hearts)
				{
					EditorGUI.indentLevel++;
					for (int i = 0; i < 13; i++)
					{
						if (!my_target.gold_hearts[i])
							GUI.color = Color.red;
						else
							GUI.color = Color.white;
						
						my_target.gold_hearts[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.gold_hearts[i], typeof(Sprite), false) as Sprite;
						GUI.color = Color.white;
					}
					EditorGUI.indentLevel--;
				}

				//jokers
				my_target.gold_normal_joker = EditorGUILayout.ObjectField("joker",my_target.gold_normal_joker, typeof(Sprite), false) as Sprite;
				my_target.gold_red_joker = EditorGUILayout.ObjectField("red joker",my_target.gold_red_joker, typeof(Sprite), false) as Sprite;
				my_target.gold_black_joker = EditorGUILayout.ObjectField("black joker",my_target.gold_black_joker, typeof(Sprite), false) as Sprite;

				EditorGUI.indentLevel--;
			}

			my_target.editor_show_sprites_wild = EditorGUILayout.Foldout(my_target.editor_show_sprites_wild, "wild");
			if (my_target.editor_show_sprites_wild)
			{
				EditorGUI.indentLevel++;
				for (int i = 0; i < 13; i++)
					{
					my_target.wild[i] = EditorGUILayout.ObjectField(Sprite_name(i),my_target.wild[i], typeof(Sprite), false) as Sprite;
					}

				my_target.wild_joker = EditorGUILayout.ObjectField("joker",my_target.wild_joker, typeof(Sprite), false) as Sprite;
				EditorGUI.indentLevel--;
			}

			my_target.editor_show_sprites_padlock = EditorGUILayout.Foldout(my_target.editor_show_sprites_padlock, "padlock");			
			if (my_target.editor_show_sprites_padlock)
				{
				EditorGUI.indentLevel++;
				my_target.padlock_sprite = EditorGUILayout.ObjectField("padlock sprite",my_target.padlock_sprite, typeof(Sprite), false) as Sprite;
				my_target.royal_padlock_sprite = EditorGUILayout.ObjectField("royal padlock sprite",my_target.royal_padlock_sprite, typeof(Sprite), false) as Sprite;
				my_target.keyhole_padlock_sprite = EditorGUILayout.ObjectField("keyhole padlock sprite",my_target.keyhole_padlock_sprite, typeof(Sprite), false) as Sprite;
				EditorGUI.indentLevel--;
				}

			my_target.editor_show_sprites_bonus_cards = EditorGUILayout.Foldout(my_target.editor_show_sprites_bonus_cards, "bonus cards");			
			if (my_target.editor_show_sprites_bonus_cards)
				{
				EditorGUI.indentLevel++;
				if (my_target.bonus_cards_sprites.Length != number_of_bonus)
					my_target.bonus_cards_sprites = new Sprite[number_of_bonus];
				
				for (int i = 0; i < number_of_bonus-1; i++)
				{
					my_target.bonus_cards_sprites[i] =  EditorGUILayout.ObjectField(Enum.GetName(typeof(deck.bonus_type),i).ToString() + " card["+i+"]", my_target.bonus_cards_sprites[i] , typeof(Sprite), true) as Sprite;
				}
				EditorGUI.indentLevel--;
				}

			EditorGUI.indentLevel--;
		}

		if (EditorGUI.EndChangeCheck ())
			EditorUtility.SetDirty(my_target);

	}

    void Particles()
    {
        deck my_target = (deck)target;
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(my_target, "deck_edit_particles");

        my_target.editor_show_particles = EditorGUILayout.Foldout(my_target.editor_show_particles, "Particles");
        if (my_target.editor_show_particles)
        {
            EditorGUI.indentLevel++;

            my_target.useParticles = EditorGUILayout.Toggle("Use Particles", my_target.useParticles);

            if (my_target.particlesObj.Length != 9)
                my_target.particlesObj = new GameObject[9];

            for (int i = 0; i < my_target.particlesObj.Length; i++)
            {
                if (my_target.particlesObj[i] == null && my_target.useParticles)
                    GUI.color = Color.red;
                else
                    GUI.color = Color.white;

                string this_name = "";

                if (i == 0)
                    this_name = "Clubs";
                else if (i == 1)
                    this_name = "Spades";
                else if (i == 2)
                    this_name = "Diamonds";
                else if (i == 3)
                    this_name = "Hearts";
                else if (i == 4)
                    this_name = "Joker";
                else if (i == 5)
                    this_name = "Red Joker";
                else if (i == 6)
                    this_name = "Black Joker";
                else if (i == 7)
                    this_name = "Wild";
                else if (i == 8)
                    this_name = "Bonus";


                my_target.particlesObj[i] = EditorGUILayout.ObjectField(this_name, my_target.particlesObj[i], typeof(GameObject), true) as GameObject;

                GUI.color = Color.white;

            }

            EditorGUI.indentLevel--;
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(my_target);
    }

    void Sfx()
	{
		deck my_target = (deck)target;
		EditorGUI.BeginChangeCheck ();
        Undo.RecordObject(my_target, "deck_edit_sfx");

        my_target.editor_show_sfx = EditorGUILayout.Foldout(my_target.editor_show_sfx, "Audio sfx");
		if (my_target.editor_show_sfx)
		{
			EditorGUI.indentLevel++;
            my_target.take_this_card_start_sfx = EditorGUILayout.ObjectField("take card start", my_target.take_this_card_start_sfx, typeof(AudioClip), false) as AudioClip;
            my_target.take_this_card_end_sfx = EditorGUILayout.ObjectField("take card end", my_target.take_this_card_end_sfx, typeof(AudioClip), false) as AudioClip;
			my_target.destroy_this_card_sfx = EditorGUILayout.ObjectField("destroy card", my_target.destroy_this_card_sfx, typeof(AudioClip), false) as AudioClip;
			my_target.new_card_sfx = EditorGUILayout.ObjectField("new card", my_target.new_card_sfx, typeof(AudioClip), false) as AudioClip;

			my_target.blocked_card_sfx = EditorGUILayout.ObjectField("blocked card", my_target.blocked_card_sfx, typeof(AudioClip), false) as AudioClip;
			my_target.wrong_card_sfx = EditorGUILayout.ObjectField("wrong card", my_target.wrong_card_sfx, typeof(AudioClip), false) as AudioClip;

			my_target.win_sfx = EditorGUILayout.ObjectField("win", my_target.win_sfx, typeof(AudioClip), false) as AudioClip;
			my_target.reach_goal_sfx = EditorGUILayout.ObjectField("reach goal sfx", my_target.reach_goal_sfx, typeof(AudioClip), false) as AudioClip;
			my_target.lose_sfx = EditorGUILayout.ObjectField("lose", my_target.lose_sfx, typeof(AudioClip), false) as AudioClip;

			my_target.gui_button_sfx = EditorGUILayout.ObjectField("gui button sfx", my_target.gui_button_sfx, typeof(AudioClip), false) as AudioClip;

			EditorGUI.indentLevel--;
		}
		if (EditorGUI.EndChangeCheck ())
			EditorUtility.SetDirty(my_target);

	}

	void Advanced()
	{
		deck my_target = (deck)target;
		EditorGUI.BeginChangeCheck ();
        Undo.RecordObject(my_target, "deck_edit_advanced");

        my_target.editor_show_advanced = EditorGUILayout.Foldout(my_target.editor_show_advanced, "Advanced");
		if (my_target.editor_show_advanced)
		{
			EditorGUI.indentLevel++;
            my_target.background_image = EditorGUILayout.ObjectField(" background image", my_target.background_image, typeof(Image), true) as Image;

            my_target.editor_show_gui = EditorGUILayout.Foldout(my_target.editor_show_gui, "GUI");
			if (my_target.editor_show_gui)
			{
				EditorGUI.indentLevel++;
				my_target.show_win_or_lose_screen_after_delay = EditorGUILayout.FloatField("show win/lose screen after",my_target.show_win_or_lose_screen_after_delay);

				my_target.game_screen =  EditorGUILayout.ObjectField("game screen", my_target.game_screen , typeof(GameObject), true) as GameObject;
				EditorGUI.indentLevel++;
					my_target.score_text = EditorGUILayout.ObjectField("score text", my_target.score_text, typeof(Text), true) as Text;
					my_target.target_score_slider = EditorGUILayout.ObjectField("target score slider", my_target.target_score_slider, typeof(Slider), true) as Slider;
					my_target.target_score_text = EditorGUILayout.ObjectField("target score text", my_target.target_score_text, typeof(Text), true) as Text;

					EditorGUILayout.Space();

					my_target.combo_text = EditorGUILayout.ObjectField("combo text", my_target.combo_text, typeof(Text), true) as Text;
						my_target.end_combo_obj =  EditorGUILayout.ObjectField("end combo", my_target.end_combo_obj , typeof(GameObject), true) as GameObject;
						my_target.total_combo_multiplier_text = EditorGUILayout.ObjectField("total combo multiplier text", my_target.total_combo_multiplier_text, typeof(Text), true) as Text;

					my_target.combo_sum_count_text = EditorGUILayout.ObjectField("combo sum count text", my_target.combo_sum_count_text, typeof(Text), true) as Text; 
						my_target.combo_sum_slider = EditorGUILayout.ObjectField("combo sum slider", my_target.combo_sum_slider, typeof(Slider), true) as Slider;
						my_target.combo_sum_bonus_text = EditorGUILayout.ObjectField("combo sum bonus text", my_target.combo_sum_bonus_text, typeof(Text), true) as Text; 

					EditorGUILayout.Space();

					my_target.deck_card_left_text = EditorGUILayout.ObjectField("deck card left text", my_target.deck_card_left_text, typeof(Text), true) as Text;
					//my_target.gold_cards_count_text = EditorGUILayout.ObjectField("gold card count", my_target.gold_cards_count_text, typeof(Text), true) as Text;

					EditorGUILayout.Space();

					my_target.undo_button = EditorGUILayout.ObjectField("undo button", my_target.undo_button, typeof(Button), true) as Button;
					my_target.undo_count_text = EditorGUILayout.ObjectField("undo count", my_target.undo_count_text, typeof(Text), true) as Text;

					EditorGUILayout.Space();

					//goals window
					my_target.goals_screen =  EditorGUILayout.ObjectField("goals screen", my_target.goals_screen , typeof(GameObject), true) as GameObject;
					my_target.goal_toggle =  EditorGUILayout.ObjectField("goal toggle", my_target.goal_toggle , typeof(GameObject), true) as GameObject;
					my_target.goals_screen_list =  EditorGUILayout.ObjectField("goal toggle list", my_target.goals_screen_list  , typeof(Transform), true) as Transform;

					EditorGUILayout.Space();

					//bonus
					my_target.bonus_container =  EditorGUILayout.ObjectField("bonus container", my_target.bonus_container   , typeof(Transform), true) as Transform;
					my_target.bonus_button_obj =  EditorGUILayout.ObjectField("bonus button obj", my_target.bonus_button_obj   , typeof(GameObject), true) as GameObject;
					
					if (my_target.bonus_icons.Length != number_of_bonus)
						my_target.bonus_icons = new Sprite[number_of_bonus];

				for (int i = 0; i < number_of_bonus-1; i++)
						{
						my_target.bonus_icons[i] =  EditorGUILayout.ObjectField(Enum.GetName(typeof(deck.bonus_type),i).ToString() + " icon["+i+"]", my_target.bonus_icons[i] , typeof(Sprite), true) as Sprite;
						}

				EditorGUI.indentLevel--;
				EditorGUILayout.Space();
				my_target.my_end_screen = EditorGUILayout.ObjectField("end_screen script", my_target.my_end_screen , typeof(end_screen), true) as end_screen; 
				EditorGUI.indentLevel--;
			}

			EditorGUILayout.Space();
			my_target.ghost_card =  EditorGUILayout.ObjectField("ghost card", my_target.ghost_card , typeof(Transform), true) as Transform;
			my_target.board =  EditorGUILayout.ObjectField("board", my_target.board , typeof(Transform), true) as Transform;
			my_target.target_new_card =  EditorGUILayout.ObjectField("target_new_card", my_target.target_new_card , typeof(SpriteRenderer), true) as SpriteRenderer;
			my_target.new_card_anim_sprite =  EditorGUILayout.ObjectField("new_card_anim_sprite", my_target.new_card_anim_sprite , typeof(SpriteRenderer), true) as SpriteRenderer;
			my_target.new_card_anim =  EditorGUILayout.ObjectField("new_card_anim", my_target.new_card_anim , typeof(Animation), true) as Animation;

			my_target.my_wild_card_deck = EditorGUILayout.ObjectField("wild card deck", my_target.my_wild_card_deck , typeof(wild_card_deck), true) as wild_card_deck;

			EditorGUI.indentLevel--;
		}
		
		if (EditorGUI.EndChangeCheck ())
			EditorUtility.SetDirty(my_target);

	}

	string Sprite_name(int card_value)
	{
		string return_this = "";

		if (card_value == 0)
			return_this = "A";
		else if (card_value == 10)
			return_this = "J";
		else if (card_value == 11)
			return_this = "Q";
		else if (card_value == 12)
			return_this = "K";
		else
			return_this = (card_value + 1).ToString();

		return return_this;
	}
}
